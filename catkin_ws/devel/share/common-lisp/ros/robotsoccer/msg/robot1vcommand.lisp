; Auto-generated. Do not edit!


(cl:in-package robotsoccer-msg)


;//! \htmlinclude robot1vcommand.msg.html

(cl:defclass <robot1vcommand> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (vx
    :reader vx
    :initarg :vx
    :type cl:float
    :initform 0.0)
   (vy
    :reader vy
    :initarg :vy
    :type cl:float
    :initform 0.0)
   (vtheta
    :reader vtheta
    :initarg :vtheta
    :type cl:float
    :initform 0.0))
)

(cl:defclass robot1vcommand (<robot1vcommand>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <robot1vcommand>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'robot1vcommand)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-msg:<robot1vcommand> is deprecated: use robotsoccer-msg:robot1vcommand instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <robot1vcommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:header-val is deprecated.  Use robotsoccer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'vx-val :lambda-list '(m))
(cl:defmethod vx-val ((m <robot1vcommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:vx-val is deprecated.  Use robotsoccer-msg:vx instead.")
  (vx m))

(cl:ensure-generic-function 'vy-val :lambda-list '(m))
(cl:defmethod vy-val ((m <robot1vcommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:vy-val is deprecated.  Use robotsoccer-msg:vy instead.")
  (vy m))

(cl:ensure-generic-function 'vtheta-val :lambda-list '(m))
(cl:defmethod vtheta-val ((m <robot1vcommand>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:vtheta-val is deprecated.  Use robotsoccer-msg:vtheta instead.")
  (vtheta m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <robot1vcommand>) ostream)
  "Serializes a message object of type '<robot1vcommand>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'vtheta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <robot1vcommand>) istream)
  "Deserializes a message object of type '<robot1vcommand>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'vtheta) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<robot1vcommand>)))
  "Returns string type for a message object of type '<robot1vcommand>"
  "robotsoccer/robot1vcommand")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'robot1vcommand)))
  "Returns string type for a message object of type 'robot1vcommand"
  "robotsoccer/robot1vcommand")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<robot1vcommand>)))
  "Returns md5sum for a message object of type '<robot1vcommand>"
  "75d173e232e550ed86a6be02fb54a823")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'robot1vcommand)))
  "Returns md5sum for a message object of type 'robot1vcommand"
  "75d173e232e550ed86a6be02fb54a823")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<robot1vcommand>)))
  "Returns full string definition for message of type '<robot1vcommand>"
  (cl:format cl:nil "Header header~%float32 vx~%float32 vy~%float32 vtheta~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'robot1vcommand)))
  "Returns full string definition for message of type 'robot1vcommand"
  (cl:format cl:nil "Header header~%float32 vx~%float32 vy~%float32 vtheta~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <robot1vcommand>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <robot1vcommand>))
  "Converts a ROS message object to a list"
  (cl:list 'robot1vcommand
    (cl:cons ':header (header msg))
    (cl:cons ':vx (vx msg))
    (cl:cons ':vy (vy msg))
    (cl:cons ':vtheta (vtheta msg))
))
