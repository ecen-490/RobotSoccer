import serial
import struct
import time
import numpy as np


checksum = 0
RoboClaw1 = 0x80;
RoboClaw2 = 0x81;

class Cmd():
	M1FORWARD = 0
	M1BACKWARD = 1
	SETMINMB = 2
	SETMAXMB = 3
	M2FORWARD = 4
	M2BACKWARD = 5
	M17BIT = 6
	M27BIT = 7
	MIXEDFORWARD = 8
	MIXEDBACKWARD = 9
	MIXEDRIGHT = 10
	MIXEDLEFT = 11
	MIXEDFB = 12
	MIXEDLR = 13
	GETM1ENC = 16
	GETM2ENC = 17
	GETM1SPEED = 18
	GETM2SPEED = 19
	RESETENC = 20
	GETVERSION = 21
	SETM1ENCCOUNT = 22
	SETM2ENCCOUNT = 23
	GETMBATT = 24
	GETLBATT = 25
	SETMINLB = 26
	SETMAXLB = 27
	SETM1PID = 28
	SETM2PID = 29
	GETM1ISPEED = 30
	GETM2ISPEED = 31
	M1DUTY = 32
	M2DUTY = 33
	MIXEDDUTY = 34
	M1SPEED = 35
	M2SPEED = 36
	MIXEDSPEED = 37
	M1SPEEDACCEL = 38
	M2SPEEDACCEL = 39
	MIXEDSPEEDACCEL = 40
	M1SPEEDDIST = 41
	M2SPEEDDIST = 42
	MIXEDSPEEDDIST = 43
	M1SPEEDACCELDIST = 44
	M2SPEEDACCELDIST = 45
	MIXEDSPEEDACCELDIST = 46
	GETBUFFERS = 47
	GETPWMS = 48
	GETCURRENTS = 49
	MIXEDSPEED2ACCEL = 50
	MIXEDSPEED2ACCELDIST = 51
	M1DUTYACCEL = 52
	M2DUTYACCEL = 53
	MIXEDDUTYACCEL = 54
	READM1PID = 55
	READM2PID = 56
	SETMAINVOLTAGES = 57
	SETLOGICVOLTAGES = 58
	GETMINMAXMAINVOLTAGES = 59
	GETMINMAXLOGICVOLTAGES = 60
	SETM1POSPID = 61
	SETM2POSPID = 62
	READM1POSPID = 63
	READM2POSPID = 64
	M1SPEEDACCELDECCELPOS = 65
	M2SPEEDACCELDECCELPOS = 66
	MIXEDSPEEDACCELDECCELPOS = 67
	SETM1DEFAULTACCEL = 68
	SETM2DEFAULTACCEL = 69
	SETPINFUNCTIONS = 74
	GETPINFUNCTIONS = 75
	SETDEADBAND = 76
	GETDEADBAND = 77
	RESTOREDEFAULTS = 80
	GETTEMP = 82
	GETTEMP2 = 83
	GETERROR = 90
	GETENCODERMODE = 91
	SETM1ENCODERMODE = 92
	SETM2ENCODERMODE = 93
	WRITENVM = 94
	READNVM = 95
	SETCONFIG = 98
	GETCONFIG = 99
	SETM1MAXCURRENT = 133
	SETM2MAXCURRENT = 134
	GETM1MAXCURRENT = 135
	GETM2MAXCURRENT = 136
	SETPWMMODE = 148
	GETPWMMODE = 149
	FLAGBOOTLOADER = 255

def sendcommand(address,command):
    global checksum
    checksum = address
    port.write(chr(address));
    checksum += command
    port.write(chr(command));
    return;

def readbyte():
    global checksum
    val = struct.unpack('>B',port.read(1));
    checksum += val[0]
    return val[0];    
def readsbyte():
    global checksum
    val = struct.unpack('>b',port.read(1));
    checksum += val[0]
    return val[0];    
def readword():
    global checksum
    val = struct.unpack('>H',port.read(2));
    checksum += (val[0]&0xFF)
    checksum += (val[0]>>8)&0xFF
    return val[0];    
def readsword():
    global checksum
    val = struct.unpack('>h',port.read(2));
    checksum += val[0]
    checksum += (val[0]>>8)&0xFF
    return val[0];    
def readlong():
    global checksum
    val = struct.unpack('>L',port.read(4));
    checksum += val[0]
    checksum += (val[0]>>8)&0xFF
    checksum += (val[0]>>16)&0xFF
    checksum += (val[0]>>24)&0xFF
    return val[0];    
def readslong():
    global checksum
    val = struct.unpack('>l',port.read(4));
    checksum += val[0]
    checksum += (val[0]>>8)&0xFF
    checksum += (val[0]>>16)&0xFF
    checksum += (val[0]>>24)&0xFF
    return val[0];    

def writebyte(val):
    global checksum
    checksum += val
    return port.write(struct.pack('>B',val));
def writesbyte(val):
    global checksum
    checksum += val
    return port.write(struct.pack('>b',val));
def writeword(val):
    global checksum
    checksum += val
    checksum += (val>>8)&0xFF
    return port.write(struct.pack('>H',val));
def writesword(val):
    global checksum
    checksum += val
    checksum += (val>>8)&0xFF
    return port.write(struct.pack('>h',val));
def writelong(val):
    global checksum
    checksum += val
    checksum += (val>>8)&0xFF
    checksum += (val>>16)&0xFF
    checksum += (val>>24)&0xFF
    return port.write(struct.pack('>L',val));
def writeslong(val):
    global checksum
    checksum += val
    checksum += (val>>8)&0xFF
    checksum += (val>>16)&0xFF
    checksum += (val>>24)&0xFF
    return port.write(struct.pack('>l',val));

def M1Forward(addr,val):
    sendcommand(addr,Cmd.M1FORWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def M1Backward(addr,val):
    sendcommand(addr,Cmd.M1BACKWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def SetMinMainBattery(addr, val):
    sendcommand(addr,Cmd.SETMINMB)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def SetMaxMainBattery(addr, val):
    sendcommand(addr,Cmd.SETMAXMB)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def M2Forward(addr,val):
    sendcommand(addr,Cmd.M2FORWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def M2Backward(addr,val):
    sendcommand(addr,Cmd.M2BACKWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def DriveM1(addr,val):
    sendcommand(addr,Cmd.M17BIT)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def DriveM2(addr,val):
    sendcommand(addr,Cmd.M27BIT)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def ForwardMixed(addr,val):
    sendcommand(addr,Cmd.MIXEDFORWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def BackwardMixed(addr,val):
    sendcommand(addr,Cmd.MIXEDBACKWARD)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def RightMixed(addr,val):
    sendcommand(addr,Cmd.MIXEDRIGHT)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def LeftMixed(addr,val):
    sendcommand(addr,Cmd.MIXEDLEFT)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def DriveMixed(addr, val):
    sendcommand(addr,Cmd.MIXEDFB)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def TurnMixed(addr, val):
    sendcommand(addr,Cmd.MIXEDLR)
    writebyte(val)
    writebyte(checksum&0x7F);
    return;

def readM1encoder(addr):
    sendcommand(addr,Cmd.GETM1ENC);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (enc,status);
    return (-1,-1);

def readM2encoder(addr):
    sendcommand(addr,Cmd.GETM2ENC);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (enc,status);
    return (-1,-1);

def readM1speed(addr):
    sendcommand(addr,Cmd.GETM1SPEED);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return enc
    return -1;

def readM2speed(addr):
    sendcommand(addr,Cmd.GETM2SPEED);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return enc
    return -1;
 
def ResetEncoderCnts(addr):
    sendcommand(addr,Cmd.RESETENC)
    writebyte(checksum&0x7F);
    return;

def readversion(addr):
    sendcommand(addr,Cmd.GETVERSION)
    return port.read(32);

def readmainbattery(addr):
    sendcommand(addr,Cmd.GETMBATT);
    val = readword()
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return val
    return -1

def readlogicbattery(addr):
    sendcommand(addr,Cmd.GETLBATT);
    val = readword()
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return val
    return -1

def SetM1pidq(addr,p,i,d,qpps):
    sendcommand(addr,Cmd.SETM1PID)
    writelong(d)
    writelong(p)
    writelong(i)
    writelong(qpps)
    writebyte(checksum&0x7F);
    return;

def SetM2pidq(addr,p,i,d,qpps):
    sendcommand(addr,Cmd.SETM2PID)
    writelong(d)
    writelong(p)
    writelong(i)
    writelong(qpps)
    writebyte(checksum&0x7F);
    return;

def readM1instspeed(addr):
    sendcommand(addr,Cmd.GETM1ISPEED);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (enc,status);
    return (-1,-1);

def readM2instspeed(addr):
    sendcommand(addr,Cmd.GETM2ISPEED);
    enc = readslong();
    status = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (enc,status);
    return (-1,-1);

def SetM1Duty(addr, val):
    sendcommand(addr,Cmd.M1DUTY)
    writesword(val)
    writebyte(checksum&0x7F);
    return;

def SetM2Duty(addr,val):
    sendcommand(addr,Cmd.M2DUTY)
    writesword(val)
    writebyte(checksum&0x7F);
    return;

def SetMixedDuty(addr,m1,m2):
    sendcommand(addr,Cmd.MIXEDDUTY)
    writesword(m1)
    writesword(m2)
    writebyte(checksum&0x7F);
    return;

def SetM1Speed(addr,val):
    sendcommand(addr,Cmd.M1SPEED)
    writeslong(val)
    writebyte(checksum&0x7F);
    return;

def SetM2Speed(addr,val):
    sendcommand(addr,Cmd.M2SPEED)
    writeslong(val)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeed(addr,m1,m2):
    sendcommand(addr,Cmd.MIXEDSPEED)
    writeslong(m1)
    writeslong(m2)
    writebyte(checksum&0x7F);
    return;

def SetM1SpeedAccel(addr,accel,speed):
    sendcommand(addr,Cmd.M1SPEEDACCEL)
    writelong(accel)
    writeslong(speed)
    writebyte(checksum&0x7F);
    return;

def SetM2SpeedAccel(addr,accel,speed):
    sendcommand(addr,Cmd.M2SPEEDACCEL)
    writelong(accel)
    writeslong(speed)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeedAccel(addr,accel,speed1,speed2):
    sendcommand(addr,Cmd.MIXEDSPEEDACCEL)
    writelong(accel)
    writeslong(speed1)
    writeslong(speed2)
    writebyte(checksum&0x7F);
    return;

def SetM1SpeedDistance(addr,speed,distance,buffer):
    sendcommand(addr,Cmd.M1SPEEDDIST)
    writeslong(speed)
    writelong(distance)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetM2SpeedDistance(addr,speed,distance,buffer):
    sendcommand(addr,Cmd.M2SPEEDDIST)
    writeslong(speed)
    writelong(distance)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeedDistance(addr,speed1,distance1,speed2,distance2,buffer):
    sendcommand(addr,Cmd.MIXEDSPEEDDIST)
    writeslong(speed1)
    writelong(distance1)
    writeslong(speed2)
    writelong(distance2)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetM1SpeedAccelDistance(addr,accel,speed,distance,buffer):
    sendcommand(addr,Cmd.M1SPEEDACCELDIST)
    writelong(accel)
    writeslong(speed)
    writelong(distance)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetM2SpeedAccelDistance(addr,accel,speed,distance,buffer):
    sendcommand(addr,Cmd.M2SPEEDACCELDIST)
    writelong(accel)
    writeslong(speed)
    writelong(distance)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeedAccelDistance(addr,accel,speed1,distance1,speed2,distance2,buffer):
    sendcommand(addr,MIXEDSPEEDACCELDIST)
    writelong(accel)
    writeslong(speed1)
    writelong(distance1)
    writeslong(speed2)
    writelong(distance2)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def readbuffercnts(addr):
    sendcommand(addr,Cmd.GETBUFFERS);
    buffer1 = readbyte();
    buffer2 = readbyte();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (buffer1,buffer2);
    return (-1,-1);

def readcurrents(addr):
    sendcommand(addr,Cmd.GETCURRENTS);
    motor1 = readword();
    motor2 = readword();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (motor1,motor2);
    return (-1,-1);

def SetMixedSpeedIAccel(addr,accel1,speed1,accel2,speed2):
    sendcommand(addr,Cmd.MIXEDSPEED2ACCEL)
    writelong(accel1)
    writeslong(speed1)
    writelong(accel2)
    writeslong(speed2)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeedIAccelDistance(addr,accel1,speed1,distance1,accel2,speed2,distance2,buffer):
    sendcommand(addr,Cdm.MIXEDSPEED2ACCELDIST)
    writelong(accel1)
    writeslong(speed1)
    writelong(distance1)
    writelong(accel2)
    writeslong(speed2)
    writelong(distance2)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetM1DutyAccel(addr,accel,duty):
    sendcommand(addr,Cmd.M1DUTYACCEL)
    writesword(duty)
    writeword(accel)
    writebyte(checksum&0x7F);
    return;

def SetM2DutyAccel(addr,accel,duty):
    sendcommand(addr,Cmd.M2DUTYACCEL)
    writesword(duty)
    writeword(accel)
    writebyte(checksum&0x7F);
    return;

def SetMixedDutyAccel(addr,accel1,duty1,accel2,duty2):
    sendcommand(addr,Cmd.MIXEDDUTYACCEL)
    writesword(duty1)
    writeword(accel1)
    writesword(duty2)
    writeword(accel2)
    writebyte(checksum&0x7F);
    return;

def readM1pidq(addr):
    sendcommand(addr,Cmd.READM1PID);
    p = readlong();
    i = readlong();
    d = readlong();
    qpps = readlong();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (p,i,d,qpps);
    return (-1,-1,-1,-1)

def readM2pidq(addr):
    sendcommand(addr,Cmd.READM2PID);
    p = readlong();
    i = readlong();
    d = readlong();
    qpps = readlong();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (p,i,d,qpps);
    return (-1,-1,-1,-1)

def readmainbatterysettings(addr):
    sendcommand(addr,Cdm.GETMINMAXMAINVOLTAGES);
    min = readword();
    max = readword();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (min,max);
    return (-1,-1);

def readlogicbatterysettings(addr):
    sendcommand(addr,Cmd.GETMINMAXLOGICVOLTAGES);
    min = readword();
    max = readword();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (min,max);
    return (-1,-1);

def SetM1PositionConstants(addr,kp,ki,kd,kimax,deadzone,min,max):
    sendcommand(addr,Cmd.SETM1POSPID)
    writelong(kd)
    writelong(kp)
    writelong(ki)
    writelong(kimax)
    writelong(deadzone);
    writelong(min);
    writelong(max);
    writebyte(checksum&0x7F);
    return;

def SetM2PositionConstants(addr,kp,ki,kd,kimax,deadzone,min,max):
    sendcommand(addr,Cmd.SETM2POSPID)
    writelong(kd)
    writelong(kp)
    writelong(ki)
    writelong(kimax)
    writelong(deadzone);
    writelong(min);
    writelong(max);
    writebyte(checksum&0x7F);
    return;

def readM1PositionConstants(addr):
    sendcommand(addr,Cmd.READM1POSPID);
    p = readlong();
    i = readlong();
    d = readlong();
    imax = readlong();
    deadzone = readlong();
    min = readlong();
    max = readlong();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (p,i,d,imax,deadzone,min,max);
    return (-1,-1,-1,-1,-1,-1,-1)

def readM2PositionConstants(addr):
    sendcommand(addr,Cmd.READM2POSPID);
    p = readlong();
    i = readlong();
    d = readlong();
    imax = readlong();
    deadzone = readlong();
    min = readlong();
    max = readlong();
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return (p,i,d,imax,deadzone,min,max);
    return (-1,-1,-1,-1,-1,-1,-1)

def SetM1SpeedAccelDeccelPosition(addr,accel,speed,deccel,position,buffer):
    sendcommand(addr,Cmd.M1SPEEDACCELDECCELPOS)
    writelong(accel)
    writelong(speed)
    writelong(deccel)
    writelong(position)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetM2SpeedAccelDeccelPosition(addr,accel,speed,deccel,position,buffer):
    sendcommand(addr,Cmd.M2SPEEDACCELDECCELPOS)
    writelong(accel)
    writelong(speed)
    writelong(deccel)
    writelong(position)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def SetMixedSpeedAccelDeccelPosition(addr,accel1,speed1,deccel1,position1,accel2,speed2,deccel2,position2,buffer):
    sendcommand(addr,Cmd.MIXEDSPEEDACCELDECCELPOS)
    writelong(accel1)
    writelong(speed1)
    writelong(deccel1)
    writelong(position1)
    writelong(accel2)
    writelong(speed2)
    writelong(deccel2)
    writelong(position2)
    writebyte(buffer)
    writebyte(checksum&0x7F);
    return;

def readtemperature(addr):
    sendcommand(addr,Cmd.GETTEMP);
    val = readword()
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return val
    return -1

def readerrorstate(addr):
    sendcommand(addr,Cmd.GETERROR);
    val = readbyte()
    crc = checksum&0x7F
    if crc==readbyte()&0x7F:
        return val
    return -1
    
def readEncoderMode(addr):
  sendcommand(addr,Cmd.GETENCODERMODE)
  mode1 = readbyte()
  mode2 = readbyte()
  crc = checksum&0x7F
  if crc==readbyte()&0x7F:
    return (mode1,mode2)
  return (-1,-1)
  
def writeSettingsToMem(addr):
  sendcommand(addr,Cmd.WRITENVM)
  crc = checksum&0x7F
  if crc==readbyte()&0x7F:
    return 0
  return -16

def calibrateRoboclaws():
    p = int(65536 * 4) # int(65536 * 4)
    i = int(65536 * 2) # int(65536 * 2)
    d = int(65536 * 1) # int(65536 * 6)
    #last good calibration readings
    voltage = 16.9 # 16.7   # 15.7   # 15.9   # 15.9   # 15.8   # 16.5   # 16.5   # 15.9   # 15.9   # 15.5   # 15.3   # 16.6   # 15.5
    #qqps_m1 = 142977 # 141606 # 118234 # 129122 # 136502 # 140181 # 146772 # 130185 # 146330 # 149353 # 137669 # 141136 # 148132 # 149287
   # qqps_m2 = 178091 # 187808 # 139632 # 159086 # 164265 # 164244 # 177244 # 180669 # 180616 # 166407 # 172434 # 165175 # 168984 # 169069
    #qqps_m3 = 195319 # 175863 # 130377 # 154211 # 171489 # 165285 # 183906 # 181536 # 175021 # 170281 # 159700 # 161999 # 165146 # 164071

    qqps_m1 = 138900#182512#182874#191050#191979
    qqps_m2 = 142086#173874#178140#201026#206618
    qqps_m3 = 139784#182041#193058#189978

    #read_v = readmainbattery(128) / 10.0

    #scale = lambda x: int(x*voltage/read_v)
    #speedM1 = scale(qqps_m1)
    #speedM2 = scale(qqps_m2)
    #speedM3 = scale(qqps_m3)

    SetM1pidq(RoboClaw2,p,i,d,qqps_m1)
    SetM1pidq(RoboClaw1,p,i,d,qqps_m2)
    SetM2pidq(RoboClaw1,p,i,d,qqps_m3)





#Rasberry Pi/Linux Serial instance example
port = serial.Serial("/dev/ttySAC0", baudrate=38400, timeout=1)



#Get version string
#sendcommand(RoboClaw2,21)
#rcv = port.read(32) #32
#print(repr(rcv))
#sendcommand(RoboClaw1,21)
#rcv = port.read(32) #32
#print(repr(rcv))
