from ._robot2commands import *
from ._robot1commands import *
from ._kickerplate_robot1command import *
from ._locations import *
from ._robot1vcommand import *
from ._currentlocations import *
from ._commandcenter import *
