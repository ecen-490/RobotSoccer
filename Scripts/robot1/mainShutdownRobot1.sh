#!/bin/sh

kill -9 `ps aux | grep RobotMain | awk '{print $2}'`
kill -9 `ps aux | grep LocationNode | awk '{print $2}'`
kill -9 `ps aux | grep Robot1 | awk '{print $2}'`

exit
