; Auto-generated. Do not edit!


(cl:in-package robotsoccer-srv)


;//! \htmlinclude predictedLocations-request.msg.html

(cl:defclass <predictedLocations-request> (roslisp-msg-protocol:ros-message)
  ((time
    :reader time
    :initarg :time
    :type cl:float
    :initform 0.0)
   (case
    :reader case
    :initarg :case
    :type cl:integer
    :initform 0))
)

(cl:defclass predictedLocations-request (<predictedLocations-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <predictedLocations-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'predictedLocations-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-srv:<predictedLocations-request> is deprecated: use robotsoccer-srv:predictedLocations-request instead.")))

(cl:ensure-generic-function 'time-val :lambda-list '(m))
(cl:defmethod time-val ((m <predictedLocations-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-srv:time-val is deprecated.  Use robotsoccer-srv:time instead.")
  (time m))

(cl:ensure-generic-function 'case-val :lambda-list '(m))
(cl:defmethod case-val ((m <predictedLocations-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-srv:case-val is deprecated.  Use robotsoccer-srv:case instead.")
  (case m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <predictedLocations-request>) ostream)
  "Serializes a message object of type '<predictedLocations-request>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'time))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let* ((signed (cl:slot-value msg 'case)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <predictedLocations-request>) istream)
  "Deserializes a message object of type '<predictedLocations-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'time) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'case) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<predictedLocations-request>)))
  "Returns string type for a service object of type '<predictedLocations-request>"
  "robotsoccer/predictedLocationsRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'predictedLocations-request)))
  "Returns string type for a service object of type 'predictedLocations-request"
  "robotsoccer/predictedLocationsRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<predictedLocations-request>)))
  "Returns md5sum for a message object of type '<predictedLocations-request>"
  "4a06b0990e052bedf3f3b9f3d4f4dda1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'predictedLocations-request)))
  "Returns md5sum for a message object of type 'predictedLocations-request"
  "4a06b0990e052bedf3f3b9f3d4f4dda1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<predictedLocations-request>)))
  "Returns full string definition for message of type '<predictedLocations-request>"
  (cl:format cl:nil "float32 time~%int32 case~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'predictedLocations-request)))
  "Returns full string definition for message of type 'predictedLocations-request"
  (cl:format cl:nil "float32 time~%int32 case~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <predictedLocations-request>))
  (cl:+ 0
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <predictedLocations-request>))
  "Converts a ROS message object to a list"
  (cl:list 'predictedLocations-request
    (cl:cons ':time (time msg))
    (cl:cons ':case (case msg))
))
;//! \htmlinclude predictedLocations-response.msg.html

(cl:defclass <predictedLocations-response> (roslisp-msg-protocol:ros-message)
  ((predictedStates
    :reader predictedStates
    :initarg :predictedStates
    :type cl:string
    :initform ""))
)

(cl:defclass predictedLocations-response (<predictedLocations-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <predictedLocations-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'predictedLocations-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-srv:<predictedLocations-response> is deprecated: use robotsoccer-srv:predictedLocations-response instead.")))

(cl:ensure-generic-function 'predictedStates-val :lambda-list '(m))
(cl:defmethod predictedStates-val ((m <predictedLocations-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-srv:predictedStates-val is deprecated.  Use robotsoccer-srv:predictedStates instead.")
  (predictedStates m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <predictedLocations-response>) ostream)
  "Serializes a message object of type '<predictedLocations-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'predictedStates))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'predictedStates))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <predictedLocations-response>) istream)
  "Deserializes a message object of type '<predictedLocations-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'predictedStates) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'predictedStates) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<predictedLocations-response>)))
  "Returns string type for a service object of type '<predictedLocations-response>"
  "robotsoccer/predictedLocationsResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'predictedLocations-response)))
  "Returns string type for a service object of type 'predictedLocations-response"
  "robotsoccer/predictedLocationsResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<predictedLocations-response>)))
  "Returns md5sum for a message object of type '<predictedLocations-response>"
  "4a06b0990e052bedf3f3b9f3d4f4dda1")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'predictedLocations-response)))
  "Returns md5sum for a message object of type 'predictedLocations-response"
  "4a06b0990e052bedf3f3b9f3d4f4dda1")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<predictedLocations-response>)))
  "Returns full string definition for message of type '<predictedLocations-response>"
  (cl:format cl:nil "string predictedStates~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'predictedLocations-response)))
  "Returns full string definition for message of type 'predictedLocations-response"
  (cl:format cl:nil "string predictedStates~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <predictedLocations-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'predictedStates))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <predictedLocations-response>))
  "Converts a ROS message object to a list"
  (cl:list 'predictedLocations-response
    (cl:cons ':predictedStates (predictedStates msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'predictedLocations)))
  'predictedLocations-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'predictedLocations)))
  'predictedLocations-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'predictedLocations)))
  "Returns string type for a service object of type '<predictedLocations>"
  "robotsoccer/predictedLocations")