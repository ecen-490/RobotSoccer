# Robot Soccer #

Team: M2CK<br>
Winter 2016<br>
Wiki Homepage: [Robot Soccer Wiki](https://gitlab.com/ecen-490/RobotSoccer/wikis/home)<br>
## Using GIT ##
Welcome to our group git project hosted on gitlab. Git can be managed using CLI or a GUI interface such as [Source Tree](https://www.sourcetreeapp.com/)

Helpful [guide](https://guides.github.com/features/mastering-markdown/) for writing the readme files.

Gitlab [specifics](https://github.com/gitlabhq/gitlabhq/blob/master/doc/markdown/markdown.md) for editing MD files. 

The project files can be cloned from URL using Source Tree or CLI with the following URL. (https://gitlab.com/ecen-490/RobotSoccer/)

Short guide on how to use git. (http://rogerdudler.github.io/git-guide/). GUI's make reading changes and uploading simple. The basic outline is you clone the project to a working directory and then when you've completed your changes you add your files you want to commit to the project and git push to the origin.

### Basic overview if using CLI ###
```
git clone https://gitlab.com/ecen-490/RobotSoccer.git
*complete your changes*
git add -A // Add all files to your commit
git commit -m "commit message goes here"
git push // Will prompt for your credentials
```

## Project File Structure ##

### catkin_ws ###
All ROS project files and source code is located in this folder. See the [ROS Tutorial](http://wiki.ros.org/ROS/Tutorials) for how to use ROS. Pay particular attention to the CMakeLists.txt document for including new messages or services. Source is compiled as follows.<br>
```
cd ~/RobotSoccer/catkin_ws/
catkin_make
source devel/setup.bash
```
After source code is compiled you can run the code using ROS as follows: Note we run 4 programs concurrently: computer_vision, LocationNode.py, RobotMain.py, Robot1.py
```
rosrun robotsoccer RobotMain.py
```

### Documentation ###
Contains all the Business class assignments and datasheets for the Roboclaws. 
<br>
### Scripts ###
Some helpful scripts used to launch all the different different processes at once and SSH into the robot. Note we use static IP addresses for our desktop and Robot. 

### Simulation ###
Our Matlab simulation used for the first competition. Where the main strategy was determined and refined.