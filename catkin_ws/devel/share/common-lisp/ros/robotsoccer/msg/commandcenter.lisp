; Auto-generated. Do not edit!


(cl:in-package robotsoccer-msg)


;//! \htmlinclude commandcenter.msg.html

(cl:defclass <commandcenter> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (command
    :reader command
    :initarg :command
    :type cl:integer
    :initform 0))
)

(cl:defclass commandcenter (<commandcenter>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <commandcenter>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'commandcenter)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-msg:<commandcenter> is deprecated: use robotsoccer-msg:commandcenter instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <commandcenter>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:header-val is deprecated.  Use robotsoccer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'command-val :lambda-list '(m))
(cl:defmethod command-val ((m <commandcenter>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:command-val is deprecated.  Use robotsoccer-msg:command instead.")
  (command m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <commandcenter>) ostream)
  "Serializes a message object of type '<commandcenter>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'command)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <commandcenter>) istream)
  "Deserializes a message object of type '<commandcenter>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'command) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<commandcenter>)))
  "Returns string type for a message object of type '<commandcenter>"
  "robotsoccer/commandcenter")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'commandcenter)))
  "Returns string type for a message object of type 'commandcenter"
  "robotsoccer/commandcenter")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<commandcenter>)))
  "Returns md5sum for a message object of type '<commandcenter>"
  "b5324fc73f93e6e7fc3f8dd5112ba032")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'commandcenter)))
  "Returns md5sum for a message object of type 'commandcenter"
  "b5324fc73f93e6e7fc3f8dd5112ba032")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<commandcenter>)))
  "Returns full string definition for message of type '<commandcenter>"
  (cl:format cl:nil "Header header~%int32 command~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'commandcenter)))
  "Returns full string definition for message of type 'commandcenter"
  (cl:format cl:nil "Header header~%int32 command~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <commandcenter>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <commandcenter>))
  "Converts a ROS message object to a list"
  (cl:list 'commandcenter
    (cl:cons ':header (header msg))
    (cl:cons ':command (command msg))
))
