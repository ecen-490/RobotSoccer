import pygame
import time
from pygame.locals import *
from Getch import _Getch
import repeated_timer as RT
import numpy as np
import roboclaw_motion as RM
import controller as ctrl
import roboclaw_kicker as RK
from Param2 import P

class Controller():
    posMode = 'b'
    firstVisit = True
    inputError = False
    velMode = 'a'
    exitMode = chr(27)
   
    def __init__(self,V_max,V_step):
        self.V = 0.0             # Velocity
        self.V_max = V_max       # Max Velocity
        self.V_step = V_step     # Velocity step

    def incrementPosSpeed(self):
        if self.V < 0: self.V = 0 -self.V_step
        self.V = self.V + self.V_step
        if self.V > self.V_max:
            self.V = self.V_max

    def incrementNegSpeed(self):
        if self.V > 0: self.V = 0 + self.V_step
        self.V = self.V - self.V_step
        if self.V < - self.V_max:
            self.V = -self.V_max

    def slowDown(self):
        if abs(self.V) > 0.0:
            if self.V < 0.0:
                self.V = self.V + self.V_step
            elif self.V > 0.0:
                self.V = self.V - self.V_step
            else:
                self.V = 0.0
        else:
            self.V = 0.0
        self.V = 0

    def stop(self):
        self.V = 0


Vx = Controller(1,0.2)
Vy = Controller(1,0.2)
Vth = Controller(2.0,0.5)


def initScreen():
    pygame.init()
    screen=pygame.display.set_mode((640,480),0,24)
    pygame.display.set_caption("Key Press Test")
    f1=pygame.font.SysFont("comicsansms",24)
    screen.fill((255,255,255))



def velocityControlInstructions():
    print('Please press one of the following keys:\n')
    print('\'w\' - Increase forward speed \n')
    print('\'s\' - Increase reverse speed \n')
    print('\'a\' - Increase left translational speed \n')
    print('\'d\' - Increase right translational speed \n')
    print('\'j\' - Increase left rotational speed \n')
    print('\'k\' - Increase right rotational speed \n')
    print('\'q\' - Quit the mode \n')

def velocityControl():  
    exitMode = False 
    pygame.event.get()   
    if pygame.key.get_focused():
        press=pygame.key.get_pressed()
	
	if(press[pygame.K_SPACE]):
	    RK.goGoGadgetKickerPlate(250.0, 127)

        if(press[pygame.K_w] and not press[pygame.K_s]):
            Vx.incrementPosSpeed()
        elif(press[pygame.K_s] and not press[pygame.K_w]):
            Vx.incrementNegSpeed()
        elif(not press[pygame.K_s] and not press[pygame.K_w]):
            Vx.slowDown()

        if(press[pygame.K_a] and not press[pygame.K_d]):
            Vy.incrementPosSpeed()
        elif(press[pygame.K_d] and not press[pygame.K_a]):
            Vy.incrementNegSpeed()
        elif(not press[pygame.K_a] and not press[pygame.K_d]):
            Vy.slowDown()

        if(press[pygame.K_j] and not press[pygame.K_k]):
            Vth.incrementPosSpeed()
        elif(press[pygame.K_k] and not press[pygame.K_j]):
            Vth.incrementNegSpeed()
        elif(not press[pygame.K_j] and not press[pygame.K_k]):
            Vth.slowDown()

        if(press[pygame.K_t]):
            Vx.stop()
            Vy.stop()
            Vth.stop()

        #pygame.display.update()
        if(press[pygame.K_q]):
            Vx.stop()
            Vy.stop()
            Vth.stop()
            pygame.display.quit()
            exitMode = True
        RM.moveBodyFrame(Vx.V,Vy.V,Vth.V,0)

        #print('x_vel ', Vx.V)
        #print('y_vel', Vy.V)
        #print('theta_vel',Vth.V)

    # Calle move function
    
    return exitMode
                       






def get_modeInstructions():
    print('\n')
    if Controller.firstVisit:
        print('Welcome friend! \n')
        Controller.firstVisit = False
    elif Controller.inputError:
        print('I\'m sorry, but that is not a valid input. \n')
        Controller.inputError = False
    else:       
        print('I\'m\' glad that you have returned friend')

    print('Please select the mode from the following options: \n')
    print( '\t\'a\' - Velocity Control \n')
    print( '\t\'b\' - Positional Control \n') 
    print('\t\'escape\' - Quit the program \n')



def get_mode():
    get_modeInstructions()
    correctInput = False
    while(not correctInput):
        getch = _Getch()
        k = getch() 
        if k == Controller.posMode:
            correctInput = True
        elif k == Controller.velMode:
            initScreen()
            velocityControlInstructions()
            correctInput = True
        elif k == Controller.exitMode:
            correctInput = True
            print('I\'m sorry to see you leave.\n')
        else:
            Controller.InputError = True
            get_modeInstructions()
    return k



def positionControl(): 
    exitMode = False   
    correctInput = False
    print('Please enter desired position in meters and degrees, or type \'q\' to quit.')
    print('The positional format is: x y theta')
    while not correctInput:
        user_input = raw_input('Enter in x y theta or type q --> ')
        split_it_up = user_input.split()

        if user_input == 'q':
            exitMode = True
            correctInput = True
        elif len(split_it_up) == 3 and split_it_up[0].isdigit() and split_it_up[1].isdigit() and split_it_up[2].isdigit():                        
            x_d = float(split_it_up[0])
            y_d = float(split_it_up[1])
            theta_d = float(split_it_up[2])*np.pi/180.0
            #updateDesiredPosition(x_d,y_d,theta_d)
            correctInput = True
        else:
            print('I\'m sorry, but that input was not understood.\n')
            print('To exit type \'q\'. \r')
            print('To enter posisitonal data type x y theta. For example: 1 0 0')
    return exitMode


def test():
    hello = 'hi'
    # print('hi')



if __name__ == "__main__": 
    mode = ''
    while mode != Controller.exitMode:  
        mode = get_mode()
        if mode == Controller.posMode:          # Positional Control
            t = RT.RepeatedTimer(P.Ts,ctrl.updateRobotMovement)

        exitMode = False  
        
        
        while mode != Controller.exitMode and not exitMode:
            if mode == Controller.velMode:          
                exitMode = velocityControl()
                time.sleep(0.5)
            elif mode == Controller.posMode:
                exitMode = positionControl()
                time.sleep(0.5)
            else:
                exitMode = True

        if mode == Controller.posMode:
            t.stop()
        
    







# KeyASCII      ASCII   Common Name
# K_BACKSPACE   \b      backspace
# K_TAB         \t      tab
# K_CLEAR               clear
# K_RETURN      \r      return
# K_PAUSE               pause
# K_ESCAPE      ^[      escape
# K_SPACE               space
# K_EXCLAIM     !       exclaim
# K_QUOTEDBL    "       quotedbl
# K_HASH        #       hash
# K_DOLLAR      $       dollar
# K_AMPERSAND   &       ampersand
# K_QUOTE               quote
# K_LEFTPAREN   (       left parenthesis
# K_RIGHTPAREN  )       right parenthesis
# K_ASTERISK    *       asterisk
# K_PLUS        +       plus sign
# K_COMMA       ,       comma
# K_MINUS       -       minus sign
# K_PERIOD      .       period
# K_SLASH       /       forward slash
# K_0           0       0
# K_1           1       1
# K_2           2       2
# K_3           3       3
# K_4           4       4
# K_5           5       5
# K_6           6       6
# K_7           7       7
# K_8           8       8
# K_9           9       9
# K_COLON       :       colon
# K_SEMICOLON   ;       semicolon
# K_LESS        <       less-than sign
# K_EQUALS      =       equals sign
# K_GREATER     >       greater-than sign
# K_QUESTION    ?       question mark
# K_AT          @       at
# K_LEFTBRACKET [       left bracket
# K_BACKSLASH   \       backslash
# K_RIGHTBRACKET ]      right bracket
# K_CARET       ^       caret
# K_UNDERSCORE  _       underscore
# K_BACKQUOTE   `       grave
# K_a           a       a
# K_b           b       b
# K_c           c       c
# K_d           d       d
# K_e           e       e
# K_f           f       f
# K_g           g       g
# K_h           h       h
# K_i           i       i
# K_j           j       j
# K_k           k       k
# K_l           l       l
# K_m           m       m
# K_n           n       n
# K_o           o       o
# K_p           p       p
# K_q           q       q
# K_r           r       r
# K_s           s       s
# K_t           t       t
# K_u           u       u
# K_v           v       v
# K_w           w       w
# K_x           x       x
# K_y           y       y
# K_z           z       z
# K_DELETE              delete
# K_KP0                 keypad 0
# K_KP1                 keypad 1
# K_KP2                 keypad 2
# K_KP3                 keypad 3
# K_KP4                 keypad 4
# K_KP5                 keypad 5
# K_KP6                 keypad 6
# K_KP7                 keypad 7
# K_KP8                 keypad 8
# K_KP9                 keypad 9
# K_KP_PERIOD   .       keypad period
# K_KP_DIVIDE   /       keypad divide
# K_KP_MULTIPLY *       keypad multiply
# K_KP_MINUS    -       keypad minus
# K_KP_PLUS     +       keypad plus
# K_KP_ENTER    \r      keypad enter
# K_KP_EQUALS   =       keypad equals
# K_UP                  up arrow
# K_DOWN                down arrow
# K_RIGHT               right arrow
# K_LEFT                left arrow
# K_INSERT              insert
# K_HOME                home
# K_END                 end
# K_PAGEUP              page up
# K_PAGEDOWN            page down
# K_F1                  F1
# K_F2                  F2
# K_F3                  F3
# K_F4                  F4
# K_F5                  F5
# K_F6                  F6
# K_F7                  F7
# K_F8                  F8
# K_F9                  F9
# K_F10                 F10
# K_F11                 F11
# K_F12                 F12
# K_F13                 F13
# K_F14                 F14
# K_F15                 F15
# K_NUMLOCK             numlock
# K_CAPSLOCK            capslock
# K_SCROLLOCK           scrollock
# K_RSHIFT              right shift
# K_LSHIFT              left shift
# K_RCTRL               right ctrl
# K_LCTRL               left ctrl
# K_RALT                right alt
# K_LALT                left alt
# K_RMETA               right meta
# K_LMETA               left meta
# K_LSUPER              left windows key
# K_RSUPER              right windows key
# K_MODE                mode shift
# K_HELP                help
# K_PRINT               print screen
# K_SYSREQ              sysrq
# K_BREAK               break
# K_MENU                menu
# K_POWER               power
# K_EURO                euro
