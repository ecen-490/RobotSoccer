import numpy as np 
import numpy.linalg as linalg
import Queue as Queue
import time
from Param2 import P, x, y , theta

_PTs = 0.1                              # prediction time step, s
_PN = 10                                # Number of iterations in every prediction state update     
_camera_sample_rate = 1
_N = 1

# Noise level on camera
_camera_sigma_ball = 0.02               # units in meters
_camera_sigma_robot_position = 0.02     # units in meters
_camera_sigma_robot_angle = 2*np.pi/180 # units in radians


class wall:
    def __init__(self):
        self.type = 'Wall'
        self.Ymax = 1.82
        self.Xmax = 1.33


class robotHome:

    def __init__(self):
        self.type = 'RobotHome'
        self.Q = 0.01*np.diag([1,                                            # Robot Home x position model variance
                          1,                                                 # Robot Home y position model variance
                         (20*np.pi/180)**2])                                 # Robot Home theta position model variance
        self.R = np.diag([_camera_sigma_robot_position**2,                   # Covariance matrix for the sensor (camera)
                          _camera_sigma_robot_position**2,
                          _camera_sigma_robot_angle**2])
        self.xhat = np.matrix([[0.0],                                        # Robot Home x position
            [0.0],                                                           # Robot Home y position
            [0.0]])                                                          # Robot Home theta position

        # States used for prediction
        self.phat = np.matrix([[0.0],               # Robot Home x position, m
            [0.0],                                  # Robot Home y position, m
            [0.0],                                  # Robot Home theta position, m
            [0.0],                                  # Robot Home x velocity, m/s
            [0.0],                                  # Robot Home y velocity, m/s
            [0.0],                                  # Robot Home theta velocity, m/s
            [0.0],                                  # Robot Home x acceleration, m/s**2
            [0.0],                                  # Robot Home y acceleration, m/s**2
            [0.0]])                                 # Robot Home theta acceleration, m/s**2

        self.A = np.matrix([[0,0,0,1,0,0,0,0,0],
                    [0,0,0,0,1,0,0,0,0],
                    [0,0,0,0,0,1,0,0,0],
                    [0,0,0,0,0,0,1,0,0],
                    [0,0,0,0,0,0,0,1,0],
                    [0,0,0,0,0,0,0,0,1],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0],
                    [0,0,0,0,0,0,0,0,0]])

        self.S = np.diag([.1,                                                # Robot Home initial x position variance
                          .1,                                                # Robot Home initial y position variance
                          .1])                                               # Robot Home initial theta position variance

        self.v_command = np.matrix([[0.0],                                   # Robot Home x velocity command
                                    [0.0],                                   # Robot Home y velocity command
                                    [0.0]])                                  # Robot Home theta velocity command

        self.Ts = P.Ts                                                       # Time update for kalman filter. This should be the same
                                                                             # As the time update used on the observer for motion control
        self.PTs = _PTs                                                      # Prediction time step, s
        self.xhat_past = Queue.Queue(500)                                    # Stores the states past values
        self.u_past = Queue.Queue(500)                                       # Stores past input values
        self.S_past = Queue.Queue(500)                                       # Stores past values
        self.pos_measured = np.matrix([[0.0],[0.0],[0.0]])                   # This is the position measured from the camera
        self.flag_pos_update = False                                         # Indicates that positional data came in
        self.deltaTC = 0.0                                                   # Time delay between 
        self.flag_set_prediction_states = False

        self.x = 0.0                                                         # x position
        self.y = 0.0                                                         # y position
        self.theta = 0.0                                                     # theta position

        self.theta_past = 0.0


    def predict_states(self):
        """ Increments the predicted states by 1 time step
        :return:
        """
        if self.flag_set_prediction_states :              # At initial run, the current states are passes to the predicted states
            self.phat[0:3,0] = self.xhat
            self.phat[3:6,0] = self.v_command
            self.phat[6:9,0] = np.matrix([[x.acceleration],[y.acceleration],[theta.acceleration]])
            self.flag_set_prediction_states = False
                    # prediction step between measurments
        
        for i in range(0,_PN):
            self.phat = self.phat + self.PTs/_PN*self.A*self.phat


    def init_pos(self,x,y,theta):
        self.xhat[0,0] = x                # Initiate the x, y and theta position at start of game
        self.xhat[1,0] = y

        if theta > np.pi:
            theta =theta - 2*np.pi
        self.xhat[2,0] = theta
        self.theta_past = theta

        self.x = x
        self.y = y
        self.theta = theta


    def update_pos_measured(self,measured_positions,TC): # Once the camera has processed the data, this function must be called to update 
        self.pos_measured = measured_positions           # the measured positions. THIS FUNCTION MUST BE CALLED BEFORE THE UPDATE_POS FUNCTION
        self.account_for_periodicity()
        self.flag_pos_update = True
        self.deltaTC = TC

    def account_for_periodicity(self):
        #print('tehta past', self.theta_past)
        #print('theta new', self.pos_measured[2,0])
        # while (self.theta_past - self.pos_measured[2,0]) > np.pi:
        #     self.pos_measured[2,0] = self.pos_measured[2,0] + 2*np.pi
        # while (self.theta_past - self.pos_measured[2,0]) < np.pi:
        #     self.pos_measured[2,0] = self.pos_measured[2,0] - 2*np.pi

        if abs(self.theta_past - self.pos_measured[2,0]) > np.pi:
            if self.theta_past > self.pos_measured[2,0]:
                self.pos_measured[2,0] = self.pos_measured[2,0] + 2*np.pi
            else:
                self.pos_measured[2,0] = self.pos_measured[2,0] - 2*np.pi

        self.theta_past = self.pos_measured[2,0]

    def map_theta(self):
        while self.theta > 2*np.pi:
            self.theta = self.theta - 2*np.pi

        while self.theta < 0:
            self.theta = self.theta + 2*np.pi


    def update_v_command(self,v_command):
        #print("v_command", v_command)
        self.v_command = v_command

    def kalman_filter(self):
        #print("kalman filter begin")
        # prediction step between measurments

        u = self.v_command
        #print('u', u)
        #print('ts', self.Ts)
        for i in range(0,_N):
            self.xhat = self.xhat + self.Ts/_N*u
            self.S = self.S + self.Ts/_N*self.Q

        #print('kalman states', self.xhat)

        self.xhat_past.put(self.xhat)
        self.S_past.put(self.S)
        self.u_past.put(u)

        #print("kalman filter spot 2", self.u_past.qsize())

        # correction step at measurment
        if self.flag_pos_update :                                           # Checks to see if a positional update came in
            #print("update flag detected")
            deltaTQ = float(self.u_past.qsize() * self.Ts)                  # Computes the time since last correction update
            num_throw_away = int((deltaTQ - self.deltaTC)/self.Ts-1)        # Number of items to throw out of the queue
            # print("deltaTQ", deltaTQ)
            # print("deltaTC", self.deltaTC)
            # print("throw away ", num_throw_away)
            # print("u q size ", self.u_past.qsize())
            # print("s q size ", self.S_past.qsize())
            # print("xhat q size ", self.xhat_past.qsize())

            while(num_throw_away > 0):                                      # Removes elements from the queue that represents data
                self.u_past.get()                                           # Calculated before the camera picture was taken.
                self.S_past.get()
                self.xhat_past.get()
                num_throw_away = num_throw_away - 1



            self.xhat = self.xhat_past.get()                                # Sets xhat to xhat_past
            self.S = self.S_past.get()                                      # Sets S to S_past

            #print(self.u_past.qsize())

            while not self.xhat_past.empty():
                self.S_past.get()                                           # Empty the Queue
                self.xhat_past.get()                                        # Empty the Queue
            #print("S", self.S)
            ##print("R", self.R)
            self.L = self.S*linalg.inv(self.R+self.S)                       # Compute the correction parameter
            #print("L", self.L)
            self.S = (np.eye(3) - self.L)*self.S                      # Compute the covariance parameter
            self.xhat = self.xhat + self.L*(self.pos_measured - self.xhat)  # Makes the positional correction
            while not self.u_past.empty():                                  # Needs to catch up to current time by iterating through the
               # print("q size", self.u_past.qsize())                       # past input values from when xhat_past was locked up till present time.

                u = self.u_past.get()                                       # Grabs the earliest input value from the queue
                for i in range(0,_N):                                        # Updates xhat with u as input
                    self.xhat = self.xhat + self.Ts/_N*u
                    self.S = self.S + self.Ts/_N*self.Q
            self.flag_pos_update = False                                    # Cancels flag
           # print("finish")

        self.x = self.xhat[0,0]
        self.y = self.xhat[1,0]
        self.theta = self.xhat[2,0]
        self.theta_past = self.xhat[2,0]
        self.map_theta()
        #print('theta', self.theta)


class Ball:

    def __init__(self):
        self.Type = 'Ball'
        self.Q = 0.01**2*np.diag([.001**2,        # Ball x position model variance
            .001**2,                              # Ball y position model variance
            .01**2,                               # Ball x velocity model variance
            .01**2,                               # Ball y velocity model variance
            .1**2,                                # Ball x acceleration model variance
            .1**2,                                # Ball y acceleration model variance
            10**2,                                # Ball x jerk model variance
            10**2])                               # Ball y jerk model variance


        self.R = np.diag([_camera_sigma_ball**2,    # Camera x position variance
            _camera_sigma_ball**2])                 # Camera y position variance

        self.A = np.matrix([[0,0,1,0,0,0,0,0],
                            [0,0,0,1,0,0,0,0],
                            [0,0,0,0,1,0,0,0],
                            [0,0,0,0,0,1,0,0],
                            [0,0,0,0,0,0,1,0],
                            [0,0,0,0,0,0,0,1],
                            [0,0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0,0]])

        self.C = np.matrix([[1,0,0,0,0,0,0,0],
                           [0,1,0,0,0,0,0,0]])

        self.xhat = np.matrix([[0.0],               # Ball x position, m
            [0.0],                                  # Ball y position, m
            [0.0],                                  # Ball x velocity, m/s
            [0.0],                                  # Ball y velocity, m/s
            [0.0],                                  # Ball x acceleration, m/s**2
            [0.0],                                  # Ball y acceleration, m/s**2
            [0.0],                                  # Ball x jerk, m/s**3
            [0.0]])                                 # Ball y jerk, m/s**3

        # States used for prediction
        self.phat = np.matrix([[0.0],               # Ball x position, m
            [0.0],                                  # Ball y position, m
            [0.0],                                  # Ball x velocity, m/s
            [0.0],                                  # Ball y velocity, m/s
            [0.0],                                  # Ball x acceleration, m/s**2
            [0.0],                                  # Ball y acceleration, m/s**2
            [0.0],                                  # Ball x jerk, m/s**3
            [0.0]])                                 # Ball y jerk, m/s**3

        self.S = np.diag([1,                      # Initial variance of x-position of ball
            1,                                    # Initial variance of y-position of ball
            .01,                                  # Initial variance of x-velocity of ball
            .01,                                  # Initial variance of y-velocity of ball
            .001,                                 # Initial variance of x-acceleration of ball
            .001,                                 # Initial variance of y-acceleration of ball
            .0001,                                # Initial variance of x-jerk of ball
            .0001])                               # Initial variance of y-jerk of ball


        # print('S',self.S)
        # print('Q',self.Q)
        # print('A',self.A)


        self.Ts = P.Ts                                                       # Time update for kalman filter. This should be the same
                                                                             # As the time update used on the observer for motion control
        self.PTs = _PTs                                                      # Prediction time step, s
        self.xhat_past = Queue.Queue(500)                                    # Stores past input values
        self.S_past = Queue.Queue(500)                                       # Stores past values
        self.pos_measured = np.matrix([[0.0],[0.0]])                         # This is the state measured from the camera
        self.flag_pos_update = False                                         # Indicates that positional data came in
        self.deltaTC = 0.0                                                   # Time delay between 
        self.flag_set_prediction_states = False

        self.x = 0.0                                                         # y pos
        self.y = 0.0                                                         # x pos

    def predict_states(self): # increments the predicted states by 1 time step
        if self.flag_set_prediction_states :              # At initial run, the current states are passes to the predicted states
            self.phat = self.xhat
            self.flag_set_prediction_states = False
                    # prediction step between measurments
        
        for i in range(0,_PN):
            self.phat = self.phat + self.PTs/_PN*self.A*self.phat

    def init_pos(self,x,y):
        self.xhat[0,0] = x                # Initiate the x and y position at start of game
        self.xhat[1,0] = y
        self.x = x
        self.y = y

    def update_pos_measured(self,measured_positions,TC): # Once the camera has processed the data, this function must be called to update 
        self.pos_measured = measured_positions           # the measured positions. THIS FUNCTION MUST BE CALLED BEFORE THE UPDATE_POS FUNCTION
        self.flag_pos_update = True
        self.deltaTC = TC

    def kalman_filter(self):
        #print("kalman filter begin")
        # prediction step between measurments

        for i in range(0,_N):
            self.xhat = self.xhat + self.Ts/_N*self.A*self.xhat
            self.S = self.S + self.Ts/_N*(self.A*self.S + self.S*self.A.transpose() + self.Q)

        # self.ballWallBounce()
        self.xhat_past.put(self.xhat)
        self.S_past.put(self.S)

        #print("kalman filter spot 2", self.u_past.qsize())

        # correction step at measurment
        if self.flag_pos_update :                                           # Checks to see if a positional update came in
            #print("update flag detected")
            deltaTQ = float(self.S_past.qsize() * self.Ts)                  # Computes the time since last correction update
            num_throw_away = int((deltaTQ - self.deltaTC)/self.Ts-1)        # Number of items to throw out of the queue
            # print("deltaTQ", deltaTQ)
            # print("deltaTC", self.deltaTC)
            # print("throw away ", num_throw_away)
            while(num_throw_away > 0):                                      # Removes elements from the queue that represents data
                                                                            # Calculated before the camera picture was taken.
                self.S_past.get()
                self.xhat_past.get()
                num_throw_away = num_throw_away - 1


            # print("s q size ", self.S_past.qsize())
            # print("xhat q size ", self.xhat_past.qsize())


            self.xhat = self.xhat_past.get()                                # Sets xhat to xhat_past
            self.S = self.S_past.get()   
            #print("S", self.S)
            ##print("R", self.R)
            self.L = self.S*self.C.transpose()*linalg.inv(self.R+self.C*self.S*self.C.transpose())    # Compute the correction parameter
            #print("L", self.L)
            #self.S = (np.identity(3) - self.L)*self.S                      # Compute the covariance parameter
            self.S = (np.eye(8)-self.L*self.C)*self.S                           # Sets S to S_past
            self.xhat = self.xhat + self.L*(self.pos_measured - self.C*self.xhat)  # Makes the positional correction  
           

            while not self.xhat_past.empty(): 
                self.S_past.get()                                           # Empty the Queue
                self.xhat_past.get()                                        # Empty the Queue 
                for i in range(0,_N):
                    self.xhat = self.xhat + self.Ts/_N*self.A*self.xhat
                    # self.ballWallBounce()
                    self.S = self.S + self.Ts/_N*(self.A*self.S + self.S*self.A.transpose() + self.Q)


            self.flag_pos_update = False                                    # Cancels flag
           # print("finish")

        self.x = self.xhat[0,0]
        self.y = self.xhat[1,0]

    def ballWallBounce(self,Wall):
        # Assumes that it is perfectly elastic collision
        # self is the ball bouncing off of the wall
        # Wall is the class Wall that states the max X and Y position
        if abs(self.xhat[0,0]) >= Wall.Xmax:  # self has hit the wall's x-boundary
            # Position and velocity correction
            self.xhat[0,0] = self.xhat[0,0] - self.xhat[2,0]*self.Ts
            self.xhat[2,0] = -self.xhat[2,0]
            self.xhat[0,0] = self.xhat[0,0] + self.xhat[2,0]*self.Ts
        elif abs(self.xhat[1,0]) >= Wall.Ymax: # self has hit the wall's y-boundary
            # Position and velocity correction
            self.xhat[1,0] = self.xhat[1,0] - self.xhat[3,0]*self.Ts
            self.xhat[3,0] = -self.xhat[3,0]
            self.xhat[1,0] = self.xhat[1,0] + self.xhat[3,0]*self.Ts
        # else:

            # No correction is needed. Do nothing


class robotAway:
    def __init__(self):
        self.type = 'RobotAway'
        self.Q = np.diag([.001**2,        # Robot Away x position model variance
                          .001**2,        # Robot Away y position model variance
                          .01**2,         # Robot Away theta model variance
                          .1**2,          # Robot Away x velocity model variance
                          .1**2,          # Robot Away y velocity model variance
                          .1**2,          # Robot Away theta velocity model variance
                          1**2,           # Robot Away x acceleration model variance
                          1**2,           # Robot Away y acceleration model variance
                          20*np.pi/180])   # Robot Away theta acceleration model variance




        self.R = np.diag([_camera_sigma_robot_position**2,    # Camera x position variance
            _camera_sigma_robot_position**2,                  # Camera y position variance
            _camera_sigma_robot_angle])                       # Camera theta position variance

        self.A = np.matrix([[0,0,0,1,0,0,0,0,0],
                            [0,0,0,0,1,0,0,0,0],
                            [0,0,0,0,0,1,0,0,0],
                            [0,0,0,0,0,0,1,0,0],
                            [0,0,0,0,0,0,0,1,0],
                            [0,0,0,0,0,0,0,0,1],
                            [0,0,0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0,0,0],
                            [0,0,0,0,0,0,0,0,0]])

        self.C = np.matrix([[1,0,0,0,0,0,0,0,0],
                           [0,1,0,0,0,0,0,0,0],
                           [0,0,1,0,0,0,0,0,0]])

        self.xhat = np.matrix([[0.0],               # Robot Away x position, m
            [0.0],                                  # Robot Away y position, m
            [0.0],                                  # Robot Away theta position, m
            [0.0],                                  # Robot Away x velocity, m/s
            [0.0],                                  # Robot Away y velocity, m/s
            [0.0],                                  # Robot Away theta velocity, m/s
            [0.0],                                  # Robot Away x acceleration, m/s**2
            [0.0],                                  # Robot Away y acceleration, m/s**2
            [0.0]])                                 # Robot Away theta acceleration, m/s**2

        # States used for prediction
        self.phat = np.matrix([[0.0],               # Robot Away x position, m
            [0.0],                                  # Robot Away y position, m
            [0.0],                                  # Robot Away theta position, m
            [0.0],                                  # Robot Away x velocity, m/s
            [0.0],                                  # Robot Away y velocity, m/s
            [0.0],                                  # Robot Away theta velocity, m/s
            [0.0],                                  # Robot Away x acceleration, m/s**2
            [0.0],                                  # Robot Away y acceleration, m/s**2
            [0.0]])                                 # Robot Away theta acceleration, m/s**2

        self.S = np.diag([0.1,                      # Initial variance of x-position of Robot Away
            .1,                                     # Initial variance of y-position of Robot Away
            .1,                                     # Initial variance of the theta position of Robot Away
            .01,                                    # Initial variance of x-velocity of Robot Away
            .01,                                    # Initial variance of y-velocity of Robot Away
            .01,                                    # Initial variance of theta velocity of Robot Away
            .001,                                   # Initial variance of x-acceleration of Robot Away
            .001,                                   # Initial variance of y-acceleration of Robot Away
            .001])                                  # Initial variance of theta acceleration of Robot Away


        self.Ts = P.Ts                                                       # Time update for kalman filter. This should be the same
                                                                             # As the time update used on the observer for motion control
        self.PTs = _PTs
        self.xhat_past = Queue.Queue(500)                                    # Stores past input values
        self.S_past = Queue.Queue(500)                                       # Stores past values
        self.pos_measured = np.matrix([[0.0],[0.0]])                         # This is the state measured from the camera
        self.flag_pos_update = False                                         # Indicates that positional data came in
        self.deltaTC = 0.0                                                   # Time delay between 
        self.flag_set_prediction_states = False
        self.x = 0.0                                                         # x pos
        self.y = 0.0                                                         # y pos
        self.theta = 0.0                                                     # tehta pos

        self.theta_past = 0.0

    def predict_states(self): # increments the predicted states by 1 time step
        if self.flag_set_prediction_states :              # At initial run, the current states are passes to the predicted states
            self.phat = self.xhat
            self.flag_set_prediction_states = False
                    # prediction step between measurments
        
        for i in range(0,_PN):
            self.phat = self.phat + self.PTs/_PN*self.A*self.phat

    def init_pos(self,x,y,theta):
        self.xhat[0,0] = x                # Initiate the x, y and theta position at start of game
        self.xhat[1,0] = y
        self.xhat[2,0] = theta
        self.x = x
        self.y = y
        self.theta = theta

    def update_pos_measured(self,measured_positions,TC): # Once the camera has processed the data, this function must be called to update 
        self.pos_measured = measured_positions           # the measured positions. THIS FUNCTION MUST BE CALLED BEFORE THE UPDATE_POS FUNCTION
        self.account_for_periodicity()
        self.flag_pos_update = True
        self.deltaTC = TC

    def account_for_periodicity(self):
        #print('tehta past', self.theta_past)
        #print('theta new', self.pos_measured[2,0])
        # while (self.theta_past - self.pos_measured[2,0]) > np.pi:
        #     self.pos_measured[2,0] = self.pos_measured[2,0] + 2*np.pi
        # while (self.theta_past - self.pos_measured[2,0]) < np.pi:
        #     self.pos_measured[2,0] = self.pos_measured[2,0] - 2*np.pi

        if abs(self.theta_past - self.pos_measured[2,0]) > np.pi:
            if self.theta_past > self.pos_measured[2,0]:
                self.pos_measured[2,0] = self.pos_measured[2,0] + 2*np.pi
            else:
                self.pos_measured[2,0] = self.pos_measured[2,0] - 2*np.pi

        self.theta_past = self.pos_measured[2,0]

    def kalman_filter(self):
        # prediction step between measurments

        for i in range(0,_N):
            self.xhat = self.xhat + self.Ts/_N*self.A*self.xhat
            self.S = self.S + self.Ts/_N*(self.A*self.S + self.S*self.A.transpose() + self.Q)

        self.xhat_past.put(self.xhat) # Store new xhat to be used when camera data comes in
        self.S_past.put(self.S)       # Store new S to be used when camera data comes in

        

        # correction step at measurment
        if self.flag_pos_update :                                           # Checks to see if a positional update came in
            deltaTQ = float(self.S_past.qsize() * self.Ts)                  # Computes the time since last correction update
            num_throw_away = int((deltaTQ - self.deltaTC)/self.Ts-1)        # Number of items to throw out of the queue

            while(num_throw_away > 0):                                      # Removes elements from the queue that represents data
                self.S_past.get()                                           # Calculated before the camera picture was taken.
                self.xhat_past.get()
                num_throw_away = num_throw_away - 1


            self.xhat = self.xhat_past.get()                                # Sets xhat to the xhat when the camera picture was taken
            self.S = self.S_past.get()                                      # Sets S to the S when the camera picture was taken
            self.L = self.S*self.C.transpose()*linalg.inv(self.R+self.C*self.S*self.C.transpose())    # Compute the correction parameter
            self.S = (np.eye(9)-self.L*self.C)*self.S                           # Compute the covariance parameter
            self.xhat = self.xhat + self.L*(self.pos_measured - self.C*self.xhat)  # Makes the positional correction  
           

            while not self.xhat_past.empty(): 
                self.S_past.get()                                           # Empty the Queue
                self.xhat_past.get()                                        # Empty the Queue 
                for i in range(0,_N):
                    self.xhat = self.xhat + self.Ts/_N*self.A*self.xhat
                    self.S = self.S + self.Ts/_N*(self.A*self.S + self.S*self.A.transpose() + self.Q)


            self.flag_pos_update = False                                    # Cancels flag

        self.x = self.xhat[0,0]
        self.y = self.xhat[1,0]
        self.theta = self.xhat[2,0]


#RH1 = robotHome()
#RA1 = robotAway()
Wall = wall()
Vc = np.matrix([[1],[1],[0.5]])


#b = Ball()
V_ball = 1

#print(R1.Q)

if __name__ == "__main__": 
    print("main")

    def test_ball_kalmanFilter():
        # Does not take into account wall bounces. You will need to comment that out when testing with this code
        print("ball test")
        measured_position = np.matrix([[0.0],[0.0]])
        for i in range(0,1021):
            if i == 120 or i == 220 or i == 320 or i == 420 or i == 520 or i == 620 or i ==720 or i == 820 or i == 920 or i == 1020:
                measured_position = np.matrix([[V_ball*(i-20)*b.Ts],[V_ball*(i-20)*b.Ts]])
                #print("should be", measured_position)
                b.update_pos_measured(measured_position, 20.0*b.Ts)
            b.kalman_filter()
        print("xhat",b.xhat[0:2,0]) 
        print("should be", np.matrix([[V_ball*(i)*b.Ts],[V_ball*(i)*b.Ts]]))


    def test_RobotHome_kalmanFilter():
        print("RobotHome test")
        for i in range(0,521):
            RH1.update_v_command(Vc)
            if i == 120 or i == 220 or i == 320 or i == 420 or i == 520:
                measured_posistion = np.matrix([[(i-20)*Vc[0,0]*RH1.Ts],[(i-20)*Vc[1,0]*RH1.Ts],[(i-20)*Vc[2,0]*RH1.Ts]])
                RH1.update_pos_measured(measured_posistion,20.0*RH1.Ts)
                #print("xhat", R1.xhat)
            RH1.kalman_filter()
        print("xhat", RH1.xhat)
        print("should be", np.matrix([[Vc[0,0]*(i)*RH1.Ts],[Vc[1,0]*(i)*RH1.Ts],[Vc[2,0]*i*RH1.Ts]]))
            
    def test_RobotAway_kalmanFilter():
        print("RobotAway test")
        for i in range(0,1021):
            if i == 120 or i == 220 or i == 320 or i == 420 or i == 520 or i == 620 or i ==720 or i == 820 or i == 920 or i == 1020:
                measured_posistion = np.matrix([[(i-20)*Vc[0,0]*RA1.Ts],[(i-20)*Vc[1,0]*RA1.Ts],[(i-20)*Vc[2,0]*RA1.Ts]])
                RA1.update_pos_measured(measured_posistion,20.0*RA1.Ts)
                #print("xhat", R1.xhat)
            RA1.kalman_filter()
        print("xhat", RA1.xhat)
        print("should be", np.matrix([[Vc[0,0]*(i)*RA1.Ts],[Vc[1,0]*(i)*RA1.Ts],[Vc[2,0]*i*RA1.Ts]]))

    def ball_bounce_test():
        test = 3  # Selects which test to run

        # Ball moves in the x direction to the object
        if test == 0:
            b.xhat[2,0] = 1     # Set x velocity of ball
            b.xhat[3,0] = 0     # Set y velocity of ball
            Ox = 1              # Set x position of object
            Oy = 0              # Set y position of object
            r = 0.5             # Radius of the object

            psi = np.arange(0,np.pi,np.pi/10) # The angle between the object's center and the collision point with +y being 0 degrees
            for i in range(0,len(psi)):
                print('psi',psi[i]*180/np.pi)
                Cx = Ox - r*np.sin(psi[i])              # Set x position of collision point
                #print('Cx',Cx)
                Cy = Oy + r*np.cos(psi[i])              # Set y position of collision point
                b.ballBounce(Cx,Cy,Ox,Oy)

        # Ball moves in the -x direction to the object
        elif test == 1:
            b.xhat[2,0] = -1   # Set x velocity of the ball
            b.xhat[3,0] = 0    # Set y velocity of the ball
            Ox = 0             # Set x position of objcet
            Oy = 1             # Set y position of object
            r = 0.5            # Radius of the object
            psi = np.arange(0,np.pi,np.pi/10) # The angle between the object's center and the collision point with +y being 0 degrees
            for i in range(0,len(psi)):
                print('psi',psi[i]*180/np.pi)
                Cx = Ox + r*np.sin(psi[i])              # Set x position of collision point
                #print('Cx',Cx)
                Cy = Oy + r*np.cos(psi[i])              # Set y position of collision point
                b.ballBounce(Cx,Cy,Ox,Oy)

        # Ball moves in the +y direction to the object
        elif test == 2:
            b.xhat[2,0] = 0    # Set x velocity of the ball
            b.xhat[3,0] = 1    # Set y velocity of the ball
            Ox = 1             # Set x position of objcet
            Oy = 1             # Set y position of object
            r = 0.5            # Radius of the object
            psi = np.arange(0,np.pi,np.pi/10) # The angle between the object's center and the collision point with +y being 0 degrees
            for i in range(0,len(psi)):
                print('psi',psi[i]*180/np.pi)
                Cx = Ox + r*np.cos(psi[i])              # Set x position of collision point
                #print('Cx',Cx)
                Cy = Oy - r*np.sin(psi[i])              # Set y position of collision point
                b.ballBounce(Cx,Cy,Ox,Oy)

        # Ball moves in the -y direction to the object
        elif test == 3:
            b.xhat[2,0] = 0    # Set x velocity of the ball
            b.xhat[3,0] = -1   # Set y velocity of the ball
            Ox = 1             # Set x position of objcet
            Oy = 1             # Set y position of object
            r = 0.5            # Radius of the object
            psi = np.arange(0,np.pi,np.pi/10) # The angle between the object's center and the collision point with +y being 0 degrees
            for i in range(0,len(psi)):
                print('psi',psi[i]*180/np.pi)
                Cx = Ox + r*np.cos(psi[i])              # Set x position of collision point
                #print('Cx',Cx)
                Cy = Oy + r*np.sin(psi[i])              # Set y position of collision point
                b.ballBounce(Cx,Cy,Ox,Oy)

        else:
            print('Test selection not understood')
            

    def ball_wall_bounce_test():
        # Ball bounces off of the walls +Y boundary
        b.xhat[0,0] = 0
        b.xhat[1,0] = Wall.Ymax + 0.01
        b.xhat[2,0] = 0
        b.xhat[3,0] = 1
        b.ballWallBounce()
        print('ball state', b.xhat)

        # Ball bounces off of the walls -Y boundary
        b.xhat[0,0] = 0
        b.xhat[1,0] = -Wall.Ymax - 0.01
        b.xhat[2,0] = 0
        b.xhat[3,0] = -1
        b.ballWallBounce()
        print('ball state', b.xhat)

        # Ball bounces off of the walls +X boundary
        b.xhat[0,0] = Wall.Xmax + 0.01
        b.xhat[1,0] = 0
        b.xhat[2,0] = 1
        b.xhat[3,0] = 0
        b.ballWallBounce()
        print('ball state', b.xhat)

        # Ball bounces off of the walls -x boundary
        b.xhat[0,0] = -Wall.Xmax -0.01
        b.xhat[1,0] = 0
        b.xhat[2,0] = -1
        b.xhat[3,0] = 0
        b.ballWallBounce()
        print('ball state', b.xhat)

    test_ball_kalmanFilter()
    # ball_bounce_test()
    # test_RobotAway_kalmanFilter()
    # ball_wall_bounce_test()


