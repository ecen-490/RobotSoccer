% main control code - assumes full state knowledge
%
% M2CK - Mark Roberts, Carson Bunker, Kyle Richey, Mark Petersen
% Modified: 
%   2/11/2014 - R. Beard
%   2/18/2014 - R. Beard
%   2/24/2014 - R. Beard
%   1/4/2016  - R. Beard
%
% this first function catches simulink errors and displays the line number
function v_c=controller_home(uu,P)
    try
        v_c=controller_home_(uu,P);
    catch e
        msgString = getReport(e);
        fprintf(2,'\n%s\n',msgString);
        rethrow(e);
    end
end

% main control function
function v_c=controller_home_(uu,P)

    % process inputs to function
    % robots - own team
    for i=1:P.num_robots,
        robot(:,i)   = uu(1+3*(i-1):3+3*(i-1));
    end
    NN = 3*P.num_robots;
    % robots - opponent
    for i=1:P.num_robots,
        opponent(:,i)   = uu(1+3*(i-1)+NN:3+3*(i-1)+NN);
    end
    NN = NN + 3*P.num_robots;
    % ball
    ball = [uu(1+NN); uu(2+NN)];
    NN = NN + 2;
    % score: own team is score(1), opponent is score(2)
    score = [uu(1+NN); uu(2+NN)];
    NN = NN + 2;
    % current time
    t = uu(1+NN);
    
    v_c = strategy_switch_based_on_ball_position(robot, opponent, ball, P, t, uu);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%% Strategies %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function v_c = strategy_switch_based_on_ball_position(robot, opponent, ball, P, t, uu)
    persistent robot1Play;
    persistent robot2Play;
    persistent oldBallPosition; % x, y, time
    
    % Strategy defines
    left = 0;
    middle = 1;
    right = 2;
    offense = 0;
    defense = 1;
    
    % Initialize persistent variables
    if(t == 0)
        robot1Play = offense;
        robot2Play = defense;
        oldBallPosition = [ball(1) ball(2), 0];
    end
    
    % Variables    
    robot1 = robot(:,1);
    robot2 = robot(:,2);
    ballSection = 0;
    ball_velocity = [0 0];
    
    % Find the velocity of the ball
    ball_velocity(1) = (oldBallPosition(1) - ball(1))/(t - oldBallPosition(3));
    ball_velocity(2) = (oldBallPosition(2) - ball(2))/(t - oldBallPosition(3));
    
    % Find section ball is located in
    if(ball(1) > P.field_length > 9)
        ballSection = right;
    elseif(ball(1) < (-P.field_length/4))
        ballSection = left;
    else 
        ballSection = middle;
    end
    
    % Get robot distance to goal
    robot1ToGoal = utility_calc_distance(robot1, P.goal);
    robot2ToGoal = utility_calc_distance(robot2, P.goal);
    
    % Assign plays based on ball location
    if(ballSection == left)
        if(robot1ToGoal <= robot2ToGoal)
            v1 = play_defense(robot1, ball, P);
            v2 = play_offense(robot2, ball, P);
        else
            v1 = play_offense(robot1, ball, P);
            v2 = play_defense(robot2, ball, P);
        end
    elseif(ballSection == middle)
        if(robot1ToGoal <= robot2ToGoal)
            v1 = play_defense(robot1, ball, P);
            v2 = play_offense(robot2, ball, P);
        else
            v1 = play_offense(robot1, ball, P);
            v2 = play_defense(robot2, ball, P);
        end
    else % ballSection == right
        v1 = play_offense(robot1, ball, P);
        v2 = play_offense(robot2, ball, P);
    end
    
    % Saturate velocity on both robots
    v1 = utility_saturate_velocity(v1,P);
    v2 = utility_saturate_velocity(v2,P);
    v_c = [v1; v2];
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Plays %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function v = play_defense(robot, ball, P)

    % If ball is behind the robot move back to front of goal
    if(ball(1) < (robot(1)-((P.robot_radius)/2)))
        v = skill_go_in_front_of_ball(robot, ball, P);
    else
        v = skill_defend_semicircle(robot, ball, P);
    end
end

function v = play_offense(robot, ball, P)
    % normal vector from ball to goal
    n = P.goal-ball;
    n = n/norm(n);
    % compute position 10cm behind ball, but aligned with goal.
    position = ball - 0.2*n;
    
    if norm(position-robot(1:2))<.21,
        v = skill_go_to_point(robot, P.goal, P);
    else
        v = skill_go_to_point(robot, position, P);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%% Skills %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function v = skill_go_in_front_of_ball(robot, ball, P)
    % normal vector from ball to goal
    n = P.goal-ball;
    n = n/norm(n);
    % compute position 10cm behind ball, but aligned with goal.
    position = ball - 0.2*n;
    
    % control x position to stay on current line
    vx = -P.control_k_vx*(robot(1)-position(1));
    
    % control y position to match the ball's y-position
    vy = -P.control_k_vy*(robot(2)-position(2));
    
    % control angle to face ball, but not exceeding +/- 90 degrees.
    theta_d = atan2(ball(2)-robot(2), ball(1)-robot(1));
    if theta_d >= pi/2
       theta_d = pi/2.25;  % angle pushes ball out (more effective)
    elseif theta_d <= -pi/2
        theta_d = -pi/2.25; 
    end
    omega = -P.control_k_phi*(robot(3) - theta_d); 
    
    v = [vx; vy; omega];
end

%-----------------------------------------
% skill - go to point
%   follows the y-position of the ball, while maintaining x-position at
%   x_pos.  Angle always faces the goal.

function v = skill_go_to_point(robot, point, P)

    % control x position to stay on current line
    vx = -P.control_k_vx*(robot(1)-point(1));
    
    % control y position to match the ball's y-position
    vy = -P.control_k_vy*(robot(2)-point(2));

    % control angle to -pi/2
    theta_d = atan2(P.goal(2)-robot(2), P.goal(1)-robot(1));
    omega = -P.control_k_phi*(robot(3) - theta_d);
    
    v = [vx; vy; omega];
end

function v = skill_defend_semicircle(robot, ball, P)
    % Move midway between ball and goal
    r = utility_calc_distance(-P.goal, ball)/2;    
    
    y_delta = ball(2);

    % control angle to -pi/2
    theta_d = atan2((-P.goal(2)-y_delta), -(-P.goal(1)-ball(1)));
   
    y = r * sin(theta_d);
    x = r * cos(theta_d);
    
    vx = -P.control_k_vx*(robot(1)-x);
    vy = -P.control_k_vy*(robot(2)-y);
    
    if theta_d >= pi/2
       theta_d = pi/2.25;  % angle pushes ball out (more effective)
    elseif theta_d <= -pi/2
        theta_d = -pi/2.25;
    end
    
    omega = -P.control_k_phi*(robot(3) - theta_d); 
    
    v = [vx; vy; omega];
end
%
%%%%%%%%%%%%%%%%%%%%%%%%% Utilities %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%------------------------------------------
% utility - saturate_velocity
% 	saturate the commanded velocity 
%
function v = utility_saturate_velocity(v,P)
    if v(1) >  P.robot_max_vx,    v(1) =  P.robot_max_vx;    end
    if v(1) < -P.robot_max_vx,    v(1) = -P.robot_max_vx;    end
    if v(2) >  P.robot_max_vy,    v(2) =  P.robot_max_vy;    end
    if v(2) < -P.robot_max_vy,    v(2) = -P.robot_max_vy;    end
    if v(3) >  P.robot_max_omega, v(3) =  P.robot_max_omega; end
    if v(3) < -P.robot_max_omega, v(3) = -P.robot_max_omega; end
end

%------------------------------------------
% utility - calculate distance
% 	calculate the distance between two points 
%
function distance = utility_calc_distance(point1, point2)
    distance = sqrt((point1(1) - point2(1))^2 + (point1(2) - point2(2))^2);
end

  