#!/usr/bin/env python

# Command Center for sending commands to the robot from the vision computer. Also displays a representation of the field
# by subscribing to the location service

from parameters import *
import rospy
import cPickle as pickle
from Tkinter import *
from LocationNode import CurrentLocationsService
from robotsoccer.msg import *
from robotsoccer.srv import *

REFRESH_RATE = 100


# Draws the frame representing the field
class Field(Frame):

    # Initialization
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.ball = 0
        self.ball_x = 0
        self.ball_y = 0
        self.home = 0
        self.home_x = 0
        self.home_y = 0
        self.home_theta = 0
        self.home_d = 0
        self.home_dx = 0
        self.home_dy = 0
        self.home_dtheta = 0
        self.away = 0
        self.away_x = 0
        self.away_y = 0
        self.away_theta = 0
        self.parent = parent
        self.initUI()
        rospy.init_node('commandcenter_node', anonymous=True)
        self.pub1 = rospy.Publisher('commandcenter_commands', commandcenter, queue_size=10)
        rospy.Subscriber('robot1_commands', robot1commands, self.updateDesiredLocations)

    def updateDesiredLocations(self, data):
        #print "received new desired positions"
        #print data.x
        #print data.y
        #print data.theta
        self.home_dx = meterToPixel(data.x)
        self.home_dy = meterToPixel(data.y)
        self.home_dtheta = data.theta

# Initialzies the UI
    def initUI(self):
        self.parent.title("Colors")
        self.parent.bind("<Key>", self.key)
        self.parent.bind("<Button-1>", self.callback)
        self.pack(fill=BOTH, expand=1)
        self.canvas = Canvas(self)
        # field border
        self.canvas.create_rectangle(GUI_MARGIN, GUI_MARGIN, WIDTH_FIELD + GUI_MARGIN, HEIGHT_FIELD + GUI_MARGIN)
        # center line
        self.canvas.create_line(WIDTH_FIELD/2 + GUI_MARGIN, GUI_MARGIN, WIDTH_FIELD/2 + GUI_MARGIN, HEIGHT_FIELD + GUI_MARGIN)
        # home goal
        self.canvas.create_line(GUI_MARGIN, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2,
            GUI_MARGIN + WIDTH_GOAL, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2,
            GUI_MARGIN + WIDTH_GOAL, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2 + HEIGHT_GOAL,
            GUI_MARGIN, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2 + HEIGHT_GOAL)
        # away goal
        self.canvas.create_line(GUI_MARGIN + WIDTH_FIELD, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2,
            GUI_MARGIN + WIDTH_FIELD - WIDTH_GOAL, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2,
            GUI_MARGIN + WIDTH_FIELD - WIDTH_GOAL, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2 + HEIGHT_GOAL,
            GUI_MARGIN + WIDTH_FIELD, GUI_MARGIN + (HEIGHT_FIELD - HEIGHT_GOAL)/2 + HEIGHT_GOAL)
        # center circle
        self.canvas.create_oval(GUI_MARGIN + WIDTH_FIELD/2 - HEIGHT_GOAL/2, GUI_MARGIN + HEIGHT_FIELD/2 - HEIGHT_GOAL/2,
            GUI_MARGIN + WIDTH_FIELD/2 + HEIGHT_GOAL/2, GUI_MARGIN + HEIGHT_FIELD/2 + HEIGHT_GOAL/2)
        self.ball = self.canvas.create_oval(*self.getBallCoord(), fill='yellow')
        self.home = self.canvas.create_polygon(*self.getHomeCoord(), fill='blue')
        self.home_d = self.canvas.create_polygon(*self.getHomeCoord(), fill='blue', outline='red')
        self.away = self.canvas.create_polygon(*self.getAwayCoord(), fill='red')
        self.canvas.pack(fill=BOTH, expand=1)

    # Defines what happens during key presses for the command center
    def key(self, event):
        keyPressed = event.char
        if keyPressed == 'g': # Run the "Play" strategy
            self.sendCommand(2)
        elif keyPressed == 's': # Stop
            self.sendCommand(1)
        elif keyPressed == 'c': # Go To Center
            self.sendCommand(3)
        elif keyPressed == 'p': # Go to starting position
            self.sendCommand(4)
        elif keyPressed == 't': # Run the "Test" strategy
            self.sendCommand(5)
        else:
            print "Usage: g 'play', s 'stop', c 'center', p 'start position', t 'test' "
        print "Pressed", repr(event.char)

    # Sends the command to the robot
    def sendCommand(self, command):
        msg = commandcenter()
        msg.command = command
        self.pub1.publish(msg)

    # Callback for the frame used to display the field representation
    def callback(self, event):
        self.focus_set()

    # Used to draw the ball
    def getBallCoord(self):
        return \
            (GUI_CENTER_X + self.ball_x - WIDTH_BALL/2,
             GUI_CENTER_Y - self.ball_y - WIDTH_BALL/2,
             GUI_CENTER_X + self.ball_x + WIDTH_BALL/2,
             GUI_CENTER_Y - self.ball_y + WIDTH_BALL/2)

    # Used to draw the home robots
    def getHomeCoord(self):
        return \
            (GUI_CENTER_X + self.home_x + int(math.cos(math.pi/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x + int(math.cos(math.pi/2+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi/2+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x + int(math.cos(math.pi*5/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi*5/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x + int(math.cos(math.pi*7/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi*7/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x + int(math.cos(math.pi*3/2+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi*3/2+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x + int(math.cos(math.pi*11/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_y - int(math.sin(math.pi*11/6+self.home_theta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_x,
            GUI_CENTER_Y - self.home_y)

    def getHomeDesiredCoord(self):
        return \
            (GUI_CENTER_X + self.home_dx + int(math.cos(math.pi/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx + int(math.cos(math.pi/2+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi/2+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx + int(math.cos(math.pi*5/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi*5/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx + int(math.cos(math.pi*7/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi*7/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx + int(math.cos(math.pi*3/2+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi*3/2+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx + int(math.cos(math.pi*11/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_Y - self.home_dy - int(math.sin(math.pi*11/6+self.home_dtheta)*RADIUS_ROBOT),
            GUI_CENTER_X + self.home_dx,
            GUI_CENTER_Y - self.home_dy)


    # Used to draw the away robots
    def getAwayCoord(self):
        return (GUI_CENTER_X + self.away_x + int(math.cos(math.pi/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x + int(math.cos(math.pi/2+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi/2+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x + int(math.cos(math.pi*5/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi*5/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x + int(math.cos(math.pi*7/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi*7/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x + int(math.cos(math.pi*3/2+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi*3/2+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x + int(math.cos(math.pi*11/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_Y - self.away_y - int(math.sin(math.pi*11/6+self.away_theta)*RADIUS_ROBOT),
                GUI_CENTER_X + self.away_x,
                GUI_CENTER_Y - self.away_y)

    # Listens to the location service to update the data structures used and draw them on the field accordingly
    def updateLocations(self):
        try:
            locations = rospy.ServiceProxy('currentlocationsservice', currentlocationsservice)
            response = locations()
            locations = pickle.loads(response.pickle)
            self.ball_x = meterToPixel(locations.ballx)
            self.ball_y = meterToPixel(locations.bally)
            self.canvas.coords(self.ball, *self.getBallCoord())
            self.home_x = meterToPixel(locations.home1x)
            self.home_y = meterToPixel(locations.home1y)
            self.home_theta = locations.home1theta
            self.canvas.coords(self.home,*self.getHomeCoord())
            self.away_x = meterToPixel(locations.away1x)
            self.away_y = meterToPixel(locations.away1y)
            self.away_theta = locations.away1theta
            self.canvas.coords(self.away,*self.getAwayCoord())
            self.canvas.coords(self.home_d,*self.getHomeDesiredCoord())
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e
        self.parent.after(REFRESH_RATE, self.updateLocations)


# Initializes the frame and starts the main frame loop
if __name__ == '__main__':
    print "Searching for location service"
    rospy.wait_for_service('currentlocationsservice')
    print "Starting Command Center"
    root = Tk()
    root.resizable(width=FALSE, height=FALSE)
    ex = Field(root)
    root.after(0, ex.updateLocations)
    root.geometry("{}x{}".format(WIDTH_FIELD+2*GUI_MARGIN, HEIGHT_FIELD+2*GUI_MARGIN))
    root.mainloop()
