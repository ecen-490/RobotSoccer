# Holds all the global variables and contains helpful methods

import math
import numpy as np

PIXELS_PER_METER = 215.0

meterToPixel = lambda x: int(x*PIXELS_PER_METER)
degreeToRadian = lambda x: x/180.0 * math.pi
radianToDegree = lambda x: int(x * 180.0 / math.pi)
timeToInt = lambda x: (x.secs-1420000000)*100 + x.nsecs/10000000

HEIGHT_FIELD = 500
WIDTH_FIELD = 700
GUI_MARGIN = 10
HEIGHT_GOAL = 100
WIDTH_GOAL = 30

GUI_CENTER_X = GUI_MARGIN + WIDTH_FIELD/2
GUI_CENTER_Y = GUI_MARGIN + HEIGHT_FIELD/2

WIDTH_BALL = meterToPixel(.04178)
RADIUS_ROBOT = meterToPixel(.10124)

away_flag = 1
camera_4 = 1               # if using camera 4 you need to rotate the coordinates by 180


RA2_play = 0
RA1_play = 1

max_x = 1.75
max_y = 1.15
goalWidth = 0.635

'''
#for the small field
#field is 135" x 94" or 3.429m x 2.3876m
#each quadrant is 1.7145m x 1.1938m
max_x = 1.7145
max_y = 1.1938
'''

_DistanceBeyondGoal = 0.2
_Minimum_G2Rd = 0.3      # The minimum distance between the goal and robot
defend_goal_x = max_x + _DistanceBeyondGoal

start1_x = -0.5
start1_y = 0
start2_x = (max_x-0.5)
start2_y = start1_y

RobotRadius = 0.10  # radius of robot

homeGoal = np.matrix([[-(max_x + _DistanceBeyondGoal)], [0]])
awayGoal = np.matrix([[(max_x)], [0]])

