import roboclaw2 as r
import time as t
import matplotlib.pyplot as plt

####### YOU SHOULD CHANGE THE MOTOR ADDRESSES (128, 129, ETC) AND M1/M2 VALUES FOR YOUR SPECIFIC ROBOT#######
totalTime = 3   #seconds
sampleRate = 10 #samples per second
pulsePerRotation = 19820.0
speedM1 = 4
speedM2 = 4

speedM3 = 4

####### USE ABOUT 30,000 QPPS IF ON SOCCER FIELD, ABOUT 300,000 QPPS IF NO LOAD #######
####### FIRST GET TO WORK WITHOUT ANY INTEGRATOR, THEN SET TO ABOUT 12500 #######
# Set the PIDQ values for the different motors
r.SetM1pidq(128, 4, 2, 1, 250000)
r.SetM2pidq(128, 4, 2, 1, 250000)
r.SetM1pidq(129, 4, 2, 1, 250000)

#initialize arrays
times = []
speedsM1 = []
speedsM2 = []
speedsM3 = []

r.SetM1Speed(128, int(speedM1*pulsePerRotation))
r.SetM2Speed(128, int(speedM2*pulsePerRotation))
r.SetM1Speed(129, int(speedM3*pulsePerRotation))

for i in range(0,totalTime * sampleRate):
	t.sleep(1.0/sampleRate)
	times.append(i*1.0/sampleRate)
	speedsM1.append(r.readM1speed(128)[1]/pulsePerRotation)
	speedsM2.append(r.readM2speed(128)[1]/pulsePerRotation)
	speedsM3.append(r.readM1speed(129)[1]/pulsePerRotation)
	
r.SetM1Speed(128, 0)
r.SetM2Speed(128, 0)
r.SetM1Speed(129, 0)

plt.plot(times, speedsM1) #blue
plt.plot(times, speedsM2) #green
plt.plot(times, speedsM3) #red
plt.plot([0, totalTime], [speedM1, speedM1])
plt.plot([0, totalTime], [speedM2, speedM2])
plt.plot([0, totalTime], [speedM3, speedM3])

plt.ylabel('Rotations per second for ' + str(len(speedsM1)) + ' samples')
plt.xlabel('Sample Time (s)')
plt.show()
