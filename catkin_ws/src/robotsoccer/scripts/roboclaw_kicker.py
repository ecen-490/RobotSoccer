import roboclaw as RC
import rospy
from robotsoccer.msg import kickerplate_robot1commands
import time


class kickerPlate(self):
    def __init__(self):
        self.command = 0        # Determines how hard to kick the ball
                                # 0 - The kicker plate will retrace
                                # 1 - A small force will be applied to the kicker plate
                                # 2 - A medium force will be applied to the kicker plate
                                # 3 - A large force will be applied to the kicker plate
        self.stop = 0
        self.soft = 70
        self.medium = 105
        self.hard = 115

    def kicker_extend(self,data):
        self.command = data.force
        force = 0
        if self.command == 0:
            force = self.stop
        elif self.command == 1:
            force = self.soft
        elif self.command == 2:
            force = self.medium
        elif self.command == 3:
            force = self.hard

        RC.M2Forward(RC.RoboClaw2, force)

    def listener(self):
        rospy.init_node('robotKicker', anonymous = True)
        rospy.Subscriber('robotKicker_commands', kickerplate_robot1commands, self.kicker_extend)
        rospy.spin()

def roboclawKicker_extend(force):
        print("Extending!", force%128)
        RC.M2Forward(RC.RoboClaw2, force%128)

def roboclawKicker_retract():
        RC.M2Forward(RC.RoboClaw2, 0)

#val is how long you want it to run, force is how hard
def goGoGadgetKickerPlate(val, force):
        #RC.M2Forward(RC.RoboClaw2, force%128)
        roboclawKicker_extend(force%128)
        print("Sleeping for", val/1000.0)
        time.sleep(val/1000.0)
       #RC.M2Forward(RC.RoboClaw2, 0)
        roboclawKicker_retract()

kp = kickerPlate()	

if __name__ == "__main__":
    print("Running as main!")
    # goGoGadgetKickerPlate(70.0, 115)
    kp.listener()


# At force = 127
#full speed is 70 ms
# mid speed is 60 ms
# low speed is 50 ms

#at time = 70 ms
#full speed is 127
#9' is 115
# 8'6" force = 127, 105

# 7'9"-8' force = 100
# 5'6" force = 95
# 5'4" force = 90
#3'6" force = 85
# 2 ft (force = 80)
#below 80, nothing
