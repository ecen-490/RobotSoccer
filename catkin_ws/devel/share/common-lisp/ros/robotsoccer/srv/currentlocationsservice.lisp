; Auto-generated. Do not edit!


(cl:in-package robotsoccer-srv)


;//! \htmlinclude currentlocationsservice-request.msg.html

(cl:defclass <currentlocationsservice-request> (roslisp-msg-protocol:ros-message)
  ()
)

(cl:defclass currentlocationsservice-request (<currentlocationsservice-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <currentlocationsservice-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'currentlocationsservice-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-srv:<currentlocationsservice-request> is deprecated: use robotsoccer-srv:currentlocationsservice-request instead.")))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <currentlocationsservice-request>) ostream)
  "Serializes a message object of type '<currentlocationsservice-request>"
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <currentlocationsservice-request>) istream)
  "Deserializes a message object of type '<currentlocationsservice-request>"
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<currentlocationsservice-request>)))
  "Returns string type for a service object of type '<currentlocationsservice-request>"
  "robotsoccer/currentlocationsserviceRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'currentlocationsservice-request)))
  "Returns string type for a service object of type 'currentlocationsservice-request"
  "robotsoccer/currentlocationsserviceRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<currentlocationsservice-request>)))
  "Returns md5sum for a message object of type '<currentlocationsservice-request>"
  "0c8e4620cde9c3b16d1cfbdddb8c721c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'currentlocationsservice-request)))
  "Returns md5sum for a message object of type 'currentlocationsservice-request"
  "0c8e4620cde9c3b16d1cfbdddb8c721c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<currentlocationsservice-request>)))
  "Returns full string definition for message of type '<currentlocationsservice-request>"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'currentlocationsservice-request)))
  "Returns full string definition for message of type 'currentlocationsservice-request"
  (cl:format cl:nil "~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <currentlocationsservice-request>))
  (cl:+ 0
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <currentlocationsservice-request>))
  "Converts a ROS message object to a list"
  (cl:list 'currentlocationsservice-request
))
;//! \htmlinclude currentlocationsservice-response.msg.html

(cl:defclass <currentlocationsservice-response> (roslisp-msg-protocol:ros-message)
  ((pickle
    :reader pickle
    :initarg :pickle
    :type cl:string
    :initform ""))
)

(cl:defclass currentlocationsservice-response (<currentlocationsservice-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <currentlocationsservice-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'currentlocationsservice-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-srv:<currentlocationsservice-response> is deprecated: use robotsoccer-srv:currentlocationsservice-response instead.")))

(cl:ensure-generic-function 'pickle-val :lambda-list '(m))
(cl:defmethod pickle-val ((m <currentlocationsservice-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-srv:pickle-val is deprecated.  Use robotsoccer-srv:pickle instead.")
  (pickle m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <currentlocationsservice-response>) ostream)
  "Serializes a message object of type '<currentlocationsservice-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'pickle))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'pickle))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <currentlocationsservice-response>) istream)
  "Deserializes a message object of type '<currentlocationsservice-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'pickle) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'pickle) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<currentlocationsservice-response>)))
  "Returns string type for a service object of type '<currentlocationsservice-response>"
  "robotsoccer/currentlocationsserviceResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'currentlocationsservice-response)))
  "Returns string type for a service object of type 'currentlocationsservice-response"
  "robotsoccer/currentlocationsserviceResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<currentlocationsservice-response>)))
  "Returns md5sum for a message object of type '<currentlocationsservice-response>"
  "0c8e4620cde9c3b16d1cfbdddb8c721c")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'currentlocationsservice-response)))
  "Returns md5sum for a message object of type 'currentlocationsservice-response"
  "0c8e4620cde9c3b16d1cfbdddb8c721c")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<currentlocationsservice-response>)))
  "Returns full string definition for message of type '<currentlocationsservice-response>"
  (cl:format cl:nil "string pickle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'currentlocationsservice-response)))
  "Returns full string definition for message of type 'currentlocationsservice-response"
  (cl:format cl:nil "string pickle~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <currentlocationsservice-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'pickle))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <currentlocationsservice-response>))
  "Converts a ROS message object to a list"
  (cl:list 'currentlocationsservice-response
    (cl:cons ':pickle (pickle msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'currentlocationsservice)))
  'currentlocationsservice-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'currentlocationsservice)))
  'currentlocationsservice-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'currentlocationsservice)))
  "Returns string type for a service object of type '<currentlocationsservice>"
  "robotsoccer/currentlocationsservice")