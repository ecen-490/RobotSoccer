#!/usr/bin/env python

import rospy
import sys
import numpy as np
from enum import Enum
from CurrentLocations import *
from robotsoccer.msg import *
from robotsoccer.srv import *
from parameters import *
import parameters as P
from prediction import getPredictedBall
from LocationNode import CurrentLocationsService

_scalar1 = 1.2
_scalar2 = 2
_dminRR = P.RobotRadius + P.RobotRadius*_scalar1   # Minimum distance between two robots
_dminRB = P.RobotRadius*_scalar1                   # Minimum distance between robot and ball


# Robot object and methods
class Robot:
    # Initialize
    def __init__(self, RobotNumber):
        self.RH1 = np.matrix([[0],[0],[0],[0],[0]]) # Robot Home1's position and velocity (x, y, theta, vx, vy, vtheta)
        #RH2 = np.zeros([6,1]) # Robot Home2's position and velocity (x, y, theta, vx, vy, vtheta)
        self.RA1 = np.matrix([[0],[0],[0],[0],[0]]) # Robot Away1's position and velocity (x, y, theta, vx, vy, vtheta)
        self.RA2 = np.matrix([[0],[0],[0],[0],[0]]) # Robot Away2's position and velocity (x, y, theta, vx, vy, vtheta)
        self.Ball = np.matrix([[0],[0],[0],[0]]) # Ball's position and velocity (x, y, vx, vy)
        self.Ballphat = np.matrix([[0],[0],[0],[0]]) # Ball's predicted position
        self.desiredPoint = np.matrix([[0],[0],[0]]) # Desired Point
        self.robot = RobotNumber
        self.CollisionDetected = 0
        self.circleDirection = "None"
        self.state = "None"

    # Publish message to the robot
    def sendCommand(self):
        if self.robot == 1:
            self.msg = robot1commands()
            self.pub = rospy.Publisher('robot1_commands', robot1commands, queue_size=10)
            self.msg.x = self.desiredPoint[0,0]
            self.msg.y = self.desiredPoint[1,0]
            self.msg.theta = self.desiredPoint[2,0]
            self.pub.publish(self.msg)
        else:
            self.msg = robot2commands()
            self.pub = rospy.Publisher('robot2_commands', robot1commands, queue_size=10)
            self.msg.x = self.desiredPoint[0,0]
            self.msg.y = self.desiredPoint[1,0]
            self.msg.theta = self.desiredPoint[2,0]
            self.pub.publish(self.msg)

    def rotate(self,theta):
        r = np.matrix([[np.cos(theta), np.sin(theta)],[-np.sin(theta),np.cos(theta)]])
        return r

    def getVector(self,object1,object2):
        # print('obj1', object1)
        # print('obj2',object2)
        v = object2[0:2,0] - object1[0:2,0]  # position vector
        norm = np.linalg.norm(v)
        if norm == 0:
            norm = 0.1e8
        # print("v",v)
        vn = v/norm
        # print("vn",vn)
        # print("norm",norm )
        return vn,norm

    # Returns the angle between the two objects from object 1's perspective
    def getTheta(self, object1_pos,object2_pos):
        v = object2_pos[0:2,0] - object1_pos[0:2,0]         # position vector
        vn = v/np.linalg.norm(v)                            # normalize position vector
        # print('v', v)
        # print('vn', vn)
        theta = np.arctan2(vn[1,0],vn[0,0])
        return theta

    # Returns 1 if wall collision is detected
    def detectWallCollision(self,x,y):
        collisionWall = 0
        if abs(x + np.sign(x)*P.RobotRadius) > P.max_x or abs(y + np.sign(x)*P.RobotRadius) > P.max_y:
            collisionWall = 1
        return collisionWall

    def detectObjCollision(self,RH,Obj,rd,DP):
        collision = 0
        vnRH2DP, vmRH2DP = self.getVector(RH,DP)
        vnRH2Obj, vmRH2Obj = self.getVector(RH,Obj)

        # print(vnRH2DP)
        if vnRH2DP[0,0] == 0:
            vnRH2DP[0.0] = 0.000000001
        if vnRH2Obj[0,0] == 0:
            vnRH2Obj[0,0] = 0.000000001

        # get line equation of DP and RH!
        m1 = vnRH2DP[1,0]/vnRH2DP[0,0]
        y1 = RH[1,0]
        x1 = RH[0,0]
        b1 = y1 - m1*x1

        if m1 == 0:
            m1 = 0.00001

        # get line equation of a line normal to the above line that goes thorough the robot
        m2 = -1/m1
        y2 = Obj[1,0]
        x2 = Obj[0,0]
        b2 = y2 - m2*x2

        # find intersection point
        xi = (b2-b1)/(m1-m2)
        yi = m1*xi+b1

        # find the distance the object is from the line
        vnObj2Line, vmObj2Line = self.getVector(np.matrix([[xi],[yi]]), Obj)

        point = vnObj2Line*5

        # cross products
        cross1 = np.cross(np.array([point[0,0],point[1,0],0]),np.array([x2-x1,y2-y1,0]))
        cross2 = np.cross(np.array([point[0,0],point[1,0],0]), np.array([x2-DP[0,0],y2-DP[1,0],0]))
        loc = np.sign(cross1[2])*np.sign(cross2[2])
        # print("DP",DP)
        # print("cross1", cross1)
        # print("cross2",cross2)
        # print("loc", loc)
        # print("vmObj2Line",vmObj2Line)
        if vmObj2Line < rd and loc == -1:
            collision=1
        return collision, vmRH2Obj

    # Returns 1 if ball collision is detected. Used when defending goal
    def detectBallCollision(self,DP):
        collisionBall,vmR2B = self.detectObjCollision(self.RH1,self.Ball,_dminRB,DP)
        # print ('collisionBall', collisionBall)
        return collisionBall, vmR2B

    # Returns 1 if collision is detected between away 1 and 2.
    def detectRobotCollision(self,DP):
        collisionRA1,vmR2RA1 = self.detectObjCollision(self.RH1,self.RA1,_dminRR,DP)
        collisionRA2,vmR2RA2 = self.detectObjCollision(self.RH1,self.RA2,_dminRR,DP)
        # print("")
        # print("DP", DP)
        # print("thresholdRA1",thresholdRA1)
        # print("dot product RA1",vnR2RA1.transpose()*vnR2DP)
        # print("collisionRA1", collisionRA1)
        # print("d", vmR2RA1)
        # print("d",vmR2RA2)
        # print("collisionRA2", collisionRA2)
        return collisionRA1,vmR2RA1, collisionRA2, vmR2RA2

    def isObjectAtDP(self,obj,DP,rd):
        vnObj2DP, vmObj2DP = self.getVector(obj, DP)
        return vmObj2DP < rd

    def isRobotAtDP(self,DP):
        RA1_IsAtDP = self.isObjectAtDP(self.RA1,DP,P.RobotRadius)
        RA2_IsAtDP = self.isObjectAtDP(self.RA2,DP,P.RobotRadius)
        return RA1_IsAtDP,RA2_IsAtDP

    def getCircleDirection(self,obj,rd,DP):
        # The line is from the RH to the DP.
        # This function returns the rotation direction the robot will make around the object
      
        direction = -1
        RH_x = self.RH1[0,0]
        RH_y = self.RH1[1,0]
        Obj_x = obj[0,0]
        Obj_y = obj[1,0]
        DP_x = DP[0,0]
        DP_y = DP[1,0]
        # print np.array([DP_x - RH_x, DP_y-RH_y, 0])
        # print np.array([Obj_x - RH_x, Obj_y-RH_y, 0])
        cross1 = np.cross(np.array([DP_x - RH_x, DP_y-RH_y, 0]), np.array([Obj_x - RH_x, Obj_y-RH_y, 0]))
        if cross1[2] > 0:
            direction = 1
        # print '%e' % cross1[2]
        vnRH2DP,vmRH2DP = self.getVector(self.RH1,DP)
        vnP = np.matrix([[-vnRH2DP[1,0]],[vnRH2DP[0,0]]])
        point1 = self.RH1[0:2,0] + vnP*rd
        point2 = self.RH1[0:2,0] - vnP*rd

        if abs(point1[0,0]) > P.max_x or abs(point2[0,0]) > P.max_x:
            # print("wallx collision")
            direction = direction * -1
        elif abs(point1[1,0]) > P.max_y or abs(point2[1,0]) > P.max_y:
            direction = direction *-1
            # print("wally collision")

        if self.circleDirection == "None":
            if direction ==1:
                self.circleDirection = "c_clockwise"
            else:
                self.circleDirection = "clockwise"

    # Returns if x,y coordin
    def avoidCollision(self, x,y,avoidRA,avoidB):
        # avoidRA - if non zero, the robot will avoid opponent robots
        # avoidB - if non zero, the robot will avoid the ball
        DP = np.matrix([[x],[y]])

        if avoidB:
            collisionBall, vmR2B = self.detectBallCollision(DP)
            if collisionBall == 0:
                vmR2B = 9999
        else:
            collisionBall, vmR2B = 0,9999

        if avoidRA:
            collisionRA1,vmR2RA1, collisionRA2, vmR2RA2 = self.detectRobotCollision(DP)
            vmR2RA1 = vmR2RA1 if (P.RA1_play and collisionRA1) else 9999
            vmR2RA2 = vmR2RA2 if (P.RA2_play and collisionRA2) else 9999
        else:
            collisionRA1,vmR2RA1, collisionRA2, vmR2RA2 = 0,9999,0,9999

        index = np.argmin([vmR2B,vmR2RA1,vmR2RA2,5])

        if index == 0:  # avoid ball
            self.getCircleDirection(self.Ball,_dminRB,DP)
            x,y = self.circleObject(self.Ball,_dminRB)
        elif index == 1: # avoid RA1
            self.getCircleDirection(self.RA1,_dminRR,DP)
            x,y = self.circleObject(self.RA1,_dminRB)
        elif index == 2: # avoid RA2
            self.getCircleDirection(self.RA2,_dminRR,DP)
            x,y = self.circleObject(self.RA2,_dminRB)
        else:                # Nothing to avoid
            self.collisionDetected = 0
            self.circleDirection = "None"

        #print('CollisionDetected', self.CollisionDetected)
        #print('direction',self.circleDirection)

        return x,y

    def circleObject(self,obj, rd):
        vn1, vd1 = self.getVector(obj,self.RH1)
        print(" CD direction", self.circleDirection)

        if self.circleDirection == "c_clockwise":
            self.c_clockwise = 1
        else:
            self.c_clockwise = 0

        #print('c_clockwise', self.c_clockwise)

        d = 0.25
        d2 = 0.5
        kp = 180                                           # Proportional gain control

        rd = rd*1.2
        error = rd-vd1

        if abs(error) > d2:
            error = d2*np.sign(error)

        theta = kp*(error)/180*np.pi                     # Correction theta
        theta = theta if self.c_clockwise else theta*(-1)
        Rot = self.rotate(theta)                           # Rotation matrix
        vn1 = Rot*vn1                                      # Rotate vector

        if self.c_clockwise :
            vn2 = np.matrix([[-vn1[1,0]],[vn1[0,0]]])
        else:
            vn2 = np.matrix([[vn1[1,0]],[-vn1[0,0]]])

        dp = self.RH1[0:2,0] + vn2*d

        x = dp[0,0]
        y = dp[1,0]
        # print('theta', theta)
        # print('Rot', Rot)
        # print('vn2',vn2)
        # print('dp',dp)
        return x,y

    # Get behind ball to score or in front of ball to defend
    def getBehindBall(self,offense):
        ballPos = self.Ball[0:2,0]
        if offense:
            vn,vm = self.getVector(self.Ball,P.awayGoal)
            theta_d = self.getTheta(self.Ball,P.awayGoal)
        else:
            vn,vm = self.getVector(P.homeGoal,self.Ball)
            theta_d = self.getTheta(P.homeGoal,self.Ball)

        DP = ballPos + vn*(-1)*_dminRB*1.1
        x,y = DP[0,0],DP[1,0]

        x,y = self.avoidCollision(x,y,1,1)
        return x,y,theta_d

    # Sets the x,y,theta towards the away goal
    def go2awayGoal(self):
        vn,vm = self.getVector(self.Ball,P.awayGoal)
        d = 0.3
        DP = self.RH1[0:2,0] + vn*d
        x = DP[0,0]
        y = DP[1,0]
        theta_d = self.getTheta(self.Ball,P.awayGoal)
        return x,y,theta_d

    # spins the robot in a circle. The rotation should move the ball away from home goal
    def spin(self):
        theta = self.RH1[2,0]
        # print('theta', theta*180/np.pi)
        while theta < 0:
            theta = theta + np.pi*2
        # print('theta', theta*180/np.pi)
        if theta > np.pi:
            direction = 1
        else:
            direction = -1

        x = self.Ball[0,0]
        y = self.Ball[1,0]
        theta_d = self.RA1[2,0] + direction * np.pi/2
        theta_d = theta_d % 2*np.pi
        # print('direction', direction)
        return x,y,theta_d

    # Detects a wall stall and attempts to avoid it
    def detectWallStall(self):
        stall = 0
        vnR2B, vmR2B = self.getVector(self.RH1,self.Ball)
        threshold = 0.95
        scalar = 1.9

        dotWx = vnR2B.transpose()*(np.matrix([[1],[0]])*np.sign(self.RH1[0,0]))
        dotWy = vnR2B.transpose()*(np.matrix([[0],[1]])*np.sign(self.RH1[1,0]))
        if dotWx > threshold and abs(self.RH1[0,0]) > P.max_x - P.RobotRadius*scalar:
            stall = 1
        if dotWy > threshold and abs(self.RH1[1,0]) > P.max_y - P.RobotRadius*scalar:
            stall = 1
        # print('dotWx',dotWx)
        # print('dotWy',dotWy)
        return stall

    # detects if the ball is squished between two robots

    def detectStall(self):
        stall = 0
        vnR2B, vmR2B = self.getVector(self.RH1,self.Ball)
        vnR2RA1,vmR2RA1 = self.getVector(self.RH1,self.RA1)
        vnR2RA2,vmR2RA2 = self.getVector(self.RH1,self.RA2)

        threshold = 0.94
        scale = 1.1

        # print("blah",vnR2B.transpose()*vnR2RA1 )

        if vmR2B < _dminRB*scale and vmR2RA1 < _dminRR*scale and vnR2B.transpose()*vnR2RA1 > threshold:
            stall = 1
        if vmR2B < _dminRB*scale and vmR2RA2 < _dminRR*scale and vnR2B.transpose()*vnR2RA2 > threshold and P.RA2_play:
            stall = 1
        if self.detectWallStall():
            stall = 1

        # print('stall', stall)
        return stall

    # When the ball is too close to home goal for the robot to get behind, it will attack the ball from the side and push
    # it out. It will be two phase. phase 1 will be to get next to the ball and phase 2 will be to push the ball
    def defendBaseLine(self):
        correction = 0
        x = self.Ball[0,0]-correction
        x = -P.max_x + P.RobotRadius*1.3 if x < -P.max_x + P.RobotRadius*1.3 else x
        y = 0
        vnR2B,vmR2B = self.getVector(self.RH1,self.Ball)
        # theta_d = np.arctan2(vnR2B[1,0],vnR2B[0,0])
        theta_d = -9999

        threshold_x = 0.02
        threshold = _dminRB*1.4
        # checks to see if the robot is parallel
        # print("check first condition", abs(self.RH1[0,0]-self.Ball[0,0]+correction))
        # print(abs(self.RH1[0,0]-self.Ball[0,0]+correction) < threshold_x)
        # print("check second condition", abs(self.RH1[1,0]-self.Ball[1,0]))
        # print(abs(self.RH1[1,0]-self.Ball[1,0]) < threshold)

        if abs(self.RH1[0,0]-self.Ball[0,0]+correction) < threshold_x and abs(self.RH1[1,0]-self.Ball[1,0]) < threshold:# and abs((self.RH1([2,0]) - theta_d)) < 10*np.pi/180:
            theta_d = np.sign(self.Ball[1,0]- y)*np.pi/2
            theta_d = theta_d + np.sign(theta_d)*(-20*np.pi/180)

            x = self.Ball[0,0] + 0.25*np.cos(theta_d)
            y = self.Ball[1,0] + 0.25*np.sin(theta_d)
        else:
            y = self.Ball[1,0] + _dminRB*np.sign(self.RH1[1,0] - self.Ball[1,0])

            collisionWall = self.detectWallCollision(x,y)
            RA1_IsAtDP,RA2_IsAtDP = self.isRobotAtDP(np.matrix([[x],[y]]))


            # Checks to see if a robot or wall is in the desired place
            if collisionWall or RA1_IsAtDP*P.RA1_play or RA2_IsAtDP*P.RA2_play:
                # if a collision was detected, the robot will move in the other direction
                y = self.Ball[1,0]  - _dminRB*np.sign(self.RH1[1,0] - self.Ball[1,0])

                # checks to see if there is a collision
                collisionWall = self.detectWallCollision(x,y)
                RA1_IsAtDP,RA2_IsAtDP = self.isRobotAtDP(np.matrix([[x],[y]]))
                if collisionWall or RA1_IsAtDP*P.RA1_play or RA2_IsAtDP*P.RA2_play:
                    print("collision on both sides")
                    print("collision Wall", collisionWall)
                    # if there is a collision the robot will move either to the ball or at a robot
                    if abs(self.Ball[1,0]) < P.goalWidth: # The ball is in front of the goal and is surrounded
                        # attack the opponent's robot
                        x = self.RA1[0,0]
                        y = self.RA1[1,0]
                    else: # attack the ball
                        y = self.Ball[1,0]
                        theta_d = np.arctan2(vnR2B[1,0],vnR2B[0,0])
        if theta_d == -9999:
            theta_d = np.sign(self.Ball[1,0]- y)*np.pi/2
            # theta_d = theta_d + np.sign(theta_d)*(-20*np.pi/180)

        x,y = self.avoidCollision(x,y,1,1)
        return x,y,theta_d

    # Returns the Current play state based on ball and robot locations
    def getState(self):
        state = "getBehindBallOffense"
        r = _dminRB*1.4
        threshold = 0.99
        vnRH12B,vmRH12B = self.getVector(self.RH1,self.Ball)
        vnB2AG,vmB2AG = self.getVector(self.Ball,P.awayGoal)
        vnRH12AG,vmRH12AG = self.getVector(self.RH1,P.awayGoal)
        thetaB2AG = self.getTheta(self.Ball,P.awayGoal)
        thetaRH12AG = self.getTheta(self.RH1,P.awayGoal)

        # print("ballx",self.Ball[0,0])
        # print("comparison", P.homeGoal[0,0] + _dminRB*2+ 0.4)

        if abs(self.Ball[0,0]) > P.max_x:
            state = "goalMade"
        elif self.Ball[0,0] < P.homeGoal[0,0] + _dminRB*2 + 0.4:
            state = "defendBaseLine"
        elif vmRH12B < r and vnRH12AG.transpose()*vnB2AG > threshold:
            state = "go2AwayGoal"
            if self.state != "go2AwayGoal":
                self.kickBall(128)
        elif self.Ball[0,0] < 0 and self.Ball[3,0] < -0.1:
            state = "getBehindBallDefense"
        else:
            state = "getBehindBallOffense"

        print('state', state)
        self.state = state
        return state

    # Calculates X,Y,Theta positions to score a goal
    def play(self):
        stall = self.detectStall()
        if stall:
            x,y,theta_d = self.spin()
        else:
            state = self.getState()
            if state == "goalMade":
                x = start1_x
                y = start1_y
                theta_d = 0
            elif state == "getBehindBallOffense":
                x,y,theta_d = self.getBehindBall(1)
            elif state == "getBehindBallDefense":
                x,y,theta_d = self.getBehindBall(0)
            elif state == "go2AwayGoal":
                x,y,theta_d = self.go2awayGoal()
            else:
                x,y,theta_d = self.defendBaseLine()
        #x,y,theta_d = self.getBehindBall(1)
        self.desiredPoint = np.matrix([[x],[y],[theta_d]])
        self.sendCommand()

    # Sets desired point to the center and sends command
    def go_to_center(self):
        theta_d = 0
        x,y = self.avoidCollision(0,0,1,1)
        self.sendCommand()

    # Command the robots to go to the start positions used for the game
    def go_to_start_position(self):
        theta_d = 0
        if self.robot == 1:
            x,y = self.avoidCollision(start1_x,start1_y,1,1)
        else:
            x,y = self.avoidCollision(start2_x,start2_y,1,1)

        self.desiredPoint = np.matrix([[x], [y], [theta_d]])
        self.sendCommand()

    # Command robot to go to a point. Ensures we stay within bounds of the field
    def skill_go_to_point(self, x, y, theta):
        # if abs(x) > max_x:
        #     x = max_x * np.sign(x)
        # if abs(y) > max_y:
        #     y = max_y * np.sign(y)
        self.desiredPoint = np.matrix([[x], [y], [theta]])
        self.sendCommand()

    # Uses the kalman filter's prediction algorithm
    @staticmethod
    def getPredictedStates(time, case):
        try:
            getPrediction = rospy.ServiceProxy('predictedlocationsservice', predictedLocations)
            resp1 = getPrediction(time, case)
            return pickle.loads(resp1.predictedStates)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    # Method to kick the ball
    def kickBall(self, force):
        # Send a 0 for stop, 1 for soft, 2 for medium, 3 for hard kick
        if self.robot == 1:
            msg = kickerplate_robot1command()
            pubk = rospy.Publisher('robotKicker_commands', kickerplate_robot1command, queue_size=10)
            msg.force = force
            pubk.publish(msg)
        else:
            pass

class RobotMain:

    # Holds the game state information
    class GameState(Enum):
        stop = 1
        play = 2
        center = 3
        startPosition = 4
        test = 5

    # Initialize
    def __init__(self):
        self.gameState = self.GameState.startPosition
        self.robot1 = Robot(1)
        self.robot2 = Robot(2)
        self.robot1position = np.matrix([[0.0],[0.0],[0.0],[0.0],[0.0]])  # Current robot position: x, y, theta, vx, vy
        self.robot2position = np.matrix([[0.0],[0.0],[0.0],[0.0],[0.0]])  # Current robot position: x, y, theta, vx, vy
        self.away1position = np.matrix([[0.0],[0.0],[0.0],[0.0],[0.0]])  # Current away1 position: x, y, theta, vx, vy
        self.away2position = np.matrix([[0.0],[0.0],[0.0],[0.0],[0.0]])  # Current away2 position: x, y, theta, vx, vy
        self.ball = np.matrix([[0.0],[0.0],[0.0],[0.0]])  # Current ball position: x, y, vx, vy
        self.home_goal = np.matrix([[-max_x], [0.0]])
        self.away_goal = np.matrix([[max_x], [0.0]])
        rospy.init_node('robot_main', anonymous=True)
        self.notKicked = True

    # Received commands for the command center change the robot state
    def executeCommandCenterCommand(self, req):
        if req.command == 1:
            self.gameState = self.GameState.stop
        elif req.command == 2:
            self.gameState = self.GameState.play
        elif req.command == 3:
            self.gameState = self.GameState.center
        elif req.command == 4:
            self.gameState = self.GameState.startPosition
        elif req.command == 5:
            self.gameState = self.GameState.test
        #print "{} {}".format("Received the following command: ", req.command)

    # Determine strategy for each robot and send command
    def play(self):
        self.robot1.play()

    # Used for testing. Place whatever play you want in here
    def test(self):
        print('in test function')
        #if self.notKicked:
        #    self.notKicked = False
        #    self.robot1.trick_shot(30)
        #    self.robot1.kickBall(105)
        #else:
        #    self.notKicked= True
        pass

    # Used to command the robot to center. Only commands one robot to the center
    def center(self):
        self.robot1.go_to_center()

    # Used to command robot to starting position
    def startPosition(self):
        self.robot1.go_to_start_position()
        self.robot2.go_to_start_position()

    # Command to exit Robot Main
    def stop(self):
        return

    # Defines the main execution for the robot. Checks the game state and determines actions for each robot to take
    def executionLoop(self):
        if self.gameState == self.GameState.play:
            self.play()
        elif self.gameState == self.GameState.test:
            self.test()
        elif self.gameState == self.GameState.center:
            self.center()
        elif self.gameState == self.GameState.startPosition:
            self.startPosition()
        elif self.gameState == self.GameState.stop:
            self.stop()

    # Callback method for when filtered location data is received
    def updateCurrentPositions(self, data):
        # Robot position
        self.robot1.RH1 = np.matrix([[data.home1_x],[data.home1_y],[data.home1_theta],[data.home1_vx],[data.home1_vy]])
        self.robot1.Ball = np.matrix([[data.ball_x],[data.ball_y],[data.ball_vx],[data.ball_vy]])
        self.robot1.RA1 = np.matrix([[data.away1_x],[data.away1_y],[data.away1_theta],[data.away1_vx],[data.away1_vy]])
        self.robot1.RA2 = np.matrix([[data.away2_x],[data.away2_y],[data.away2_theta],[data.away2_vx],[data.away2_vy]])

        # Strategy positions
        self.robot1position = np.matrix([[data.home1_x],[data.home1_y],[data.home1_theta],[data.home1_vx],[data.home1_vy]])
        self.ball = np.matrix([[data.ball_x],[data.ball_y],[data.ball_vx],[data.ball_vy]])
        predict = self.getPredictedStates(0.4, 1)
        self.robot1.ballphat = predict.ballphat

        # print(' robot x', self.robot1.position[0,0])
        # print('robot y', self.robot1.position[1,0])
        # print('robot theta', self.robot1.position[2,0]*180/np.pi)
        # print('ball position', self.robot1.ball)

    # Uses the kalman filter's prediction
    @staticmethod
    def getPredictedStates(time, case):
        try:
            getPrediction = rospy.ServiceProxy('predictedlocationsservice', predictedLocations)
            resp1 = getPrediction(time, case)
            return pickle.loads(resp1.predictedStates)
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e

    # Starts the main loop
    def start(self):
        rospy.Subscriber("commandcenter_commands", commandcenter, self.executeCommandCenterCommand)
        rospy.Subscriber("current_locations", currentlocations, self.updateCurrentPositions)
        rospy.wait_for_service('predictedlocationsservice')
        print "Starting RobotMain"
        rate = rospy.Rate(50)  # 100 hz
        while not rospy.is_shutdown():
            self.executionLoop()
            rate.sleep()

if __name__ == '__main__':
    winner = RobotMain()
    winner.start()
