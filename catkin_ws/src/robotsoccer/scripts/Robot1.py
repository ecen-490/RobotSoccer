#!/usr/bin/env python

import rospy
import os
import signal
from robotsoccer.msg import *
from controller import *
from roboclaw_motion import stopRobot
import a_star_path_planning as pf


# A identical script runs on both rbots and listens for a message with the command of desired position
class Robot1:

    # Initialization
    def __init__(self):
        self.kickercommand = 0  # Determines how hard to kick the ball
        self.kick = False
        self.kick_counter = 0

    # Called every time a new robot command message is published
    def updatedesiredpositions(self, data):
        updateDesiredPosition(data.x, data.y, data.theta)
        #updateDesiredPosition(data.x, data.y, 0)
        #print "{} {}{}{}".format("desired: ", data.x, data.y, data.theta)

    # Called every time a new location message is published
    def updatecurrentpositions(self, data):
        updateCurrentPosition(data.home1_x, data.home1_y, data.home1_theta)
        #updateCurrentPosition(data.home1_x, data.home1_y, 0)
        #print("update current Position")
        #print('home x', data.home1_x)
        #print('home y', data.home1_y)
        #print('home theta', radianToDegree(data.home1_theta))

    # Listens to command center commands to exit in case of
    def executeCommandCenterCommand(self,req):
        if req.command == 1:
            print "Exiting"
            stopRobot()
            os.kill(os.getpid(), signal.SIGINT)
        elif req.command ==5:
            if not self.kick:
                self.kick = True
                self.kick_counter = 0
                self.kickercommand = 105

###KICKER PLATE COMMANDS###
    def kicker_command(self, data):
        self.kickercommand = data.force
        if not self.kick:
            print("Received kick command", self.kickercommand)
            self.kick = True
            self.kick_counter = 0
    #else:
            #print("DID NOTHING: Received kick command")


    #self.goGoGadgetKickerPlate(70, 105)
    #if counter == 0 and kick is true, extend
    #DON'T CONTINUALLY UPDATE THE ROBOCLAWS, do once
    #At counter == 7 AND kick is true, retract
    #Keep kick true until 9 (or 90 ms)
    def updateKickerCommand(self):
        if self.kick and self.kick_counter == 0:
            RC.M2Forward(RC.RoboClaw2, self.kickercommand)
            print "Kick"
            #elif self.kick and self.kick_counter <= 6:
		    #do nothing, should still be kicking
	    #if kick is still true AND kicker counter is 7, stop kicking
        elif self.kick and self.kick_counter == 7:
            RC.M2Forward(RC.RoboClaw2, 0)
            print "Retract"
        elif self.kick and self.kick_counter >= 25:
            self.kick = False
            print "Done"
        if self.kick:
            print("kicker counter:", self.kick_counter)
    #only increment if the self.kick is true, else don't change
        self.kick_counter = self.kick_counter + 1 if self.kick else self.kick_counter

    def goGoGadgetKickerPlate(self, val, force):
        #RC.M2Forward(RC.RoboClaw2, force%128)
        self.roboclawKicker_extend(force%128)
        print("Sleeping for", val/1000.0)
        time.sleep(val/1000.0)
       #RC.M2Forward(RC.RoboClaw2, 0)
        self.roboclawKicker_retract()
        time.sleep(val/1000.0)

    def roboclawKicker_extend(self, force):
        print("Extending!", force%128)
        RC.M2Forward(RC.RoboClaw2, force%128)

    def roboclawKicker_retract(self):
        RC.M2Forward(RC.RoboClaw2, 0)
##END OF KICKER PLATE CODE ##

    # Subscribes to location topic and robot1 commands message
    def listener(self):
        print "Starting robot1 node"
        rospy.init_node('robot1_node', anonymous=True)
        rospy.Subscriber("robot1_commands", robot1commands, self.updatedesiredpositions)
        # subscribes to location topic that has filtered kalman data
        rospy.Subscriber("current_locations", currentlocations, self.updatecurrentpositions)
        rospy.Subscriber("commandcenter_commands", commandcenter, self.executeCommandCenterCommand)
        #subscribes to kicker command
        rospy.Subscriber('robotKicker_commands', kickerplate_robot1command, self.kicker_command)
        #rospy.Subscriber("locTopic", locations, self.updatecurrentpositions)
        self.pub = rospy.Publisher('robot1_vcommand', robot1vcommand, queue_size=10)

        #a = pf.AStar() Initializes the A* algorithm
        #walls = ((0,5), (1,0)) Pass the objects that are obstacles. SUch as walls or robots
        #a.init_grid(35,35,walls,(cp.x,cp.y),(dp.x,dp.y)) height, width, start(x,y), end(x,y)
        #path = a.solve() Solves the maze and returns a set of x,y cells

        rate = rospy.Rate(50) # 100 Hz
        while not rospy.is_shutdown():
            self.updateKickerCommand()
            updateRobotMovement()
            #update the kicker command every cycle, it will manage the pushing and retracting
            msg = robot1vcommand()
            msg.vx = x.xhat[1, 0]
            msg.vy = y.xhat[1, 0]
            msg.vtheta = theta.xhat[1, 0]
            self.pub.publish(msg)
            rate.sleep()

if __name__ == '__main__':
    r = Robot1()
    r.listener()
