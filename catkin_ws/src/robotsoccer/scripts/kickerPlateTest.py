#!/usr/bin/env python

import rospy
from robotsoccer.msg import kickerplate_robot1commands

_Hz = 10


def talker():
    pub = rospy.Publish('robotKicker_commands',kickerplate_robot1commands,queue_size = 10)
    rospy.init_node('kickerPlateTest', anonymous = True)
    rate.rospy.Rate(_Hz)

    while not rospy.is_shutdown():
        pub.publish(int(1))
        rate.sleep()


if __name__ == '__main__':
    talker():