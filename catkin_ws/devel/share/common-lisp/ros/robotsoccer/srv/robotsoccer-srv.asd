
(cl:in-package :asdf)

(defsystem "robotsoccer-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "currentlocationsservice" :depends-on ("_package_currentlocationsservice"))
    (:file "_package_currentlocationsservice" :depends-on ("_package"))
    (:file "predictedLocations" :depends-on ("_package_predictedLocations"))
    (:file "_package_predictedLocations" :depends-on ("_package"))
  ))