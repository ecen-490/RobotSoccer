import roboclaw2 as RC
#import sys
#import serial
   
#port = serial.Serial("/dev/ttySAC0", baudrate=38400, timeout=1)

print("Looking for errors")
print("Common errors are 8 = temperature and 128 = logic battery level low")

'''
RC.sendcommand(RoboClaw2,21)
rcv = port.read(32) #32
print(repr(rcv))
RC.sendcommand(RoboClaw1,21)
rcv = port.read(32) #32
print(repr(rcv))
'''

#addr = 0x80
print("Looking at Roboclaw ", RC.RoboClaw2)
error = RC.readerrorstate(RC.RoboClaw2)
print("Error: ", error)

#addr = 0x81
print("Looking at Roboclaw ", RC.RoboClaw1)
error = RC.readerrorstate(RC.RoboClaw1)
print("Error: ", error)

