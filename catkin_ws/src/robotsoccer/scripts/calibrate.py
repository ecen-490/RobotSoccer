#!/usr/bin/python
#from motor_control.roboclaw import *
#from motor_control import velchange

import roboclaw2 as RC
import roboclaw_motion as RM

import time

#RC.calibrateRoboclaws()

p = int(4) #262144
i = int(2) #131072
d = int(1)  #65536
q = 308419

# FIXME - see note at the bottom of the file to see the comment about the readM#pidq -CB
print("printing batt",RC.readmainbattery(RC.RoboClaw1))
#p1,i1,d1,q1,crc = RC.readM1pidq(128)
t1,p1,i1,d1,q1 = RC.readM1pidq(128)
print(RC.readM1pidq(129))
print(RC.readM1pidq(128))
print(RC.readM2pidq(128))

print(t1,p1,i1,d1,q1)
print "128 M1 P=%.2f" % (p1)
print "128 M1 I=%.2f" % (i1)
print "128 M1 D=%.2f" % (d1)
print "128 M1 QPPS=",q1
t2,p2,i2,d2,q2 = RC.readM2pidq(128)
print "128 M2 P=%.2f" % (p2)
print "128 M2 I=%.2f" % (i2)
print "128 M2 D=%.2f" % (d2)
print "128 M2 QPPS=",q2
t3,p3,i3,d3,q3 = RC.readM1pidq(129)
print "129 M1 P=%.2f" % (p3)
print "129 M1 I=%.2f" % (i3)
print "129 M1 D=%.2f" % (d3)
print "129 M1 QPPS=",q3


def stop():
  RM.stopRobot()

def read(addr,motor):
  samples = 4
  result = 0
  for i in range(0, samples):
    sample = 0
    if motor == 1:
      sample = RC.readM1speed(addr)
    elif motor == 2:
      sample = RC.readM2speed(addr)
    #print sample[1]
    result = result + sample[1]
  result = result/samples
  return result
  
#MAIN
speedM1Forward=0
speedM1Backward=0
speedM2Forward=0
speedM2Backward=0
speedM3Forward=0
speedM3Backward=0

speed = 80
speedPercent = speed/127.0
t1 = 1

stop();

#Forwards
print "Forwards"
RC.M1Backward(RC.RoboClaw2,speed)
RC.M1Forward(RC.RoboClaw1,speed)
time.sleep(t1)

speedM1Backward=speedM1Backward+read(RC.RoboClaw2,1)
speedM2Forward=speedM2Forward+read(RC.RoboClaw1,1)

stop();
time.sleep(1);

#Backwards
print "Backwards"
RC.M1Forward(RC.RoboClaw2,speed)
RC.M1Backward(RC.RoboClaw1,speed)
time.sleep(t1)

speedM1Forward=speedM1Forward+read(RC.RoboClaw2,1)
speedM2Backward=speedM2Backward+read(RC.RoboClaw1,1)

stop();
time.sleep(1);

#Left back
print "Left Back"
RC.M2Forward(RC.RoboClaw1,speed); #M2 backward sample 2
RC.M1Backward(RC.RoboClaw1,speed); #M3 forward sample 1
time.sleep(t1)

speedM2Backward=speedM2Backward+read(RC.RoboClaw1,1)
speedM3Forward=speedM3Forward+read(RC.RoboClaw1,2)

stop();
time.sleep(1);

#Left forward
print "Left Forward"
RC.M2Forward(RC.RoboClaw1,speed); #M2 forward sample 2
RC.M1Backward(RC.RoboClaw2,speed); #M3 backward sample 1
time.sleep(t1)

speedM1Backward=speedM1Backward+read(RC.RoboClaw2,1)
speedM3Forward=speedM3Forward+read(RC.RoboClaw1,2)

stop();
time.sleep(1);

# RightBack
print "Right Back"
RC.M2Backward(RC.RoboClaw1,speed); #M3 forward sample 2
RC.M1Forward(RC.RoboClaw2,speed); #M3 backward sample 2
time.sleep(t1)

speedM1Forward=speedM1Forward+read(RC.RoboClaw2,1)
speedM3Backward=speedM3Backward+read(RC.RoboClaw1,2)

stop();
time.sleep(1);

# Right Forward
print "Right Forward"
RC.M1Forward(RC.RoboClaw1,speed); #M1 backward sample 2
RC.M2Backward(RC.RoboClaw1,speed); #M3 forward sample 2
time.sleep(t1)

speedM2Forward=speedM2Forward+read(RC.RoboClaw1,1)
# FIXME I'm not sure which motor is #3? -CB
#speedM3Backward=speedM3Backward+read(RC.RoboClaw1,3)

stop();

speedM1Forward=(speedM1Forward*127)/speed
speedM1Backward=(speedM1Backward*127)/speed
speedM2Forward=(speedM2Forward*127)/speed
speedM2Backward=(speedM2Backward*127)/speed
speedM3Forward=(speedM3Forward*127)/speed
speedM3Backward=(speedM3Backward*127)/speed

#print speedM1Forward;
#print speedM1Backward;
#print speedM2Forward;
#print speedM2Backward;
#print speedM3Forward;
#print speedM3Backward;

speedM1 = abs(speedM1Forward - speedM1Backward)/4
speedM2 = abs(speedM2Forward - speedM2Backward)/4
speedM3 = abs(speedM3Forward - speedM3Backward)/4

print("speedM1", speedM1)
print("speedM2", speedM2)
print("speedM3", speedM3)

print("Setting pidq to",p,i,d)
RC.SetM1pidq(RC.RoboClaw2,p,i,d,speedM1)
RC.SetM1pidq(RC.RoboClaw1,p,i,d,speedM2)
RC.SetM2pidq(RC.RoboClaw1,p,i,d,speedM3)

# FIXME - it doesn't look like readM1pidq is giving back the values that we are expecting. Sample output from readM1pidq: [1, 0.0, 0.0, 0.0, 2147483648L]
#print(RC.readM1pidq(RC.RoboClaw2))
t1,p1,i1,d1,q1 = RC.readM1pidq(128)
print "128 M1 P=%.2f" % (p1)
print "128 M1 I=%.2f" % (i1)
print "128 M1 D=%.2f" % (d1)
print "128 M1 QPPS=",q1
t2,p2,i2,d2,q2 = RC.readM2pidq(128)
print "128 M2 P=%.2f" % (p2)
print "128 M2 I=%.2f" % (i2)
print "128 M2 D=%.2f" % (d2)
print "128 M2 QPPS=",q2
t3,p3,i3,d3,q3 = RC.readM1pidq(129)
print "129 M1 P=%.2f" % (p3)
print "129 M1 I=%.2f" % (i3)
print "129 M1 D=%.2f" % (d3)
print "129 M1 QPPS=",q3
