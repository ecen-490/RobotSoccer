; Auto-generated. Do not edit!


(cl:in-package robotsoccer-msg)


;//! \htmlinclude currentlocations.msg.html

(cl:defclass <currentlocations> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (home1_x
    :reader home1_x
    :initarg :home1_x
    :type cl:float
    :initform 0.0)
   (home1_y
    :reader home1_y
    :initarg :home1_y
    :type cl:float
    :initform 0.0)
   (home1_theta
    :reader home1_theta
    :initarg :home1_theta
    :type cl:float
    :initform 0.0)
   (home1_vx
    :reader home1_vx
    :initarg :home1_vx
    :type cl:float
    :initform 0.0)
   (home1_vy
    :reader home1_vy
    :initarg :home1_vy
    :type cl:float
    :initform 0.0)
   (home2_x
    :reader home2_x
    :initarg :home2_x
    :type cl:float
    :initform 0.0)
   (home2_y
    :reader home2_y
    :initarg :home2_y
    :type cl:float
    :initform 0.0)
   (home2_theta
    :reader home2_theta
    :initarg :home2_theta
    :type cl:float
    :initform 0.0)
   (home2_vx
    :reader home2_vx
    :initarg :home2_vx
    :type cl:float
    :initform 0.0)
   (home2_vy
    :reader home2_vy
    :initarg :home2_vy
    :type cl:float
    :initform 0.0)
   (away1_x
    :reader away1_x
    :initarg :away1_x
    :type cl:float
    :initform 0.0)
   (away1_y
    :reader away1_y
    :initarg :away1_y
    :type cl:float
    :initform 0.0)
   (away1_theta
    :reader away1_theta
    :initarg :away1_theta
    :type cl:float
    :initform 0.0)
   (away1_vx
    :reader away1_vx
    :initarg :away1_vx
    :type cl:float
    :initform 0.0)
   (away1_vy
    :reader away1_vy
    :initarg :away1_vy
    :type cl:float
    :initform 0.0)
   (away2_x
    :reader away2_x
    :initarg :away2_x
    :type cl:float
    :initform 0.0)
   (away2_y
    :reader away2_y
    :initarg :away2_y
    :type cl:float
    :initform 0.0)
   (away2_theta
    :reader away2_theta
    :initarg :away2_theta
    :type cl:float
    :initform 0.0)
   (away2_vx
    :reader away2_vx
    :initarg :away2_vx
    :type cl:float
    :initform 0.0)
   (away2_vy
    :reader away2_vy
    :initarg :away2_vy
    :type cl:float
    :initform 0.0)
   (ball_x
    :reader ball_x
    :initarg :ball_x
    :type cl:float
    :initform 0.0)
   (ball_y
    :reader ball_y
    :initarg :ball_y
    :type cl:float
    :initform 0.0)
   (ball_vx
    :reader ball_vx
    :initarg :ball_vx
    :type cl:float
    :initform 0.0)
   (ball_vy
    :reader ball_vy
    :initarg :ball_vy
    :type cl:float
    :initform 0.0))
)

(cl:defclass currentlocations (<currentlocations>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <currentlocations>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'currentlocations)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-msg:<currentlocations> is deprecated: use robotsoccer-msg:currentlocations instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:header-val is deprecated.  Use robotsoccer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'home1_x-val :lambda-list '(m))
(cl:defmethod home1_x-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home1_x-val is deprecated.  Use robotsoccer-msg:home1_x instead.")
  (home1_x m))

(cl:ensure-generic-function 'home1_y-val :lambda-list '(m))
(cl:defmethod home1_y-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home1_y-val is deprecated.  Use robotsoccer-msg:home1_y instead.")
  (home1_y m))

(cl:ensure-generic-function 'home1_theta-val :lambda-list '(m))
(cl:defmethod home1_theta-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home1_theta-val is deprecated.  Use robotsoccer-msg:home1_theta instead.")
  (home1_theta m))

(cl:ensure-generic-function 'home1_vx-val :lambda-list '(m))
(cl:defmethod home1_vx-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home1_vx-val is deprecated.  Use robotsoccer-msg:home1_vx instead.")
  (home1_vx m))

(cl:ensure-generic-function 'home1_vy-val :lambda-list '(m))
(cl:defmethod home1_vy-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home1_vy-val is deprecated.  Use robotsoccer-msg:home1_vy instead.")
  (home1_vy m))

(cl:ensure-generic-function 'home2_x-val :lambda-list '(m))
(cl:defmethod home2_x-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home2_x-val is deprecated.  Use robotsoccer-msg:home2_x instead.")
  (home2_x m))

(cl:ensure-generic-function 'home2_y-val :lambda-list '(m))
(cl:defmethod home2_y-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home2_y-val is deprecated.  Use robotsoccer-msg:home2_y instead.")
  (home2_y m))

(cl:ensure-generic-function 'home2_theta-val :lambda-list '(m))
(cl:defmethod home2_theta-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home2_theta-val is deprecated.  Use robotsoccer-msg:home2_theta instead.")
  (home2_theta m))

(cl:ensure-generic-function 'home2_vx-val :lambda-list '(m))
(cl:defmethod home2_vx-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home2_vx-val is deprecated.  Use robotsoccer-msg:home2_vx instead.")
  (home2_vx m))

(cl:ensure-generic-function 'home2_vy-val :lambda-list '(m))
(cl:defmethod home2_vy-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:home2_vy-val is deprecated.  Use robotsoccer-msg:home2_vy instead.")
  (home2_vy m))

(cl:ensure-generic-function 'away1_x-val :lambda-list '(m))
(cl:defmethod away1_x-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away1_x-val is deprecated.  Use robotsoccer-msg:away1_x instead.")
  (away1_x m))

(cl:ensure-generic-function 'away1_y-val :lambda-list '(m))
(cl:defmethod away1_y-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away1_y-val is deprecated.  Use robotsoccer-msg:away1_y instead.")
  (away1_y m))

(cl:ensure-generic-function 'away1_theta-val :lambda-list '(m))
(cl:defmethod away1_theta-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away1_theta-val is deprecated.  Use robotsoccer-msg:away1_theta instead.")
  (away1_theta m))

(cl:ensure-generic-function 'away1_vx-val :lambda-list '(m))
(cl:defmethod away1_vx-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away1_vx-val is deprecated.  Use robotsoccer-msg:away1_vx instead.")
  (away1_vx m))

(cl:ensure-generic-function 'away1_vy-val :lambda-list '(m))
(cl:defmethod away1_vy-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away1_vy-val is deprecated.  Use robotsoccer-msg:away1_vy instead.")
  (away1_vy m))

(cl:ensure-generic-function 'away2_x-val :lambda-list '(m))
(cl:defmethod away2_x-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away2_x-val is deprecated.  Use robotsoccer-msg:away2_x instead.")
  (away2_x m))

(cl:ensure-generic-function 'away2_y-val :lambda-list '(m))
(cl:defmethod away2_y-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away2_y-val is deprecated.  Use robotsoccer-msg:away2_y instead.")
  (away2_y m))

(cl:ensure-generic-function 'away2_theta-val :lambda-list '(m))
(cl:defmethod away2_theta-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away2_theta-val is deprecated.  Use robotsoccer-msg:away2_theta instead.")
  (away2_theta m))

(cl:ensure-generic-function 'away2_vx-val :lambda-list '(m))
(cl:defmethod away2_vx-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away2_vx-val is deprecated.  Use robotsoccer-msg:away2_vx instead.")
  (away2_vx m))

(cl:ensure-generic-function 'away2_vy-val :lambda-list '(m))
(cl:defmethod away2_vy-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:away2_vy-val is deprecated.  Use robotsoccer-msg:away2_vy instead.")
  (away2_vy m))

(cl:ensure-generic-function 'ball_x-val :lambda-list '(m))
(cl:defmethod ball_x-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:ball_x-val is deprecated.  Use robotsoccer-msg:ball_x instead.")
  (ball_x m))

(cl:ensure-generic-function 'ball_y-val :lambda-list '(m))
(cl:defmethod ball_y-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:ball_y-val is deprecated.  Use robotsoccer-msg:ball_y instead.")
  (ball_y m))

(cl:ensure-generic-function 'ball_vx-val :lambda-list '(m))
(cl:defmethod ball_vx-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:ball_vx-val is deprecated.  Use robotsoccer-msg:ball_vx instead.")
  (ball_vx m))

(cl:ensure-generic-function 'ball_vy-val :lambda-list '(m))
(cl:defmethod ball_vy-val ((m <currentlocations>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:ball_vy-val is deprecated.  Use robotsoccer-msg:ball_vy instead.")
  (ball_vy m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <currentlocations>) ostream)
  "Serializes a message object of type '<currentlocations>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home1_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home1_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home1_theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home1_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home1_vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home2_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home2_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home2_theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home2_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'home2_vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away1_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away1_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away1_theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away1_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away1_vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away2_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away2_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away2_theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away2_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'away2_vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ball_x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ball_y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ball_vx))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'ball_vy))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <currentlocations>) istream)
  "Deserializes a message object of type '<currentlocations>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home1_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home1_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home1_theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home1_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home1_vy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home2_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home2_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home2_theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home2_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'home2_vy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away1_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away1_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away1_theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away1_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away1_vy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away2_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away2_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away2_theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away2_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'away2_vy) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ball_x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ball_y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ball_vx) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'ball_vy) (roslisp-utils:decode-single-float-bits bits)))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<currentlocations>)))
  "Returns string type for a message object of type '<currentlocations>"
  "robotsoccer/currentlocations")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'currentlocations)))
  "Returns string type for a message object of type 'currentlocations"
  "robotsoccer/currentlocations")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<currentlocations>)))
  "Returns md5sum for a message object of type '<currentlocations>"
  "5b2e4772dd173693302dadcdff14c696")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'currentlocations)))
  "Returns md5sum for a message object of type 'currentlocations"
  "5b2e4772dd173693302dadcdff14c696")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<currentlocations>)))
  "Returns full string definition for message of type '<currentlocations>"
  (cl:format cl:nil "Header header~%float32 home1_x~%float32 home1_y~%float32 home1_theta~%float32 home1_vx~%float32 home1_vy~%float32 home2_x~%float32 home2_y~%float32 home2_theta~%float32 home2_vx~%float32 home2_vy~%float32 away1_x~%float32 away1_y~%float32 away1_theta~%float32 away1_vx~%float32 away1_vy~%float32 away2_x~%float32 away2_y~%float32 away2_theta~%float32 away2_vx~%float32 away2_vy~%float32 ball_x~%float32 ball_y~%float32 ball_vx~%float32 ball_vy~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'currentlocations)))
  "Returns full string definition for message of type 'currentlocations"
  (cl:format cl:nil "Header header~%float32 home1_x~%float32 home1_y~%float32 home1_theta~%float32 home1_vx~%float32 home1_vy~%float32 home2_x~%float32 home2_y~%float32 home2_theta~%float32 home2_vx~%float32 home2_vy~%float32 away1_x~%float32 away1_y~%float32 away1_theta~%float32 away1_vx~%float32 away1_vy~%float32 away2_x~%float32 away2_y~%float32 away2_theta~%float32 away2_vx~%float32 away2_vy~%float32 ball_x~%float32 ball_y~%float32 ball_vx~%float32 ball_vy~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <currentlocations>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <currentlocations>))
  "Converts a ROS message object to a list"
  (cl:list 'currentlocations
    (cl:cons ':header (header msg))
    (cl:cons ':home1_x (home1_x msg))
    (cl:cons ':home1_y (home1_y msg))
    (cl:cons ':home1_theta (home1_theta msg))
    (cl:cons ':home1_vx (home1_vx msg))
    (cl:cons ':home1_vy (home1_vy msg))
    (cl:cons ':home2_x (home2_x msg))
    (cl:cons ':home2_y (home2_y msg))
    (cl:cons ':home2_theta (home2_theta msg))
    (cl:cons ':home2_vx (home2_vx msg))
    (cl:cons ':home2_vy (home2_vy msg))
    (cl:cons ':away1_x (away1_x msg))
    (cl:cons ':away1_y (away1_y msg))
    (cl:cons ':away1_theta (away1_theta msg))
    (cl:cons ':away1_vx (away1_vx msg))
    (cl:cons ':away1_vy (away1_vy msg))
    (cl:cons ':away2_x (away2_x msg))
    (cl:cons ':away2_y (away2_y msg))
    (cl:cons ':away2_theta (away2_theta msg))
    (cl:cons ':away2_vx (away2_vx msg))
    (cl:cons ':away2_vy (away2_vy msg))
    (cl:cons ':ball_x (ball_x msg))
    (cl:cons ':ball_y (ball_y msg))
    (cl:cons ':ball_vx (ball_vx msg))
    (cl:cons ':ball_vy (ball_vy msg))
))
