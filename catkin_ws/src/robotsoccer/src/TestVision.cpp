//============================================================================
// Name : ComputerVision.cpp
// Author : Luke Hsiao, Clover Wu
// Version :
// Copyright : Copyright 2015 Team Vektor Krum
// Description : This program receives video data from the Soccer Field's
// overhead camera, processes the images, and outputs
// the (x,y) positions of all 4 robots, the ball, and all
// robot's orientations.
//============================================================================

#include "TestComputerVision.h"
#include <math.h>

using namespace cv;

//pixel to meter conversion

float testConvertPixelToMeter(int pixel)
{
	//int pixelToMeterRatio = 215;
	float pixelToMeterRatio = 183.0;
	float meter = pixel/pixelToMeterRatio;
	// printf("meter: %f\n", meter);
	return meter;
}

//degree to radian conversion

float testConvertDegreeToRadian(int degree)
{
	float radian;
	float degreeRatio =  0.0174533;
	radian = degree *degreeRatio;
	//printf("%f",radian);
	return radian;
}

float testTrigComp ( float L)
{
	float cameraHeight = 3.45;
	float robotHeight = 0.127;
	float theta = atan(L/cameraHeight);
	float trueL = (cameraHeight - robotHeight) * tan ( theta);
	//printf("trigComp: %f\n",L - trueL);
	return trueL;
}

int main(int argc, char* argv[]) {

     /***********************Ros Publisher************************************/

    ros::init(argc, argv, "computer_vision");
    ros::NodeHandle n;
    ros::Publisher publisher = n.advertise<robotsoccer::locations>("locTopic", 1000);
    ros::Rate loop_rate(40);

    /************************************************************************/

    unsigned int printCounter = 0;
    while(ros::ok()) {

        //ros::Time timestamp = 0;

        /***********************Ros Publisher************************************/

        // Create message object
        robotsoccer::locations coordinates;
        // Fill message object with values

        coordinates.ball_x = testConvertPixelToMeter(-70);
        coordinates.ball_y =  testConvertPixelToMeter(120);


        coordinates.home1_x = testTrigComp(testConvertPixelToMeter(-150));
        coordinates.home1_y = testTrigComp(testConvertPixelToMeter(20));
        coordinates.home1_theta = testConvertDegreeToRadian(130);

	    //printf(" coordinates.home1_theta: %f\n", coordinates.home1_theta);

        coordinates.home2_x = testConvertPixelToMeter(0);
        coordinates.home2_y = testConvertPixelToMeter(0);
        coordinates.home2_theta = testConvertDegreeToRadian(0);

        coordinates.away1_x = testConvertPixelToMeter(30);
        coordinates.away1_y = testConvertPixelToMeter(40);
        coordinates.away1_theta = testConvertDegreeToRadian(180);

        coordinates.away2_x = testConvertPixelToMeter(0);
        coordinates.away2_y = testConvertPixelToMeter(0);
        coordinates.away2_theta = testConvertDegreeToRadian(0);

        //coordinates.header.stamp = timestamp;


        // Print values to ROS console
        if (!(printCounter%PRINT_FREQ)) {
            ROS_INFO("\n  timestamp: %u.%09u\n  ", coordinates.header.stamp.sec, coordinates.header.stamp.nsec);
            ROS_INFO("\n  Ball_x: %f\n  Ball_y: %f\n", coordinates.ball_x, coordinates.ball_y);
            ROS_INFO("\n  home1_x: %f\n  home1_y: %f home1_theta: %f\n", coordinates.home1_x, coordinates.home1_y,  coordinates.home1_theta);
            ROS_INFO("\n  home2_x: %f\n  home2_y: %f home2_theta: %f\n", coordinates.home2_x, coordinates.home2_y,coordinates.home2_theta);
            ROS_INFO("\n  away1_x: %f\n  away1_y: %f away1_theta: %f\n", coordinates.away1_x, coordinates.away1_y,coordinates.away1_theta);
            ROS_INFO("\n  away2_x: %f\n  away2_y: %f away2_theta: %f\n", coordinates.away2_x, coordinates.away2_y, coordinates.away2_theta);
        }

        // Publish message
        publisher.publish(coordinates);
        // Waits the necessary time between message publications to meet the
        // specified frequency set above (ros::Rate loop_rate(10);)
        loop_rate.sleep();

        /************************************************************************/
	    printCounter++;
	}
	return 0;
}

