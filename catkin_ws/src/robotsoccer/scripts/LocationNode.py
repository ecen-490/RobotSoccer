#!/usr/bin/env python

import rospy
import copy
import cPickle as pickle
from CurrentLocations import *
from prediction import *
from robotsoccer.msg import *
from robotsoccer.srv import *


# Object used to communicate with command center and robot main. Needs to be able to be serialized
class CurrentLocationsService:
    def __init__(self):
        self.home1x = 0.0
        self.home1y = 0.0
        self.home1theta = 0.0
        self.home2x = 0.0
        self.home2y = 0.0
        self.home2theta = 0.0
        self.away1x = 0.0
        self.away1y = 0.0
        self.away1theta = 0.0
        self.away2x = 0.0
        self.away2y = 0.0
        self.away2theta = 0.0
        self.ballx = 0.0
        self.bally = 0.0

        # States used for prediction
        self.ballphat = np.matrix([[0.0],           # Ball x position, m
            [0.0],                                  # Ball y position, m
            [0.0],                                  # Ball x velocity, m/s
            [0.0],                                  # Ball y velocity, m/s
            [0.0],                                  # Ball x acceleration, m/s**2
            [0.0],                                  # Ball y acceleration, m/s**2
            [0.0],                                  # Ball x jerk, m/s**3
            [0.0]])                                 # Ball y jerk, m/s**3

        # States used for prediction
        self.home1phat = np.matrix([[0.0],          # Robot Home x position, m
            [0.0],                                  # Robot Home y position, m
            [0.0],                                  # Robot Home theta position, m
            [0.0],                                  # Robot Home x velocity, m/s
            [0.0],                                  # Robot Home y velocity, m/s
            [0.0],                                  # Robot Home theta velocity, m/s
            [0.0],                                  # Robot Home x acceleration, m/s**2
            [0.0],                                  # Robot Home y acceleration, m/s**2
            [0.0]])                                 # Robot Home theta acceleration, m/s**2

        # States used for prediction
        self.away1phat = np.matrix([[0.0],          # Robot Away x position, m
            [0.0],                                  # Robot Away y position, m
            [0.0],                                  # Robot Away theta position, m
            [0.0],                                  # Robot Away x velocity, m/s
            [0.0],                                  # Robot Away y velocity, m/s
            [0.0],                                  # Robot Away theta velocity, m/s
            [0.0],                                  # Robot Away x acceleration, m/s**2
            [0.0],                                  # Robot Away y acceleration, m/s**2
            [0.0]])                                 # Robot Away theta acceleration, m/s**2


# Server and listener used for the robot to pass movement command location information to the command center
class LocationNode:
    # Initialize
    def __init__(self):
        self.locations = CurrentLocations()
        self.pub = rospy.Publisher('current_locations', currentlocations, queue_size=10)

    # Defines what happens what happens when a location message from vision is sent
    def callback(self, data):
        self.locations.setLocationsFromMeasurement(data)

    # Used by command nothing else
    def getLocations(self, req):
        response = CurrentLocationsService()
        response.home1x = self.locations.home1.x
        response.home1y = self.locations.home1.y
        response.home1theta = self.locations.home1.theta
        #response.home2x = self.locations.home2.x
        #response.home2y = self.locations.home2.y
        #response.home2theta = self.locations.home2.theta
        response.away1x = self.locations.away1.x
        response.away1y = self.locations.away1.y
        response.away1theta = self.locations.away1.theta
        #response.away2x = self.locations.away2.x
        #response.away2y = self.locations.away2.y
        #response.away2theta = self.locations.away2.theta
        response.ballx = self.locations.ball.x
        response.bally = self.locations.ball.y
        return currentlocationsserviceResponse(pickle.dumps(response))

    def getPredictedLocations(self,req):
        time = req.time
        case = req.case
        response = CurrentLocationsService()

        if case == 1:
            getPredictedBall(self.locations.ball, self.locations.home1, self.locations.away1, time)
        elif case == 2:
            getPredictedStates(self.locations.ball, self.locations.home1, self.locations.away1, time)

        response.ballphat = self.locations.ball.phat
        response.away1phat = self.locations.away1.phat
        response.home1phat = self.locations.home1.phat
        return predictedLocationsResponse(pickle.dumps(response))

    # Sends current location message
    def postLocationMessage(self):
        msg = currentlocations()
        msg.home1_x = self.locations.home1.x
        msg.home1_y = self.locations.home1.y
        msg.home1_theta = self.locations.home1.theta
        msg.home1_vx = self.locations.home1.v_command[0,0]
        msg.home1_vy = self.locations.home1.v_command[1,0]
        #msg.home2_x = self.locations.home2.x
        #msg.home2_y = self.locations.home2.y
        #msg.home2_theta = self.locations.home2.theta
        msg.away1_x = self.locations.away1.x
        msg.away1_y = self.locations.away1.y
        msg.away1_theta = self.locations.away1.theta
        msg.away1_vx = self.locations.away1.xhat[4,0]
        msg.away1_vy = self.locations.away1.xhat[5,0]
        #msg.away2_x = self.locations.away2.x
        #msg.away2_y = self.locations.away2.y
        #msg.away2_theta = self.locations.away2.theta
        msg.ball_x = self.locations.ball.x
        msg.ball_y = self.locations.ball.y
        msg.ball_vx = self.locations.ball.xhat[3,0]
        msg.ball_vy = self.locations.ball.xhat[4,0]
        self.pub.publish(msg)

    # Update robot 1 velocity to observer
    def updateRobot1Velocity(self,data):
        velocity = np.matrix([[data.vx], [data.vy], [data.vtheta]])
        self.locations.home1.update_v_command(velocity)

    # Update robot 2 velocity to observer
    def updateRobot2Velocity(self,data):
        velocity = np.matrix([[data.vx], [data.vy], [data.vtheta]])
        self.locations.home2.update_v_command(velocity)

    # Starts the listener to vision data message and message server for locations used by robots
    def run(self):
        print "Starting location node"
        rospy.init_node('location_node', anonymous=True)
        rospy.Subscriber("locTopic", locations, self.callback)
        rospy.Subscriber("robot1_vcommand", robot1vcommand, self.updateRobot1Velocity)
        #rospy.Subscriber("robot2_vcommand", robot2_vcommand, self.updateRobot2Velocity)
        rospy.Service('currentlocationsservice', currentlocationsservice, self.getLocations)
        rospy.Service('predictedlocationsservice',predictedLocations,self.getPredictedLocations)

        rate = rospy.Rate(1/self.locations.ball.Ts) # 100 Hz
        while not rospy.is_shutdown():
            self.locations.updateKalmanFilter()
            self.postLocationMessage()
            rate.sleep()

if __name__ == '__main__':
    l = LocationNode()
    l.run()
