; Auto-generated. Do not edit!


(cl:in-package robotsoccer-msg)


;//! \htmlinclude kickerplate_robot1command.msg.html

(cl:defclass <kickerplate_robot1command> (roslisp-msg-protocol:ros-message)
  ((header
    :reader header
    :initarg :header
    :type std_msgs-msg:Header
    :initform (cl:make-instance 'std_msgs-msg:Header))
   (force
    :reader force
    :initarg :force
    :type cl:integer
    :initform 0))
)

(cl:defclass kickerplate_robot1command (<kickerplate_robot1command>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <kickerplate_robot1command>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'kickerplate_robot1command)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name robotsoccer-msg:<kickerplate_robot1command> is deprecated: use robotsoccer-msg:kickerplate_robot1command instead.")))

(cl:ensure-generic-function 'header-val :lambda-list '(m))
(cl:defmethod header-val ((m <kickerplate_robot1command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:header-val is deprecated.  Use robotsoccer-msg:header instead.")
  (header m))

(cl:ensure-generic-function 'force-val :lambda-list '(m))
(cl:defmethod force-val ((m <kickerplate_robot1command>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader robotsoccer-msg:force-val is deprecated.  Use robotsoccer-msg:force instead.")
  (force m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <kickerplate_robot1command>) ostream)
  "Serializes a message object of type '<kickerplate_robot1command>"
  (roslisp-msg-protocol:serialize (cl:slot-value msg 'header) ostream)
  (cl:let* ((signed (cl:slot-value msg 'force)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 4294967296) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <kickerplate_robot1command>) istream)
  "Deserializes a message object of type '<kickerplate_robot1command>"
  (roslisp-msg-protocol:deserialize (cl:slot-value msg 'header) istream)
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'force) (cl:if (cl:< unsigned 2147483648) unsigned (cl:- unsigned 4294967296))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<kickerplate_robot1command>)))
  "Returns string type for a message object of type '<kickerplate_robot1command>"
  "robotsoccer/kickerplate_robot1command")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'kickerplate_robot1command)))
  "Returns string type for a message object of type 'kickerplate_robot1command"
  "robotsoccer/kickerplate_robot1command")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<kickerplate_robot1command>)))
  "Returns md5sum for a message object of type '<kickerplate_robot1command>"
  "cc714a235c069799075b3dc13445b9db")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'kickerplate_robot1command)))
  "Returns md5sum for a message object of type 'kickerplate_robot1command"
  "cc714a235c069799075b3dc13445b9db")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<kickerplate_robot1command>)))
  "Returns full string definition for message of type '<kickerplate_robot1command>"
  (cl:format cl:nil "Header header~%int32 force  #send over a 0 for stop, 1 for soft, 2 for medium, 3 for hard kick~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'kickerplate_robot1command)))
  "Returns full string definition for message of type 'kickerplate_robot1command"
  (cl:format cl:nil "Header header~%int32 force  #send over a 0 for stop, 1 for soft, 2 for medium, 3 for hard kick~%================================================================================~%MSG: std_msgs/Header~%# Standard metadata for higher-level stamped data types.~%# This is generally used to communicate timestamped data ~%# in a particular coordinate frame.~%# ~%# sequence ID: consecutively increasing ID ~%uint32 seq~%#Two-integer timestamp that is expressed as:~%# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')~%# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')~%# time-handling sugar is provided by the client library~%time stamp~%#Frame this data is associated with~%# 0: no frame~%# 1: global frame~%string frame_id~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <kickerplate_robot1command>))
  (cl:+ 0
     (roslisp-msg-protocol:serialization-length (cl:slot-value msg 'header))
     4
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <kickerplate_robot1command>))
  "Converts a ROS message object to a list"
  (cl:list 'kickerplate_robot1command
    (cl:cons ':header (header msg))
    (cl:cons ':force (force msg))
))
