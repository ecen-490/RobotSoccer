
(cl:in-package :asdf)

(defsystem "robotsoccer-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils :std_msgs-msg
)
  :components ((:file "_package")
    (:file "robot1commands" :depends-on ("_package_robot1commands"))
    (:file "_package_robot1commands" :depends-on ("_package"))
    (:file "robot2commands" :depends-on ("_package_robot2commands"))
    (:file "_package_robot2commands" :depends-on ("_package"))
    (:file "commandcenter" :depends-on ("_package_commandcenter"))
    (:file "_package_commandcenter" :depends-on ("_package"))
    (:file "robot1vcommand" :depends-on ("_package_robot1vcommand"))
    (:file "_package_robot1vcommand" :depends-on ("_package"))
    (:file "locations" :depends-on ("_package_locations"))
    (:file "_package_locations" :depends-on ("_package"))
    (:file "kickerplate_robot1command" :depends-on ("_package_kickerplate_robot1command"))
    (:file "_package_kickerplate_robot1command" :depends-on ("_package"))
    (:file "currentlocations" :depends-on ("_package_currentlocations"))
    (:file "_package_currentlocations" :depends-on ("_package"))
  ))