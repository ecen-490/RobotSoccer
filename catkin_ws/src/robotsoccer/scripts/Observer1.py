import numpy as np

_scale = 1
_scale2 = 1
xP = {
    'A': np.matrix([[0,1],[0,-0.0006]]),        # A matrix for observer
    'B': np.matrix([[0],[0.5]]),                # B matrix for observer
    'C': np.matrix([[1,0]]),                    # C matrix for observer
    'K': np.matrix([[124.866/_scale, 48.8645/_scale]]),       # Observer coefficient
    'ki': 6*_scale2,                                    # Observer coefficient
    'kr': 124.866/_scale,                              # Observer coefficient
    'L': np.matrix([[244],[6.243e3]]),          # Correction coefficient used by xdhat
    'Ld': 12000,                                # Correction coefficient used by dhat
    # 'K': np.matrix([[32.1629, 21.7255]]),
    # 'ki_ctrl': 3,
    # 'kr': 32.1629,
    # 'L': np.matrix([[137.9],[2030]]),
    # 'Ld': 3000,
    'F_max': 60,                                # Maximum force applied to the system
    'max_step': 0.25,                           # Max step size
    'x_e': np.matrix([[0], [0]]),               # Equilibrium States
    'type': 'x'
}
        
yP = {
    'A': np.matrix([[0,1],[0,-0.0006]]),        # A matrix for observer
    'B': np.matrix([[0],[0.5]]),                # B matrix for observer
    'C': np.matrix([[1,0]]),                    # C matrix for observer
    'K': np.matrix([[124.866/_scale, 48.8645/_scale]]),       # Observer coefficient
    'ki': 6*_scale2,                                    # Observer coefficient
    'kr': 124.866/_scale,                              # Observer coefficient
    'L': np.matrix([[244],[6.243e3]]),          # Correction coefficient used by xdhat
    'Ld': 12000,                                # Correction coefficient used by dhat
    # 'K': np.matrix([[32.1629, 21.7255]]),
    # 'ki_ctrl': 3,
    # 'kr': 32.1629,
    # 'L': np.matrix([[137.9],[2030]]),
    # 'Ld': 3000,
    'F_max': 60,                                # Maximum force applied to the system
    'max_step': 0.25,                           # Max step size
    'x_e': np.matrix([[0], [0]]),               # Equilibrium States
    'type': 'y'
}
        
thetaP = {
    'A': np.matrix([[0,1],[0,-0.0006]]),        # A matrix for observer
    'B': np.matrix([[0],[50]]),                 # B matrix for observer
    'C': np.matrix([[1,0]]),                    # C matrix for observer
    #'K': np.matrix([[38.4718, 2.7479]]),        # Observer coefficient
    #'ki': 1.9099,                               # Observer coefficient
    #'kr': 38.4718,                              # Observer coefficient
    #'L': np.matrix([[137],[1.9236e4]]),         # Correction coefficient used by xhat
    #'Ld': 3.8197e3,                             # Correction coefficient used by dhat
    'K': np.matrix([[19.2928, 1.9436]]),
    'ki': 3.81,
    'kr': 19.2928,
    'L': np.matrix([[0.0972e4],[9.6463e4]]),
    'Ld': 1.9099e3,
    'F_max': 30,                                # Maximum force applied to the system
    'max_step': np.pi/4,                        # Max step size
    'x_e': np.matrix([[0], [0]]),               # Equilibrium States
    'type': 'theta'
}
        

class P:
    Ts = 0.02
    tau = 0.05

class Obsv:
    def __init__(self,sP):
        self.A = sP['A']                          # A matrix for observer
        self.B = sP['B']                          # B matrix for observer
        self.C = sP['C']                          # C matrix for observer
        self.K = sP['K']                          # Observer coefficient
        self.ki = sP['ki']                        # Observer coefficient
        self.kr = sP['kr']                        # Observer coefficient
        self.L = sP['L']                          # Correction coefficient used by xhat
        self.Ld = sP['Ld']                        # Correction coefficient used by dhat
        self.F_max = sP['F_max']                  # Maximum force applied to the system
        self.max_step = sP['max_step']            # Max step between current position and desired position.
        self.x_e = sP['x_e']                      # Equilibrium states
        self.type = sP['type']
        self.xhat = np.matrix([[0.0],[0.0]])      # Estimated state (for observer)
        self.dhat  = np.matrix([[0.0]])           # Estimate disturbance
        self.F =0                                 # Current force being supplied to the system
        self.F_e = 0                              # Equilibrium force
        self.error_dl =0                          # Computed error from the last time step
        self.integrator =0                        # Integrator used for error correction
        self.acceleration = 0.0                   # Acceleration state
        self.d = 0                                # desired position
        self.d_step = 0                           # desired step towards the desired position


    def updateObserver(self):

        self.getNextStep()

        N = 1
 
        for i in range(0,N):
            self.xhat = self.xhat + P.Ts/N*(self.A*(self.xhat-self.x_e)+self.B*(self.F-self.F_e+self.dhat))# +m.L*(x_c-m.C*xhat))
            self.dhat = 0#m.dhat + P.Ts/N*m.Ld*(m.xdot-m.C2*m.xhat)


        xhat_dot = (self.A*(self.xhat-self.x_e)+self.B*(self.F-self.F_e+self.dhat)) # Calculate xhat_dot	    
        self.acceleration = xhat_dot[1,0] # Grab acceleration from xhat_dot


        error = self.xhat[0,0]-self.d     # Calculate the positional error	    

        if np.abs(self.xhat[1,0])<0.5 and self.type != 'theta':    # If the velocity is small the integrator will be triggered
            self.integrator = self.integrator + (P.Ts/2)*(error+self.error_dl)

        self.error_dl = error  # Update the error


        # compute the state feedback controller
        F_tilde = -self.K*(self.xhat-self.x_e) + self.kr*(self.d_step) - self.ki*self.integrator# - dhat
        F_unsat = self.F_e + F_tilde
        self.F =  self.sat( F_unsat, self.F_max)
        #print(self.F)
        #self.Fq.put(self.F)
        # integrator anti-windup
        #if self.ki!=0 and self.type != 'theta':
           #self.integrator = self.integrator + P.Ts/self.ki*(self.F-F_unsat)
        #print('integrator',self.integrator)

    def  sat(self,u,limit):        # Saturates the force if it goes beyond the rails.
        if     u > limit:  
            out = limit
        elif u < -limit:   
            out = -limit
        else:                   
            out = u
        return out


    # Calculates the next step location depending on current location and desired location.
    def getNextStep(self):
        if self.type == 'theta':                        # The rotational movement is periodic with 2pi. It must account for that.
            self.adjustForPeriodicity()

        curr = self.xhat[0,0]
        if abs(curr-self.d) > self.max_step:            # If the difference between the current and desired position is greater than
            if curr < self.d:                           # the max step. The path to get to the desired position is broken down 
                self.d_step = curr +self.max_step       # into smaller steps according to the max step size. This allows for smoother
            else:                                       # and quicker control.
                self.d_step = curr - self.max_step
        else:
            self.d_step = self.d

    # This function accounts for the periodicity of theta. The robot should never need to rotate more than 180 degrees.
    def adjustForPeriodicity(self):
        curr = self.xhat[0,0]
        i = 0
        while curr - self.d < -1*np.pi:
            curr = curr + 2*np.pi
            i = i + 1
            if i > 4:
                print("first", i)
        i = 0
        while curr + self.d > np.pi:
            curr = curr - 2*np.pi
            i = i + 1
            if i > 4:
                print("second", i)
 #       while (abs(curr - self.d) > np.pi):   # Sees if the distance between the two is greater than 180 degrees
 #           if curr > self.d:              # If the current position is greater than the desired position
 #               curr = curr - 2*np.pi      # Subtract 360 degrees from the current position
 #           else:                          # Else the current position is less than the desired position
 #               curr = curr + 2*np.pi      # Add 360 degrees to the current posistion
        self.xhat[0,0] = curr              # Update current position
        if (curr - self.d > (-np.pi)) and (curr + self.d < np.pi):
            pass
        else:
            print("error")
            print("curr", curr*180/np.pi)
            print("theta d", self.d*180/np.pi)
        #print("theta curr", curr*180/np.pi)

def printContent(m):
        print('A', m.A)
        print('B', m.B)
        print('C', m.C)
        print('K', m.K)
        print('ki', m.ki)
        print('kr',m.kr)
        print('L', m.L)
        print('Ld', m.F_max)
        print('max_step', m.max_step)
        print('xhat', m.xhat)
        print('dhat', m.dhat)
        print('F',m.F)
        print('errod_dl',m.error_dl)
        print('integrator', m.integrator)
        print('acceleration',m.acceleration)
        print('d',m.d)
        #print('max_step',m.max_step)
        print('d_step',m.d_step)


x = Obsv(xP)
y = Obsv(yP)
theta=Obsv(thetaP)


# adjust for periodicity test
if __name__ == "__main__":
    theta.xhat[0,0] = 10*np.pi/180
    theta.d = 300*np.pi/180
    theta.getNextStep()
    print('theta curr', theta.xhat[0,0]*180/np.pi) # Should be 370 degrees
    print('theta next step', theta.d_step*180/np.pi) # should be 325 degrees

    theta.xhat[0,0] = 300*np.pi/180
    theta.d = 10*np.pi/180
    theta.getNextStep()
    print('theta curr', theta.xhat[0,0]*180/np.pi) # Should be -60 degrees
    print('theta next step', theta.d_step*180/np.pi) # should be -15 degrees

    theta.xhat[0,0] = 100*np.pi/180
    theta.d = 300*np.pi/180
    theta.getNextStep()
    print('theta curr', theta.xhat[0,0]*180/np.pi) # Should be 460 degrees
    print('theta next step', theta.d_step*180/np.pi) # should be 415 degrees
