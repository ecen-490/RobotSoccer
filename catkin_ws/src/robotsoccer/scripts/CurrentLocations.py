#!/usr/bin/env python

import numpy as np
from parameters import *
import cPickle as pickle
from kalmanFilter import robotAway, robotHome, Ball
import rospy


# Holds location information for the game. Robots and ball
class CurrentLocations:

    # Initialize
    def __init__(self):
        self.home1 = robotHome()
        #self.home2 = robotHome()
        self.ball = Ball()
        self.away1 = robotAway()
        #self.away2 = robotAway()
        self.flag_init_pos = True

    # Set the locations from the vision data
    def setLocationsFromMeasurement(self, data):

        flip = 1   # Used to flip the coordinates between home and away
        rad = 0    #
        flip_c4 = 1
        rotate = 0

        if away_flag == 0: # At home
            flip = -1
            rad = np.pi

        if camera_4 == 1: # need to rotate by 180 degrees
            flip_c4 = -1
            rotate = np.pi
	
        nowTime = rospy.get_rostime()
        time_elapsed = nowTime - data.header.stamp # Time elapsed since a picture was taken and the data was received,
        time_elapsed= (float(time_elapsed.secs) + float(time_elapsed.nsecs /1000000000.0))
        #print time_elapsed
        #time_elapsed=0
                
        measured_positions_home1 = np.matrix([[data.home1_x*flip*flip_c4], [data.home1_y*flip*flip_c4], np.remainder([data.home1_theta + rad + rotate],2*np.pi)])
        #measured_positions_home2 = np.matrix([[data.home2_x*flip], [data.home2_y*flip], [data.home2_theta + rad]])
        measured_positions_away1 = np.matrix([[data.away1_x*flip*flip_c4], [data.away1_y*flip*flip_c4], np.remainder([data.away1_theta + rad + rotate],2*np.pi)])
        #measured_positions_away2 = np.matrix([[data.away2_x*flip], [data.away2_y*flip], [data.away2_theta + rad]])
        measured_positions_ball = np.matrix([[data.ball_x], [data.ball_y]])*flip*flip_c4

        #print('measured_positions_home1', measured_positions_home1)
        #print('measured_positions_ball', measured_positions_ball)

        if self.flag_init_pos:
            self.home1.init_pos(data.home1_x, data.home1_y, data.home1_theta)
            #self.home2.init_pos(data.home2_x, data.home2_y, data.home2_theta)
            self.away1.init_pos(data.away1_x, data.away1_y, data.away1_theta)
            #self.away2.init_pos(data.away2_x, data.away2_y, data.away2_theta)
            self.ball.init_pos(data.ball_x, data.ball_y)
            self.flag_init_pos = False
        else:
            self.home1.update_pos_measured(measured_positions_home1, time_elapsed)
            #self.home2.update_pos_measured(measured_positions_home2, time_elapsed)
            self.away1.update_pos_measured(measured_positions_away1, time_elapsed)
            #self.away2.update_pos_measured(measured_positions_away2, time_elapsed)
            self.ball.update_pos_measured(measured_positions_ball, time_elapsed)

    # Updates the kalman filter. Expects constant rate
    def updateKalmanFilter(self):
        if not (self.flag_init_pos):
            self.home1.kalman_filter()
            #self.home2.kalman_filter()
            self.away1.kalman_filter()
            #self.away2.kalman_filter()
            self.ball.kalman_filter()
