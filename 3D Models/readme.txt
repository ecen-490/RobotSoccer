These files have been created using the parts provided by BYU ECEn 490 Robot Soccer.

Disclaimer:
There might be some slight differences in the exact piece that you have compared to the model (extra screws or holes, etc.). I tried to keep the pieces simple enough that it wouldn’t be overwhelming to use on your computer.

The lengths in the dimension files and CAD files should be accurate within 1/16”. If you need more precision, you should measure yourself. (Make sure you double check the sizes if you are using these files to design 3D printed parts that have very small tolerances).

Fast and Full Files:
There are fast and full files. Both have the same exact dimensions (they are created from the same file). The fast files are objects that are merged sooner so they lose the color/texture data and some of the mobility but they are easier on the computers. The full files have objects that are grouped so they all move together but not merged so each individual piece could be accessed from any level (like the small wheels on the omni-wheel).

Not every piece has a fast file. If you can’t find a file within the fast folder, check the full folder.

**Electrical Boards**
The measurements in the dimensions files are correct but the 3D model will not have all of the components on the board. A space will be blocked off reflecting the maximum heights. If you plan on optimizing or utilizing the space around these boards, you may need more detailed measurements

Software:
The files are created in 123d Design from www.123dapp.com. You can either export it to another type if you’d like.


If you wish to contribute, send me the files and I can add them. If you find any errors or incorrect information, also please let me know.
Files created by Carson Bunker (carson.bunker@gmail.com)