#include "ComputerVision.h"
#include "Ball.h"
#include "Robot.h"
#include "Object.h"
#include <math.h>

#define KNRM  "\x1B[0m"     //normal
#define KRED  "\x1B[31m"    //red
#define KGRN  "\x1B[32m"    //green
#define KYEL  "\x1B[33m"    //yellow
#define KBLU  "\x1B[34m"    //blue
#define KMAG  "\x1B[35m"    //megenta
#define KCYN  "\x1B[36m"    //cyan
#define KWHT  "\x1B[37m"    //white

using namespace cv;

class hsvObject
{	//[Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]
    public:
        int Hmin;
	    int Smin;
	    int Vmin;
	    int Hmax;
	    int Smax;
	    int Vmax;
};

class colorPresets
{
   public:
	hsvObject red;  
	hsvObject purple; 
	hsvObject blue; 
	hsvObject yellow; 
	hsvObject orange; 
	hsvObject green; 
	
};

colorPresets presets;

//address of webcam(s)
string videoStreamAddress = "http://192.168.1.10:8080/stream?topic=/image&dummy=param.mjpg";

//ignores
bool ignoreHome2 = false;
bool ignoreAway1 =false;
bool ignoreAway2 =false;

// Change this parameter to determine which team we are on!
// Either set it to HOME or AWAY
int TEAM = HOME;

// Change this parameter to switch to performance, no GUI mode
// Either set it to GUI or NO_GUI
int PERF_MODE = GUI;

// Field variables
int field_width;
int field_height;
int field_center_x;
int field_center_y;

//initial min and max HSV filter values.
//these will be changed using trackbars
int H_MIN = 0;
int H_MAX = 256;
int S_MIN = 0;
int S_MAX = 256;
int V_MIN = 0;
int V_MAX = 256;

//min and max field variable values
int field_height_min = 0;
int field_width_min = 0;
int field_center_x_min = 0;
int field_center_y_min = 0;
int field_height_max = FRAME_HEIGHT + 300;
int field_width_max = FRAME_WIDTH + 300;
int field_center_x_max = FRAME_WIDTH;
int field_center_y_max = FRAME_HEIGHT;

//max number of objects to be detected in frame
const int MAX_NUM_OBJECTS=50;

//minimum and maximum object area
const int MIN_OBJECT_AREA = 7*7;
const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH/1.5;

//names that will appear at the top of each window
const string windowName = "Original Image";
const string windowName1 = "HSV Image";
const string windowName2 = "Thresholded Image";
const string windowName3 = "After Morphological Operations";
const string trackbarWindowName = "Trackbars";

// Camera Calibration Data
double dist_coeff[5][1] = {  {-0.3647790},
                             {0.184763},
                             {0.003989429},
                             {-0.0003392181},
                             {-0.06141144}
                           };

double cam_matrix[3][3] = {  {513,  0.0,  315.6},
                             {0.0,  513.94,  266.72},
                             {0.0,  0.0,  1.0}
                          };                          
                          
// Declaration of the 5 objects
Robot home1(HOME), home2(HOME);
Robot away1(AWAY), away2(AWAY);
Ball ball;

// threading
sem_t frameRawSema;
std::queue<FrameRaw> frameRawFifo;
sem_t frameMatSema;
std::queue<FrameMat> frameMatFifo;
Mat cameraFeed;
Mat HSV;
sem_t trackBallBegin;
sem_t trackHome1Begin;
sem_t trackHome2Begin;
sem_t trackAway1Begin;
sem_t trackAway2Begin;
sem_t trackBallEnd;
sem_t trackHome1End;
sem_t trackHome2End;
sem_t trackAway1End;
sem_t trackAway2End;

//pixel to meter conversion
float convertPixelToMeter(int pixel)
{
	//int pixelToMeterRatio = 215;	
	float pixelToMeterRatio = 183.0;
	float meter = pixel/pixelToMeterRatio;	
	// printf("meter: %f\n", meter);
	return meter;
}

//degree to radian conversion
float convertDegreeToRadian(int degree)
{
	float radian;
	float degreeRatio =  0.0174533;
	radian = degree *degreeRatio;
	//printf("%f",radian);
	return radian;
}

// Compenstates for different heights used on the robot
float trigComp ( float L)
{
	float theta = atan(L/cameraHeight);
	float trueL = (cameraHeight - robotHeight) * tan ( theta);
	//printf("trigComp: %f\n",L - trueL);
	return trueL;
}

string getColorfromCode(int code)
{
	switch (code)
	{
		case 1: 
			return "Red";
		case 2:
			return "Purple";
		case 3: 
			return "Blue";
		case 4:
			return "Yellow";
		case 5:
			return "Orange";
		case 6:
			return "Green";
		default:
			return 0;
	}
}

string saveColorSettingsPrinter(Robot * bot)
{	 
	std::stringstream ss;
	ss << getColorfromCode(bot->getColor()) << " " << bot->getHSVmin().val[0] << " " << bot->getHSVmin().val[1] << " " << bot->getHSVmin().val[2] << " " << bot->getHSVmax().val[0] << " " << bot->getHSVmax().val[1] << " " << bot->getHSVmax().val[2] << "\n";
	
	std::string output= ss.str();
	//std::cout << output;
	return output;
}

void saveColorSettings() {
    // Open file
    std::ofstream of("/home/ecestudent/RobotSoccer/catkin_ws/presetColors.data");

    if (!of.is_open()) {
        printf("\n\n\nERROR OPENING SETTINGS FILE!\n\n\n");
    return;
    }

    // Print human-readable header for settings file
    of << "#############################################################" << "\n";
    of << "# This settings file contains the color calibration settings" << "\n";
    of << "# Format: " << "\n";
    of << "# [RobotName] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]" << "\n";
    of << "# [Ball] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]" << "\n";
    of << "# [Field] [x_center_pos] [y_center_pos] [width] [height]" << "\n";
    of << "#############################################################" << "\n";
    of << "\n\n";

    // Robot Save format:
    // [RobotName] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]
  
    if(home1.getColor()==1){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==1){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==1) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==1) {
        of << saveColorSettingsPrinter(&away2);
    }  else {
		of << "Red" << " " <<  presets.red.Hmin << " " << presets.red.Smin << " " <<  presets.red.Vmin;
		of << " " <<  presets.red.Hmax << " " <<  presets.red.Smax << " " <<  presets.red.Vmax << "\n";
    }
	  
    if(home1.getColor()==2){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==2){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==2) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==2) {
	    of << saveColorSettingsPrinter(&away2);
    }  else {
		of << "Purple" << " " <<  presets.purple.Hmin << " " << presets.purple.Smin << " " <<  presets.purple.Vmin;
		of << " " <<  presets.purple.Hmax << " " <<  presets.purple.Smax << " " <<  presets.purple.Vmax << "\n";
    }
	
	  
    if(home1.getColor()==3){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==3){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==3) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==3) {
	    of << saveColorSettingsPrinter(&away2);
    } else {
		of << "Blue" << " " <<  presets.blue.Hmin << " " << presets.blue.Smin << " " <<  presets.blue.Vmin;
		of << " " <<  presets.blue.Hmax << " " <<  presets.blue.Smax << " " <<  presets.blue.Vmax << "\n";
    }
  
  	  
    if(home1.getColor()==4){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==4){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==4) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==4) {
	    of << saveColorSettingsPrinter(&away2);
    } else {
		of << "Yellow" << " " <<  presets.yellow.Hmin << " " << presets.yellow.Smin << " " <<  presets.yellow.Vmin;
		of << " " <<  presets.yellow.Hmax << " " <<  presets.yellow.Smax << " " <<  presets.yellow.Vmax << "\n";
    }
  
    if(home1.getColor()==5){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==5){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==5) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==5) {
		of << saveColorSettingsPrinter(&away2);
    } else {
		of << "Orange" << " " <<  presets.orange.Hmin << " " << presets.orange.Smin << " " <<  presets.orange.Vmin;
		of << " " <<  presets.orange.Hmax << " " <<  presets.orange.Smax << " " <<  presets.orange.Vmax << "\n";
    }
  
    if(home1.getColor()==6){
		of << saveColorSettingsPrinter(&home1);
    } else if (home2.getColor()==6){
		of << saveColorSettingsPrinter(&home2);
    } else if (away1.getColor() ==6) {
		of << saveColorSettingsPrinter(&away1);
    } else if (away2.getColor() ==6) {
		of << saveColorSettingsPrinter(&away2);
    }  else {
		of << "Green" << " " <<  presets.green.Hmin << " " << presets.green.Smin << " " <<  presets.green.Vmin;
		of << " " <<  presets.green.Hmax << " " <<  presets.green.Smax << " " <<  presets.green.Vmax << "\n";
    }

    // close file
    of.close();
    printf("Color Settings Saved!\n");
}

// Saves all of the pertinent calibration settings to human-readable file
void saveSettings() {
    // Open file
    std::ofstream of("/home/ecestudent/RobotSoccer/catkin_ws/settings.data");

    if (!of.is_open()) {
        printf("\n\n\nERROR OPENING SETTINGS FILE!\n\n\n");
        return;
    }

    const string h1 = "Home1";
    const string h2 = "Home2";
    const string a1 = "Away1";
    const string a2 = "Away2";
    const string b = "Ball";
    const string f = "Field";
    int tempH, tempS, tempV;

    // Print human-readable header for settings file
    of << "#############################################################" << "\n";
    of << "# This settings file contains the color calibration settings" << "\n";
    of << "# Format: " << "\n";
    of << "# [RobotName] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]" << "\n";
    of << "# [Ball] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]" << "\n";
    of << "# [Field] [x_center_pos] [y_center_pos] [width] [height]" << "\n";
    of << "#############################################################" << "\n";
    of << "\n\n";

    // Ball Save format:
    // [Ball] [Hmin] [Smin] [Vmin] [Hmax] [Smax] [Vmax]
    tempH = ball.getHSVmin().val[0];
    tempS = ball.getHSVmin().val[1];
    tempV = ball.getHSVmin().val[2];
    of << b << " " <<  tempH << " " << tempS << " " <<  tempV;
    tempH = ball.getHSVmax().val[0];
    tempS = ball.getHSVmax().val[1];
    tempV = ball.getHSVmax().val[2];
    of << " " <<  tempH << " " <<  tempS << " " <<  tempV << "\n";

    // Field Save format:
    // [Field] [x_pos] [y_pos] [width] [height]
    of << f << " " << field_center_x << " " << field_center_y;
    of << " " << field_width << " " <<  field_height << "\n";

    // close file
    of.close();
    printf("Settings Saved!\n");
}

// Reads in saved settings from settings.data and stores them in objects
void restoreSettings() {
    const string h1 = "Home1";
    const string h2 = "Home2";
    const string a1 = "Away1";
    const string a2 = "Away2";
    const string b = "Ball";
    const string f = "Field";
    int tempH, tempS, tempV;

    std::stringstream ss;
    std::ifstream in("/home/ecestudent/RobotSoccer/catkin_ws/settings.data");
    if (!in.good()) {
        printf("\n\n\nERROR, Couldn't open file\n\n\n");
        return;
    }

    // char line[MAX_CHARS];
    string ID;
    string line;

    // Parse File line by line
    while(getline(in, line)) {
        if (line.c_str()[0] == '#') {
            continue; //skip comments
        }

        ss.str(line);
        ss >> ID;

        if (ID == b) {
            ss >> tempH;
            ss >> tempS;
            ss >> tempV;
            ball.setHSVmin(Scalar(tempH, tempS, tempV));
            ss >> tempH;
            ss >> tempS;
            ss >> tempV;
            ball.setHSVmax(Scalar(tempH, tempS, tempV));
        } else if (ID == f) {
            ss >> field_center_x;
            ss >> field_center_y;
            ss >> field_width;
            ss >> field_height;
        }

        ss.clear();
    }

    in.close();
    printf("Settings Restored!\n");
}

//-----------------------------------------------------------------------------
// Utility Functions Shared by Classes
//-----------------------------------------------------------------------------

// This function is called whenever a trackbar changes
void on_trackbar( int, void* ) {
    // Does nothing
}

string intToString(int number) {
    std::stringstream ss;
    ss << number;
    return ss.str();
}

// Runs the undistortion
void undistortImage(Mat &source) {
    // Setup Distortion matrices
    Mat cameraMatrix = Mat(3, 3, CV_64F, cam_matrix); // read in 64-bit doubles
    Mat distCoeffs = Mat(5, 1, CV_64F, dist_coeff);

    double y_shift = 60;
    double x_shift = 70;
    int enlargement = 185;
    Size imageSize = source.size();
    imageSize.height += enlargement;
    imageSize.width += enlargement;
    Mat temp = source.clone();

    Mat newCameraMatrix = cameraMatrix.clone();

    // Adjust the position of the newCameraMatrix
    // at() is a templated function so <double> is necessary
    newCameraMatrix.at<double>(1,2) += y_shift; //shift the image down
    newCameraMatrix.at<double>(0,2) += x_shift; //shift the image right

    Mat map1;
    Mat map2;
    initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(), newCameraMatrix, imageSize, CV_16SC2, map1, map2);

    remap(temp, source, map1, map2, INTER_LINEAR);
}

void createHSVTrackbars() {
	//create window for trackbars
	namedWindow(trackbarWindowName,0);

	//create trackbars and insert them into window
	//3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.H_LOW),
	//the max value the trackbar can move (eg. H_HIGH), 
	//and the function that is called whenever the trackbar is moved(eg. on_trackbar)
	createTrackbar( "H_MIN", trackbarWindowName, &H_MIN, H_MAX, on_trackbar );
	createTrackbar( "H_MAX", trackbarWindowName, &H_MAX, H_MAX, on_trackbar );
	createTrackbar( "S_MIN", trackbarWindowName, &S_MIN, S_MAX, on_trackbar );
	createTrackbar( "S_MAX", trackbarWindowName, &S_MAX, S_MAX, on_trackbar );
	createTrackbar( "V_MIN", trackbarWindowName, &V_MIN, V_MAX, on_trackbar );
	createTrackbar( "V_MAX", trackbarWindowName, &V_MAX, V_MAX, on_trackbar );
}

//Converts from image coordinates to field coordinates
Point convertCoordinates(Point imageCoordinates) {
    int img_x = imageCoordinates.x;
    int img_y = imageCoordinates.y;

    int field_x;
    int field_y;
    Point result;

    field_x = img_x - (field_width/2);

    field_y = (field_height/2) - img_y;

    result.x = field_x;
    result.y = field_y;
    return result;
}

// This function reduces the noise of the image by eroding the image first
// then dialating the remaining image to produce cleaner objects
void morphOps(Mat &thresh) {
    //create structuring element that will be used to "dilate" and "erode" image.

    //the element chosen here is a 3px by 3px rectangle
    Mat erodeElement = getStructuringElement( MORPH_RECT,Size(2,2));
    //dilate with larger element so make sure object is nicely visible
    Mat dilateElement = getStructuringElement( MORPH_RECT,Size(3,3));

    erode(thresh,thresh,erodeElement);
    dilate(thresh,thresh,dilateElement);

    erodeElement = getStructuringElement( MORPH_RECT,Size(3,3));
    //dilate with larger element so make sure object is nicely visible
    dilateElement = getStructuringElement( MORPH_RECT,Size(6,6));

    erode(thresh,thresh,erodeElement);
    dilate(thresh,thresh,dilateElement);
}

//-----------------------------------------------------------------------------
//  End of Utility Functions Shared by Classes
//-----------------------------------------------------------------------------

// Generates prompts for field calibration of size/center
void calibrateField(VideoCapture capture) {
    Mat cameraFeed;
    int field_origin_x;
    int field_origin_y;

    //create window for trackbars
    namedWindow(trackbarWindowName,0);

    //create trackbars and insert them into window
    //3 parameters are: the address of the variable that is changing when the trackbar is moved(eg.H_LOW),
    //the max value the trackbar can move (eg. H_HIGH),
    //and the function that is called whenever the trackbar is moved(eg. on_trackbar)
    createTrackbar( "Field Center Y", trackbarWindowName, &field_center_y_min, field_center_y_max, on_trackbar );
    createTrackbar( "Field Center X", trackbarWindowName, &field_center_x_min, field_center_x_max, on_trackbar );
    createTrackbar( "Field Height", trackbarWindowName, &field_height_min, field_height_max, on_trackbar );
    createTrackbar( "Field Width", trackbarWindowName, &field_width_min, field_width_max, on_trackbar );

    // Set Trackbar Initial Positions
    setTrackbarPos( "Field Center Y", trackbarWindowName, field_center_y);
    setTrackbarPos( "Field Center X", trackbarWindowName, field_center_x);
    setTrackbarPos( "Field Height", trackbarWindowName, field_height);
    setTrackbarPos( "Field Width", trackbarWindowName, field_width);

    // Wait forever until user sets the values
    while (1) {

        capture.read(cameraFeed);
        //  undistortImage(cameraFeed);

        // Wait for user to set values
        field_center_y = getTrackbarPos( "Field Center Y", trackbarWindowName);
        field_center_x = getTrackbarPos( "Field Center X", trackbarWindowName);
        field_height = getTrackbarPos( "Field Height", trackbarWindowName);
        field_width = getTrackbarPos( "Field Width", trackbarWindowName);

        field_origin_x = field_center_x - (field_width/2);
        field_origin_y = field_center_y - (field_height/2);

        Rect fieldOutline(field_origin_x, field_origin_y, field_width, field_height);

        // Draw centering lines
        Point top_mid(field_center_x, field_origin_y);
        Point bot_mid(field_center_x, field_origin_y+field_height);
        Point left_mid(field_origin_x, field_center_y);
        Point right_mid(field_origin_x+field_width, field_center_y);
        line(cameraFeed,top_mid, bot_mid, Scalar(200,200,200), 1, 8, 0);
        line(cameraFeed,left_mid, right_mid, Scalar(200,200,200), 1, 8, 0);

        // Draw outline
        rectangle(cameraFeed,fieldOutline,Scalar(255,255,255), 1, 8 ,0);
        imshow(windowName,cameraFeed);

        char pressedKey;
        pressedKey = cvWaitKey(50); // Wait for user to press 'Enter'
        if (pressedKey == '\n') {
            field_center_y = getTrackbarPos( "Field Center Y", trackbarWindowName);
            field_center_x = getTrackbarPos( "Field Center X", trackbarWindowName);
            field_height = getTrackbarPos( "Field Height", trackbarWindowName);
            field_width = getTrackbarPos( "Field Width", trackbarWindowName);

            printf("\n\nField Values Saved!\n");
            printf("Field Center Y: %d\n", field_center_y);
            printf("Field Center X: %d\n", field_center_x);
            printf("Field Width: %d\n", field_width);
            printf("Field Height: %d\n", field_height);
            destroyAllWindows();
            return;
        }
    }
}

int printColors()
{
	int home1 = 1;
	int home2 = 2;
	int away1 = 3;
	int away2 = 4;
	int color;
	bool valid = false;
	printf("%s1: Red\n", KRED);
	printf("%s2: Purple\n", KMAG);   
	printf("%s3: Blue\n", KBLU);    
	printf("%s4: Yellow\n", KYEL);
	printf("%s5: Orange\n", KYEL);
	printf("%s6: Green\n", KGRN);
	printf("%s", KNRM); //reset	 
	printf("7:Ignore spot (Robot won't be tracked)\n");
		
	while(!valid)
	{
		char c =std::cin.get();	
		valid = true;
		
		switch(c)
		{
			case '1':	
				 color =1;
				printf("%sYou Selected Red\n", KRED);
				 break;
			case '2':		
				 color =2;
				 printf("%sYou Selected Purple\n", KMAG);   
				break;
			case '3':			
				 color =3;
				 printf("%sYou Selected Blue\n", KBLU);  
				break;
			case '4':			
				 color =4;
				 printf("%sYou Selected Yellow\n", KYEL);
				break;
			case '5':			
				 color =5;
				 printf("%sYou Selected Orange\n", KYEL);
				break;
			case '6':			
				 color =6;
				 printf("%sYou Selected Green\n", KGRN);
				break;
			case '7':
				color=7;
				printf("You selected ignore. This spot will not be tracked\n");
				break;
			default:
				valid = false;
				//printf("Invalid input, try again\n");
				break;
		}
	}
	//printf("\n%i\n",color);
	printf("%s\n", KNRM); //reset	 
	return color;
}

void setColor(Robot * bot, int color, colorPresets presets)
{
	switch (color)
	{
		case 1:
			//printf("setting red color\n");
			bot->setHSVmin(Scalar(presets.red.Hmin, presets.red.Smin, presets.red.Vmin));
			bot->setHSVmax(Scalar(presets.red.Hmax, presets.red.Smax, presets.red.Vmax));
			break;
		case 2:
		    bot->setHSVmin(Scalar(presets.purple.Hmin, presets.purple.Smin, presets.purple.Vmin));
			bot->setHSVmax(Scalar(presets.purple.Hmax, presets.purple.Smax, presets.purple.Vmax));
			break;
		case 3:
			bot->setHSVmin(Scalar(presets.blue.Hmin, presets.blue.Smin, presets.blue.Vmin));
			bot->setHSVmax(Scalar(presets.blue.Hmax, presets.blue.Smax, presets.blue.Vmax));
			break;
		case 4:
			bot->setHSVmin(Scalar(presets.yellow.Hmin, presets.yellow.Smin, presets.yellow.Vmin));
			bot->setHSVmax(Scalar(presets.yellow.Hmax, presets.yellow.Smax, presets.yellow.Vmax));
			break;
		case 5:
			bot->setHSVmin(Scalar(presets.orange.Hmin, presets.orange.Smin, presets.orange.Vmin));
			bot->setHSVmax(Scalar(presets.orange.Hmax, presets.orange.Smax, presets.orange.Vmax));
			break;
		case 6:
			bot->setHSVmin(Scalar(presets.green.Hmin, presets.green.Smin, presets.green.Vmin));
			bot->setHSVmax(Scalar(presets.green.Hmax, presets.green.Smax, presets.green.Vmax));
			break;
		case 7:
			bot->setHSVmin(Scalar(0,0,0));
			bot->setHSVmax(Scalar(0,0,0));
			break;
		default:
			printf("invalid color in switch\n");
			break;
	}
}

void importColorPresets()
{
    const string red = "Red";
    const string purple = "Purple";
    const string blue = "Blue";
    const string yellow = "Yellow";
    const string orange = "Orange";
    const string green = "Green";
    int tempH, tempS, tempV;

    std::stringstream ss;
    std::ifstream in("/home/ecestudent/RobotSoccer/catkin_ws/presetColors.data");
    if (!in.good()) {
        printf("\n\n\nERROR, Couldn't open file\n\n\n");
        return;
    }

    // char line[MAX_CHARS];
    string ID;
    string line;

    // Parse File line by line
    while(getline(in, line)) {
        if (line.c_str()[0] == '#') {
            continue; //skip comments
        }

        ss.str(line);
        ss >> ID;

        if (ID == red) {
            ss >> presets.red.Hmin;
            ss >> presets.red.Smin;
            ss >> presets.red.Vmin;
            ss >> presets.red.Hmax;
            ss >> presets.red.Smax;;
            ss >> presets.red.Vmax;
        } else if (ID == purple) {
            ss >> presets.purple.Hmin;
            ss >> presets.purple.Smin;
            ss >> presets.purple.Vmin;
            ss >> presets.purple.Hmax;
            ss >> presets.purple.Smax;;
            ss >> presets.purple.Vmax;
        } else if (ID == blue) {
		    ss >> presets.blue.Hmin;
            ss >> presets.blue.Smin;
            ss >> presets.blue.Vmin;
            ss >> presets.blue.Hmax;
            ss >> presets.blue.Smax;;
            ss >> presets.blue.Vmax;
        } else if (ID == yellow) {
            ss >> presets.yellow.Hmin;
            ss >> presets.yellow.Smin;
            ss >> presets.yellow.Vmin;
            ss >> presets.yellow.Hmax;
            ss >> presets.yellow.Smax;;
            ss >> presets.yellow.Vmax;
        } else if (ID == orange) {
		    ss >> presets.orange.Hmin;
            ss >> presets.orange.Smin;
            ss >> presets.orange.Vmin;
            ss >> presets.orange.Hmax;
            ss >> presets.orange.Smax;;
            ss >> presets.orange.Vmax;
        } else if (ID == green) {
            ss >> presets.green.Hmin;
            ss >> presets.green.Smin;
            ss >> presets.green.Vmin;
            ss >> presets.green.Hmax;
            ss >> presets.green.Smax;
            ss >> presets.green.Vmax;
        }

        ss.clear();
    }

    in.close();
    printf("\nColor Presets sucessfully imported\n");
    int color;
	
	printf("Please assign each robot a preset color\n\n");	
	
	printf("Home 1:\n\n");  
	color = printColors();  
	setColor(&home1,color,presets);	
	home1.setColor(color);	

	printf("\nHome 2:\n\n");  
	color = printColors();  
	setColor(&home2,color,presets);
	home2.setColor(color);
	if(color == 7){
		ignoreHome2 =true;
	}
	
	printf("\nAway 1:\n\n");  
	color = printColors();  
	setColor(&away1,color,presets);
	away1.setColor(color);
	if(color == 7){
		ignoreAway1 =true;
	}

	printf("\nAway 2:\n\n");  
	color = printColors();  
	setColor(&away2,color,presets);
	away2.setColor(color);
	if(color == 7){
		ignoreAway2 =true;
	}
}

// Generates all the calibration prompts (field + ball + robots)
void runFullCalibration(VideoCapture capture) {
    restoreSettings();
    importColorPresets();
    printf("\nEnter values to calibrate field\n");
    calibrateField(capture);
    printf("\n\nEnter Ball HSV Values\n");
    ball.calibrateBall(capture);
    printf("\n\nEnter Home 1 HSV Values\n");
    home1.calibrateRobot(capture);
    printf("\n\nEnter Home 2 HSV Values\n");
    home2.calibrateRobot(capture);
    printf("\n\nEnter Away 1 HSV Values\n");
    away1.calibrateRobot(capture);
    printf("\n\nEnter Away 2 HSV Values\n");
    away2.calibrateRobot(capture);
    saveSettings();
    saveColorSettings();
}

// Special function for both getting the next image and reading the timestamp
// from the IP camera. Currently NOT very optimized for performance.
ros::Time getNextImage(std::ifstream & myFile, std::vector<char> & imageArray) {
  imageArray.clear();
  imageArray.reserve(1024*64);
  char buffer[4];
  ros::Time timestamp;
  bool foundImage = false;
  while(!foundImage){
    myFile.read(buffer,1);
    if((*buffer) == (char)0xFF){
      myFile.read(buffer,1);
      if((*buffer) == (char)0xD8){
        //printf("found start of image \n");
        imageArray.push_back((char)0xFF);
        imageArray.push_back((char)0xD8);
        while(1){
          myFile.read(buffer,1);
          imageArray.push_back(*buffer);
          if((*buffer) == (char)0xFF){
            myFile.read(buffer,1);
            imageArray.push_back(*buffer);
            if((*buffer) == (char)0xFE){
              myFile.read(buffer,4);
              imageArray.push_back(*buffer);
              imageArray.push_back(*(buffer+1));
              imageArray.push_back(*(buffer+2));
              imageArray.push_back(*(buffer+3));
              if((*(buffer+3)) == (char)0x01) {
                myFile.read(buffer,4);
                unsigned int sec = 0;
                for(int i = 0; i < 4; i++){
                  imageArray.push_back(*(buffer + i));
                  sec <<= 8;
                  sec += *(unsigned char*)(void*)(buffer+i);
                }

                myFile.read(buffer,1);
                unsigned int hundreds = 0;
                imageArray.push_back(*buffer);
                hundreds += *(unsigned char*)(void*)buffer;
                timestamp.sec = sec;
                timestamp.nsec = hundreds * 10000000;
              }


            }else if((*buffer) == (char)0xD9){
              //printf("found end of image\n");
              foundImage = true;
              break;
            }
          }
        }
      }
    }
  }
  return timestamp;
}

//This thread loads the streaming video into memory to be loaded into openCV
void * parserThread(void * notUsed){
    VideoCapture capture;
    capture.open(videoStreamAddress);
    FrameRaw frame;

    do {
        bool status = capture.read(frame.image);
	
        if(!status){
    	    printf("(Capture) Couldn't read next frame\n");
        }
        frame.timestamp=ros::Time::now();
        int value;
        sem_getvalue(&frameRawSema, &value);
        if (value < MIN_BUFFER_SIZE){
            frameRawFifo.push(frame);
            sem_post(&frameRawSema);
        }
    } while (1);

    return NULL;
}

//This thread converts JPEGs into Mats and undistorts them.
void * processorThread(void * notUsed){
    FrameRaw frameRaw;
    FrameMat frameMat;
    while(1) {
        sem_wait(&frameRawSema);
        frameRaw = frameRawFifo.front();
        frameRawFifo.pop();

        int value;
        sem_getvalue(&frameMatSema, &value);

        if (value < MIN_BUFFER_SIZE){
            frameMat.timestamp = frameRaw.timestamp;
	        frameMat.image = frameRaw.image;
      
            int field_origin_x;
            int field_origin_y;
      
            //convert frame from BGR to HSV colorspace
            field_origin_x = field_center_x - (field_width/2);
            field_origin_y = field_center_y - (field_height/2);
            Rect myROI(field_origin_x,field_origin_y,field_width, field_height);
            frameMat.image = frameMat.image(myROI);

            cvtColor(frameMat.image,frameMat.HSV,COLOR_BGR2HSV);

            frameMatFifo.push(frameMat);
            sem_post(&frameMatSema);
        }
    }

    return NULL;
}

// Track Ball
void * ballThread(void * notUsed) {
    Mat threshold;

    while(1) {
        sem_wait(&trackBallBegin);
        inRange(HSV,ball.getHSVmin(),ball.getHSVmax(),threshold);
        ball.trackFilteredBall(threshold,HSV,cameraFeed);
        //printf("ball tracked");
        sem_post(&trackBallEnd);
    }
}

// Track home1
void * home1Thread(void * notUsed) {
    Mat threshold;
    while(1) {
        sem_wait(&trackHome1Begin);
        inRange(HSV,home1.getHSVmin(),home1.getHSVmax(),threshold);
        home1.trackFilteredRobot(threshold,HSV,cameraFeed);
        //printf("home1 tracked");
        sem_post(&trackHome1End);
    }
}

// Track home 2
void * home2Thread(void * notUsed) {
    Mat threshold;
    while(1) {
        sem_wait(&trackHome2Begin);
        inRange(HSV,home2.getHSVmin(),home2.getHSVmax(),threshold);
        home2.trackFilteredRobot(threshold,HSV,cameraFeed);
        //printf("home2 tracked");
        sem_post(&trackHome2End);
    }
}

// Track away 1
void * away1Thread(void * notUsed) {
    Mat threshold;
    while(1) {
        sem_wait(&trackAway1Begin);
        inRange(HSV,away1.getHSVmin(),away1.getHSVmax(),threshold);
        away1.trackFilteredRobot(threshold,HSV,cameraFeed);
        //printf("Away1 tracked");
        sem_post(&trackAway1End);
    }
}

// Track away 2
void * away2Thread(void * notUsed) {
    Mat threshold;
    while(1) {
        sem_wait(&trackAway2Begin);
        inRange(HSV,away2.getHSVmin(),away2.getHSVmax(),threshold);
        away2.trackFilteredRobot(threshold,HSV,cameraFeed);
        //printf("Away2 tracked");
        sem_post(&trackAway2End);
    }
}

void setVideoStreamAddress()
{
	printf("\n\nWelcome to M2CK's vision code\n");
	printf("Checking if there are webcams online\n");
	VideoCapture capture;
	
	bool dotTenAlive=capture.open("http://192.168.1.10:8080/stream?topic=/image&dummy=param.mjpg");
	capture.release();
	
	bool dot48Alive=capture.open("http://192.168.1.48:8080/stream?topic=/image&dummy=param.mjpg");
	capture.release();

	bool dot78Alive=capture.open("http://192.168.1.78:8080/stream?topic=/image&dummy=param.mjpg");
	capture.release();
	
	bool dot79Alive=capture.open("http://192.168.1.79:8080/stream?topic=/image&dummy=param.mjpg");
	capture.release();

	printf("\nPlease choose which webcam feed to use:\n");
	printf("1: 192.168.1.10 - Default- WideScreen: %s\n",dotTenAlive ? "Alive" : "Dead");
	printf("2: 192.168.1.48 - FullScreen: %s\n",dot48Alive ? "Alive" : "Dead");
	printf("3: 192.168.1.78 - FullScreen: %s\n",dot78Alive ? "Alive" : "Dead");
	printf("4: 192.168.1.79 - FullScreen: %s\n",dot79Alive ? "Alive" : "Dead");

	char c =std::cin.get();
	
	if (c=='2')
	{
		videoStreamAddress ="http://192.168.1.48:8080/stream?topic=/image&dummy=param.mjpg";
		printf("Using Webcam 2\n");
	} else if (c=='3') {
	    videoStreamAddress ="http://192.168.1.78:8080/stream?topic=/image&dummy=param.mjpg";
		printf("Using Webcam 3\n");
	} else if (c=='4') {
	    videoStreamAddress ="http://192.168.1.79:8080/stream?topic=/image&dummy=param.mjpg";
		printf("Using Webcam 4\n");
	} else {
			printf("Using Webcam 1\n");
	}

	printf("Starting Vision Setup\n");
}

int main(int argc, char* argv[]) {
	//if we would like to calibrate our filter values, set to true.
	bool calibrationMode = true;

	//Matrix to store each frame of the webcam feed
	Mat threshold1;         // threshold image of ball
	Mat threshold2;         // threshold image of robot
	Mat threshold;          // combined image
	Mat bw;                 // black and white mat
    Mat BGR;                // BGR mat
  
	//video capture object to acquire webcam feed
	VideoCapture capture;
	
	setVideoStreamAddress();
	
	

    capture.open(videoStreamAddress); //set to 0 to use the webcam

	//set height and width of capture frame
	capture.set(CV_CAP_PROP_FRAME_WIDTH,FRAME_WIDTH);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT,FRAME_HEIGHT);

    if (calibrationMode == true) {
        // Calibrate the camera first
        runFullCalibration(capture);
    }

    printf("Starting Vision Threads + Communication\n");
    //namedWindow(windowName,WINDOW_NORMAL);

    /***********************Ros Publisher************************************/

    ros::init(argc, argv, "computer_vision");
    ros::NodeHandle n;
    ros::Publisher publisher = n.advertise<robotsoccer::locations>("locTopic", 1000);
    ros::Rate loop_rate(40);

    /************************************************************************/

  
    /************************************************************************/
	//start an infinite loop where webcam feed is copied to cameraFeed matrix
	//all of our operations will be performed within this loop
    capture.release();

    pthread_t parser;
    pthread_t processor;
    pthread_t ballT;
    pthread_t away1T;
    pthread_t away2T;
    pthread_t home1T;
    pthread_t home2T;

    sem_init(&frameRawSema,0,0);
    sem_init(&frameMatSema,0,0);

    sem_init(&trackBallBegin,0,0);

    sem_init(&trackHome1Begin,0,0);
    sem_init(&trackHome2Begin,0,0);

    sem_init(&trackAway1Begin,0,0);
    sem_init(&trackAway2Begin,0,0);

    sem_init(&trackBallEnd,0,0);

    sem_init(&trackHome1End,0,0);
    sem_init(&trackHome2End,0,0);

    sem_init(&trackAway1End,0,0);
    sem_init(&trackAway2End,0,0);

    pthread_create (&parser, NULL, parserThread, NULL);
    pthread_create (&processor, NULL, processorThread, NULL);
    pthread_create (&ballT, NULL, ballThread, NULL);
    pthread_create (&home1T, NULL, home1Thread, NULL);
    pthread_create (&home2T, NULL, home2Thread, NULL);
    pthread_create (&away1T, NULL, away1Thread, NULL);
    pthread_create (&away2T, NULL, away2Thread, NULL);
  
    unsigned int printCounter = 0;

    while(ros::ok()) {
        sem_wait(&frameMatSema);
        FrameMat frame = frameMatFifo.front();
        frameMatFifo.pop();

        //store image to matrix
        ros::Time timestamp = frame.timestamp;
        cameraFeed = frame.image;
        HSV = frame.HSV;

        sem_post(&trackBallBegin);
        sem_post(&trackHome1Begin);
        sem_post(&trackAway1Begin);
        sem_post(&trackHome2Begin);
        sem_post(&trackAway2Begin);

        sem_wait(&trackBallEnd);
        sem_wait(&trackHome1End);
        sem_wait(&trackAway1End);
        sem_wait(&trackHome2End);
        sem_wait(&trackAway2End);

        if (PERF_MODE == GUI) {
            // Show Field Outline
            Rect fieldOutline(0, 0, field_width, field_height);
            rectangle(cameraFeed,fieldOutline,Scalar(255,255,255), 1, 8 ,0);
      
            // Draw centering lines
            Point top_mid(field_width/2, 0);
            Point bot_mid(field_width/2, field_height);
            Point left_mid(0, field_height/2);
            Point right_mid(field_width, field_height/2);
            line(cameraFeed,top_mid, bot_mid, Scalar(200,200,200), 1, 8, 0);
            line(cameraFeed,left_mid, right_mid, Scalar(200,200,200), 1, 8, 0);
      
            //create window for trackbars
            imshow(windowName,cameraFeed);
        }
    
        /***********************Ros Publisher************************************/

        // Create message object
        robotsoccer::locations coordinates;
        // Fill message object with values

        coordinates.ball_x = convertPixelToMeter(ball.get_x_pos());
        coordinates.ball_y =  convertPixelToMeter(ball.get_y_pos());
	
	    //printf("ball: %f\n",convertPixelToMeter(ball.get_x_pos()));
	
        coordinates.home1_x = trigComp(convertPixelToMeter(home1.get_x_pos()));
        coordinates.home1_y = trigComp(convertPixelToMeter(home1.get_y_pos()));
        coordinates.home1_theta = convertDegreeToRadian(home1.getAngle());
	
	    //printf(" coordinates.home1_theta: %f\n", coordinates.home1_theta);
	    if(!ignoreHome2){
		    coordinates.home2_x = trigComp(convertPixelToMeter(home2.get_x_pos()));
		    coordinates.home2_y = trigComp(convertPixelToMeter(home2.get_y_pos()));
		    coordinates.home2_theta = convertDegreeToRadian(home2.getAngle());
	    } else {
		    coordinates.home2_x = 5;
		    coordinates.home2_y = 5;
		    coordinates.home2_theta = 0;
	    }

	    if(!ignoreAway1){
		    coordinates.away1_x = trigComp(convertPixelToMeter(away1.get_x_pos()));
		    coordinates.away1_y = trigComp(convertPixelToMeter(away1.get_y_pos()));
		    coordinates.away1_theta =convertDegreeToRadian(away1.getAngle());
	    } else {
		    coordinates.away1_x = 6;
		    coordinates.away1_y  = 6;
		    coordinates.away1_theta =6;
	    }
	
	    if(!ignoreAway2){
		    coordinates.away2_x = trigComp(convertPixelToMeter(away2.get_x_pos()));
		    coordinates.away2_y = trigComp(convertPixelToMeter(away2.get_y_pos()));
		    coordinates.away2_theta = convertDegreeToRadian(away2.getAngle());
	    } else {
		    coordinates.away2_x = 7;
		    coordinates.away2_y =7;
		    coordinates.away2_theta =7;
	    }
        coordinates.header.stamp = timestamp;

        // Print values to ROS console
        if (!(printCounter%PRINT_FREQ)) {
            ROS_INFO("\n  timestamp: %u.%09u\n  ", coordinates.header.stamp.sec, coordinates.header.stamp.nsec);
            ROS_INFO("\n  Ball_x: %f\n  Ball_y: %f\n", coordinates.ball_x, coordinates.ball_y);
            ROS_INFO("\n  home1_x: %f\n  home1_y: %f home1_theta: %f\n", coordinates.home1_x, coordinates.home1_y,  coordinates.home1_theta);
            ROS_INFO("\n  home2_x: %f\n  home2_y: %f home2_theta: %f\n", coordinates.home2_x, coordinates.home2_y,coordinates.home2_theta);
            ROS_INFO("\n  away1_x: %f\n  away1_y: %f away1_theta: %f\n", coordinates.away1_x, coordinates.away1_y,coordinates.away1_theta);
            ROS_INFO("\n  away2_x: %f\n  away2_y: %f away2_theta: %f\n", coordinates.away2_x, coordinates.away2_y, coordinates.away2_theta);
        }

        // Publish message
        publisher.publish(coordinates);
        // Waits the necessary time between message publications to meet the
        // specified frequency set above (ros::Rate loop_rate(10);)
        loop_rate.sleep();

        /************************************************************************/

		//image will not appear without this waitKey() command
		// Wait to check if user wants to switch Home/Away
        char pressedKey;
        pressedKey = cvWaitKey(1); // Wait for user to press 'Enter'
        if (pressedKey == 'a') {
            TEAM = AWAY;
        } else if (pressedKey == 'h') {
            TEAM = HOME;
        }

	printCounter++;

	}

	return 0;
}
