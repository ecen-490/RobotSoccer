import numpy as np 
#from kalmanFilter import b,Wall,RH1,RA1
from kalmanFilter import Wall

# general radius of robots,m
_robot_radius = 0.2

_dampingCoeff = 0.9   # Percentage of retained when the ball hits a robot

_ball_robot_collisionAngle = 20*np.pi/180  # Angle between velocity vectors that will make the ball inherit the robots velocity

_robot_robot_collsisionAngle = 45*np.pi/180 # Angle between velocity vectors that will make the robots stop

def getPredictedStates(ball,RH1,RA1,t):
    # ball is the ball kalman Filter object
    # RH1 is the robot home 1 kalman filter object
    # RA1 is the robot away 1 kalman filter object
    # Wall is the wall object
    # t is the amount of time to predict into the future, s

    count = int(np.ceil(t/ball.PTs)) # the number of iterations needed inorder to predict into the future by 't' seconds

    ball.flag_set_prediction_states = True
    RH1.flag_set_prediction_states = True
    RA1.flag_set_prediction_states = True



    for i in range(0,count):
        # Push states forward
        ball.predict_states()   
        RH1.predict_states()
        RA1.predict_states()
        #print("RH1 phat", RH1.phat)

        # Dectect collisions
        robotWallBounce(RH1,Wall)
        robotWallBounce(RA1,Wall)
        ballWallBounce(ball,Wall)
        detectCollision(ball,RH1,RA1)

def getPredictedBall(ball,RH1,RA1,t):
    # ball is the ball kalman Filter object
    # RH1 is the robot home 1 kalman filter object
    # RA1 is the robot away 1 kalman filter object
    # Wall is the wall object
    # t is the amount of time to predict into the future, s

    count = int(np.ceil(t/ball.PTs)) # the number of iterations needed inorder to predict into the future by 't' seconds

    ball.flag_set_prediction_states = True
    RH1.flag_set_prediction_states = True
    RA1.flag_set_prediction_states = True




    for i in range(0,count):
        # Push states forward
        ball.predict_states()

        #print("RH1 phat", RH1.phat)

        # Dectect collisions
        ballWallBounce(ball,Wall)
        detectCollision(ball,RH1,RA1)





def detectCollision(b,RH1,RA1):
    # This function is used to detect if the ball has collided with a robot. or a robot has collided 
    # with another robot. Used for prediction
    b_x = b.phat[0,0]  # X position of ball prediction
    b_y = b.phat[1,0]  # Y position of ball prediction
    RH1_x = RH1.phat[0,0] # X position of robot home prediction
    RH1_y = RH1.phat[1,0] # Y position of robot home prediction
    RA1_x = RA1.phat[0,0] # X position of robot away prediction
    RA1_y = RA1.phat[1,0] # Y position of robot away prediction

    dist_b_RH1 = np.sqrt( (b_x-RH1_x)**2 + (b_y - RH1_y)**2)          # distance between ball and RH1, m
    dist_b_RA1 = np.sqrt( (b_x-RA1_x)**2 + (b_y - RA1_y)**2)          # distance between ball and RA1, m
    dist_RH1_RA1 = np.sqrt( (RH1_x - RA1_x)**2 + (RH1_y - RA1_y)**2)  # distance between RH1 and RA1, m

    if dist_b_RH1 < _robot_radius: # ball collided with RH1
        ball_robot_collision(b,RH1)
    elif dist_b_RA1 < _robot_radius: # ball collided with RA1
        ball_robot_collision(b,RA1)
    elif dist_RH1_RA1 < 2*_robot_radius: # the robots collided with eachother
        robot_robot_collision(RH1,RA1)



def robot_robot_collision(R1,R2):
    # If the robots collide face to face (within 2*_robot_robot_collsisionAngle) then the robots will stop moving
    # Else only the faster moving robot will stop


    R1_Vx = R1.phat[3,0]   # X velocity of robot 1
    R1_Vy = R1.phat[4,0]   # Y velocity of robot 1
    R2_Vx = R2.phat[3,0]   # X velocity of robot 2
    R2_Vy = R2.phat[4,0]   # Y velocity of robot 2
    R1_V_mag = np.sqrt(R1_Vx**2 + R1_Vy**2) +.1e-3  # magnitude of the robot1's velocity vector
    R2_V_mag = np.sqrt(R2_Vx**2 + R2_Vy**2) +.1e-3# magnitude of the robot1's velocity vector
    psi = np.arccos((R1_Vx*(-R2_Vx)+R1_Vy*(-R2_Vy))/(R1_V_mag*R2_V_mag)) # angle between robot1's velocity vecotor and the negative
                                                                         # of the robot2's velociy vector. If this angle is less than the 
                                                                         # _robot_robot_collsisionAngle, then the robots will stop moving.



    if abs(psi) < _robot_robot_collsisionAngle: # Stop the robots
        # Move robots back before collision                                                                  
        R1.phat[0,0] = R1.phat[0,0] - R1.phat[3,0]*R1.PTs
        R1.phat[1,0] = R1.phat[1,0] - R1.phat[4,0]*R1.PTs
        R2.phat[0,0] = R2.phat[0,0] - R2.phat[3,0]*R2.PTs
        R2.phat[1,0] = R2.phat[1,0] - R2.phat[4,0]*R2.PTs 
        R1.phat[3,0] = 0
        R1.phat[4,0] = 0
        R2.phat[3,0] = 0
        R2.phat[4,0] = 0
    elif R1_V_mag > R2_V_mag: # Stop robot 1
        # Move robot back before collision                                                                   
        R1.phat[0,0] = R1.phat[0,0] - R1.phat[3,0]*R1.PTs
        R1.phat[1,0] = R1.phat[1,0] - R1.phat[4,0]*R1.PTs
        # set velocity to 0
        R1.phat[3,0] = 0
        R1.phat[4,0] = 0
    else:                     # Stop robot 2
        # Move robot back before collision
        R2.phat[0,0] = R2.phat[0,0] - R2.phat[3,0]*R2.PTs
        R2.phat[1,0] = R2.phat[1,0] - R2.phat[4,0]*R2.PTs 
        # set velocity to 0
        R2.phat[3,0] = 0
        R2.phat[4,0] = 0

def robotWallBounce(robot,Wall):
    # This function is used for future prediction
    # Assumes that a collision with the ball is perfectly elastic
    # robot is the robot bouncing off of the wall
    # Wall is the class Wall that states the max X and Y position
    if abs(robot.phat[0,0]) >= Wall.Xmax:  # robot has hit the wall's x-boundary
        # Position and velocity correction
        robot.phat[0,0] = robot.phat[0,0] - robot.phat[3,0]*robot.PTs
        robot.phat[3,0] = -robot.phat[3,0]
        robot.phat[0,0] = robot.phat[0,0] + robot.phat[3,0]*robot.PTs
    elif abs(robot.phat[1,0]) >= Wall.Ymax: # robot has hit the wall's y-boundary
        # Position and velocity correction
        robot.phat[1,0] = robot.phat[1,0] - robot.phat[4,0]*robot.PTs
        robot.phat[4,0] = -robot.phat[4,0]
        robot.phat[1,0] = robot.phat[1,0] + robot.phat[4,0]*robot.PTs
    # else:

        # No correction is needed. Do nothing

def ballWallBounce(ball,Wall):
    # Assumes that it is perfectly elastic collision
    # ball is the ball bouncing off of the wall
    # Wall is the class Wall that states the max X and Y position
    if abs(ball.phat[0,0]) >= Wall.Xmax:  # ball has hit the wall's x-boundary
        # Position and velocity correction
        ball.phat[0,0] = ball.phat[0,0] - ball.phat[2,0]*ball.PTs
        ball.phat[2,0] = -ball.phat[2,0]
        ball.phat[0,0] = ball.phat[0,0] + ball.phat[2,0]*ball.PTs
    elif abs(ball.phat[1,0]) >= Wall.Ymax: # ball has hit the wall's y-boundary
        # Position and velocity correction
        ball.phat[1,0] = ball.phat[1,0] - ball.phat[3,0]*ball.PTs
        ball.phat[3,0] = -ball.phat[3,0]
        ball.phat[1,0] = ball.phat[1,0] + ball.phat[3,0]*ball.PTs
    # else:

        # No correction is needed. Do nothing


_Velocity_threshold = 0.01      # At this magnitude velocity or less, assume the ball isnt moving
def ball_robot_collision(b,R):
    Cx = b.phat[0,0]       # Cx is the collision x-coordinate
    Cy = b.phat[1,0]       # Cy is the collision y-coordinate
    b_Vx = b.phat[2,0]                   # Ball velocity in the x direction
    b_Vy = b.phat[3,0]                   # Ball velocity in the y direction
    Rx = R.phat[0,0]       # Rx is the robot the ball is going to collide with x direction
    Ry = R.phat[1,0]       # Ry is the robot the ball is going to collide with y direction
    R_Vx = R.phat[3,0]     # Robot's x velocity
    R_Vy = R.phat[4,0]     # Robot's y velocity

    b_V_mag = np.sqrt(b_Vx**2 + b_Vy**2) +.1e-3 # magnitude of the ball's velocity vector
    R_V_mag = np.sqrt(R_Vx**2 + R_Vy**2) +.1e-3 # magnitude of the robot's velocity vector
    psi = np.arccos((b_Vx*(-R_Vx)+b_Vy*(-R_Vy))/(b_V_mag*R_V_mag)) # angle between the ball's velocity vecotor and the negative
                                                                   # of the robots velociy vector. If this angle is less than the 
                                                                   # _ball_robot_collisionAngle, then the ball will inherit the robots velocity.



    V = np.matrix([[b_Vx],[b_Vy]])            

    theta = np.arctan2(Cy-Ry,Cx-Rx)       # Angle between object's center and colision point

    # Determines the rotation angle to change frames, and changes frame
    if theta > 0: 
        theta = np.pi - theta
        R = RHrotate(theta)
        Bounce_V = R*V
    else:
        theta = np.pi + theta
        R = RHrotate(theta).transpose()
        Bounce_V = R*V

    Bounce_V[0,0] = -Bounce_V[0,0]   # Account for bouncing
   
    V = R.transpose()*Bounce_V       # Rotate frame back

    # print('theta', theta*180/np.pi) 
    # print('Vx',V[0,0])
    # print('Vy',V[1,0])

    # Corrects the position
    b.phat[0,0] = b.phat[0,0] - b.PTs*b.phat[2,0]
    b.phat[1,0] = b.phat[1,0] - b.PTs*b.phat[3,0]

    

    if(abs(psi) < _ball_robot_collisionAngle and R_V_mag > b_V_mag): # the ball inherits the robot's velocity
        b.phat[2,0] = R_Vx
        b.phat[3,0] = R_Vy
    else:                      
        # Store the corrected velocities
        b.phat[2,0] = _dampingCoeff*V[0,0]
        b.phat[3,0] = _dampingCoeff*V[1,0]

    b.phat[0,0] = b.phat[0,0] + b.PTs*b.phat[2,0]
    b.phat[1,0] = b.phat[1,0] + b.PTs*b.phat[3,0]

    

        

        



def RHrotate(theta):
    return np.matrix([[np.cos(theta),  -np.sin(theta)], 
                      [np.sin(theta),  np.cos(theta)]])



if __name__ == "__main__": 

    def predictionTest(b,RH1,RA1,Wall):
        b.xhat[0,0] = 1     # x position
        b.xhat[1,0] = 1     # y position
        b.xhat[2,0] = 1   # x velocity
        b.xhat[3,0] = 1   # y veloctiy

        RH1.xhat[0,0] = -0.75  # x position
        RH1.xhat[1,0] = 0 # y position
        RH1.v_command[0,0] = 0.5 # x velocity
        RH1.v_command[1,0] = 0   # y velocity

        RA1.xhat[0,0] = 0.75     # x position
        RA1.xhat[1,0] = 0   # y position
        RA1.xhat[3,0] = -0.5  # x velocity
        RA1.xhat[4,0] = 0     # y velocity

        getPredictedStates(b,RH1,RA1,Wall,1)


    predictionTest(b,RH1,RA1,Wall)
    print('ball',b.phat[0:4,0])
    print('RH1', RH1.phat[0:2,0],RH1.phat[3:5,0])
    print('RA1', RA1.phat[0:2,0],RA1.phat[3:5,0])
