Odroid Board:

|<- width->|

- - - - - -  
|.........|
|.........|
|.........|  Length
|.........|
|.........|
|.........|
|.........|
-----------

Main Board Width: 1 7/8”
Main Board Length: 3 1/4”
Main Board Thickness: 1/32”

1. Length from bottom edge to ethernet port: 1/4”
2. Length from top edge to heatsink: 3/8”

3. Maximum Height above board (USB ports are tallest item on top of board): 5/8”
4. Maximum height below board (headphone jack): 5/32”

2          1
 |||||    ||  < 3
-------------
          ||  < 4

