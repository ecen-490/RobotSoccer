import numpy as np
from Observer1 import*
import time
import roboclaw_motion as rm
import roboclaw2 as RC


RC.calibrateRoboclaws()


def updateRobotMovement():
    x.updateObserver()
    y.updateObserver()
    theta.updateObserver()             

    rm.moveBodyFrame(x.xhat[1,0],y.xhat[1,0],theta.xhat[1,0],theta.xhat[0,0])
   # print('xhat', x.xhat[1,0])
  #  print('yhat', y.xhat[1,0])
  #  print('thetahat', theta.xhat[1,0])

    #print('x pos', x.xhat[0,0])
    #print('y pos', y.xhat[0,0])
    #print('theta pos', theta.xhat[0,0])
    #rm.moveBodyFrameVA(Vx,Vy,omega,x.acceleration,y.acceleration,theta.acceleration,theta.xhat[0,0])

def map_theta(theta_c):
    while(theta_c > 2*np.pi):
        theta_c = theta_c - 2*np.pi
    while(theta_c < 0):
        theta_c = theta_c + 2*np.pi
    return theta_c


def updateCurrentPosition(x_c,y_c,theta_c):
    x.xhat[0,0] = float(x_c)
    y.xhat[0,0] = float(y_c)
    theta.xhat[0,0] = float(theta_c)

def updateDesiredPosition(x_d,y_d,theta_d):
    x.d = x_d
    y.d = y_d
    theta.d = theta_d


def getError(x,y,theta):
    x_error = abs(abs(x.xhat[0,0] - x.d))
    y_error = abs(abs(y.xhat[0,0]-y.d))
    theta_error = abs(abs(theta.xhat[0,0] - theta.d))
    return x_error, y_error, theta_error


if __name__ == "__main__": 
    # Used for testing
    updateCurrentPosition(0,0,0)
    updateDesiredPosition(1,0,0)
    count = 0
    while count < 200:
         updateRobotMovement()
         time.sleep(0.01)
         count = count + 1
    rm.moveBodyFrame(0,0,0,0)
