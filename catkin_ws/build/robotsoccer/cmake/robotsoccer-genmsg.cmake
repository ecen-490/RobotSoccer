# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "robotsoccer: 7 messages, 2 services")

set(MSG_I_FLAGS "-Irobotsoccer:/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg;-Istd_msgs:/opt/ros/indigo/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(robotsoccer_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv" ""
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv" ""
)

get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg" NAME_WE)
add_custom_target(_robotsoccer_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "robotsoccer" "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg" "std_msgs/Header"
)

#
#  langs = gencpp;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_msg_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)

### Generating Services
_generate_srv_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)
_generate_srv_cpp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
)

### Generating Module File
_generate_module_cpp(robotsoccer
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(robotsoccer_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(robotsoccer_generate_messages robotsoccer_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_cpp _robotsoccer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(robotsoccer_gencpp)
add_dependencies(robotsoccer_gencpp robotsoccer_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS robotsoccer_generate_messages_cpp)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_msg_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)

### Generating Services
_generate_srv_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)
_generate_srv_lisp(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
)

### Generating Module File
_generate_module_lisp(robotsoccer
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(robotsoccer_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(robotsoccer_generate_messages robotsoccer_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_lisp _robotsoccer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(robotsoccer_genlisp)
add_dependencies(robotsoccer_genlisp robotsoccer_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS robotsoccer_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_msg_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/indigo/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)

### Generating Services
_generate_srv_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)
_generate_srv_py(robotsoccer
  "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
)

### Generating Module File
_generate_module_py(robotsoccer
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(robotsoccer_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(robotsoccer_generate_messages robotsoccer_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/currentlocations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot2commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1commands.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/predictedLocations.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/locations.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/robot1vcommand.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/kickerplate_robot1command.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/srv/currentlocationsservice.srv" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg/commandcenter.msg" NAME_WE)
add_dependencies(robotsoccer_generate_messages_py _robotsoccer_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(robotsoccer_genpy)
add_dependencies(robotsoccer_genpy robotsoccer_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS robotsoccer_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/robotsoccer
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(robotsoccer_generate_messages_cpp std_msgs_generate_messages_cpp)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/robotsoccer
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(robotsoccer_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/robotsoccer
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(robotsoccer_generate_messages_py std_msgs_generate_messages_py)
