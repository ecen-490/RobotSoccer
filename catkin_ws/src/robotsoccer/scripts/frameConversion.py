
from numpy import matrix
from numpy import linalg
import math

#define s1,s2,s3

realWorldOffset = 1 #1.698
s = 2.9e-2#.0282977488817 #radius of wheel, m
r = 7.3e-2#6.95e-2#7.85e-2#.08 #radius from center to center of wheel, m


# Wheel 1 corresponds to RC2 M1
# Wheel 2 corresponds to RC1 M1
# Wheel 3 corresponds to RC1 M2

# theta at 0 degrees corresponds to the positive x direction

r1theta = -math.pi/3.0
r1x = math.cos(r1theta)*r
r1y = math.sin(r1theta)*r
r2theta = math.pi/3.0
r2x = math.cos(r2theta)*r
r2y = math.sin(r2theta)*r
r3theta = math.pi
r3x = math.cos(r3theta)*r
r3y = math.sin(r3theta)*r

#print r1x
#print r1y
#print r2x
#print r2y
#print r3x
#print r3y

s1theta = math.pi/2+r1theta
#s1theta = r1theta - math.pi/2
s1x = -math.cos(s1theta)
s1y = -math.sin(s1theta)

s2theta = math.pi/2+r2theta
#s2theta = r2theta - math.pi/2
s2x = -math.cos(s2theta)
s2y = -math.sin(s2theta)

s3theta = -math.pi/2
#s3theta = r3theta - math.pi/2
s3x = 0#math.cos(s3theta)
s3y = -math.sin(s3theta)
#print s1x
#print s1y
#print s2x
#print s2y
#print s3x
#print s3y
mSub = matrix( [[s1x,s1y,(s1y*r1x - s1x*r1y)],
                [s2x,s2y,(s2y*r2x - s2x*r2y)],
                [s3x,s3y,(s3y*r3x - s3x*r3y)]] )
                
#print mSub

M = realWorldOffset*(1.0/s)*mSub

R = lambda theta: matrix( [[math.cos(theta),math.sin(theta),0.0],
                   [-math.sin(theta),math.cos(theta),0.0],
                                                   [0.0,0.0,1.0]] )


#print M
# theta is the angle between the inertial frame and the body frame

# Converts the velocities in the inertial frame to the wheel speeds in 
# the body frame
def InertialVelocitiestoWheelSpeeds(Vx,Vy,omega,theta):
  velocityMatrix = matrix([[Vx],
                                 [Vy],
                                 [omega]])
  WheelSpeeds = M*R(theta)*velocityMatrix
  #print WheelSpeeds
  # returns omega1, omega2, omega3
  return WheelSpeeds.getA()[0][0], WheelSpeeds.getA()[1][0], WheelSpeeds.getA()[2][0]

# Converts the wheel speeds in the body frame to the velocities
# in the body frame
def WheelSpeedstoInertialVelocities(omega1,omega2,omega3,theta):
  WheelSpeeds = matrix([[omega1],
            [omega2],
            [omega3]])
  velocity = R(theta).transpose()*linalg.inv(M)*WheelSpeeds
  # returns Vx, Vy, omega
  return velocity.getA()[0][0], velocity.getA()[1][0], velocity.getA()[2][0]


  



def radianToQpps(radian):
  result = int(radian * 19820.0 / (2*math.pi))
  #print result 
  if result > 308420:
    return 308420
  elif result < -308420:
    return -308420
  else:
    return result


