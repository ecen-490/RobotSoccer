import serial
import struct
import time
import roboclaw2 as RC

def stopRobot():
	RC.M1Forward(RC.RoboClaw1,0)
	RC.M2Forward(RC.RoboClaw1,0)
	RC.M1Forward(RC.RoboClaw2,0)

stopRobot()
