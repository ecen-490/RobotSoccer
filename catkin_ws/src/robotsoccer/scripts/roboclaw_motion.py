#import serial
#import struct
import time
import math

import sys
import numpy as np
import roboclaw2 as RC
import frameConversion as FC

offset = 1.1
conversion1 = 19820.0*offset
conversion2 = 19829.0*offset
limit = 308420*2
def radianToQpps(radian):
    result = int(radian * conversion2 / (2.0*math.pi))
    if result > limit:
        return limit
    elif result < -limit:
        return -limit
    else:
        return result

def qppsToRadian(qpps):
    result = qpps / conversion2 * (2.0*math.pi)
    return result

# reads the motor's encoders and returns the velocities in the inertial frame.
def getVelocity(theta):
    #print("GetVelocity, readM1speed ",RC.readM1speed(RC.RoboClaw2))
    qpps_M1 = RC.readM1speed(RC.RoboClaw2)
    qpps_M2 = RC.readM1speed(RC.RoboClaw1)
    qpps_M3 = RC.readM2speed(RC.RoboClaw1)
    # LOOKME - readM1/2speed returns a tuple with 3 items, item[1] appears to be the speed, [2] appears to be the direction and [0] is always 0 -CB
    omega1 = qppsToRadian(qpps_M1[1])
    omega2 = qppsToRadian(qpps_M2[1])
    omega3 = qppsToRadian(qpps_M3[1])
    Vx,Vy,omega = FC.WheelSpeedstoInertialVelocities(omega1,omega2,omega3,theta)
    #print "Velocities\n"
    #print Vx
    #print Vy
    #print omega
    return Vx, Vy, omega

def stopRobot():
    RC.M1Forward(RC.RoboClaw1,0)
    RC.M2Forward(RC.RoboClaw1,0)
    RC.M1Forward(RC.RoboClaw2,0)

def SpinInCircle(percentOfMax):
    MaxSpeed = 127.0
    temp = MaxSpeed*percentOfMax
    desiredSpeed = temp
    if(desiredSpeed > MaxSpeed):
        desiredSpeed = desiredSpeed/MaxSpeed
    desiredSpeed = int(desiredSpeed)
    RC.M1Forward(RC.RoboClaw1,desiredSpeed)
    RC.M2Forward(RC.RoboClaw1,desiredSpeed)
    RC.M1Forward(RC.RoboClaw2,desiredSpeed)
    time.sleep(2)

# Control the velocity only
def moveBodyFrame(Vx,Vy,omega,theta):
    #print("roboclaw_motion: moveBodyFrame, next up is the intertialvelocitiestowheelspeeds")
    #print FC.InertialVelocitiestoWheelSpeeds(Vx,Vy,omega,theta)
    omega1,omega2,omega3 = FC.InertialVelocitiestoWheelSpeeds(Vx,Vy,omega,theta)
    qqps_M1 = radianToQpps(omega1)
    qqps_M2 = radianToQpps(omega2)
    qqps_M3 = radianToQpps(omega3)
    #print "pulses"
    #print qqps_M1
    #print qqps_M2
    #print qqps_M3
    #print("roboclaw_motion: Time to move! Setting speeds to", qqps_M1, qqps_M2, qqps_M3)
    RC.SetM1Speed(RC.RoboClaw2,qqps_M1)
    RC.SetM1Speed(RC.RoboClaw1,qqps_M2)
    RC.SetM2Speed(RC.RoboClaw1,qqps_M3)

# Control the velocity and the acceleration
def moveBodyFrameVA(Vx,Vy,omega,Ax,Ay,Aomega,theta):
    #print Vx
    #print Vy
    #print omega
    #print Ax
    #print Ay
    #print Aomega
    #print theta
    omega1,omega2,omega3 = FC.InertialVelocitiestoWheelSpeeds(Vx,Vy,omega,theta)
    Aomega1, Aomega2, Aomega3 = FC.InertialVelocitiestoWheelSpeeds(Ax,Ay,Aomega,theta)
    Vqqps_M1 = radianToQpps(omega1)
    Vqqps_M2 = radianToQpps(omega2)
    Vqqps_M3 = radianToQpps(omega3)
    Aqqps_M1 = np.abs(radianToQpps(Aomega1))
    Aqqps_M2 = np.abs(radianToQpps(Aomega2))
    Aqqps_M3 = np.abs(radianToQpps(Aomega3))
    #print "pulses"
    #print qqps_M1
    #print qqps_M2
    #print qqps_M3
    print( Aqqps_M1)
    print( Aqqps_M2)
    print(Aqqps_M3)
    RC.SetM1SpeedAccel(RC.RoboClaw2,Aqqps_M1,Vqqps_M1)
    RC.SetM1SpeedAccel(RC.RoboClaw1,Aqqps_M2,Vqqps_M2)
    RC.SetM2SpeedAccel(RC.RoboClaw1,Aqqps_M3,Vqqps_M3)

def moveRobotInSquare():
    Vx = 0.2
    Vy = 0.2
    t = 2
    moveBodyFrame(Vx,0,0,0)
    time.sleep(t)
    printSpeeds()
    getVelocity(0)
    time.sleep(t)
    stopRobot()
    time.sleep(t)
    moveBodyFrame(0,Vy,0,0)
    time.sleep(t)
    printSpeeds()
    time.sleep(t)
    stopRobot()
    time.sleep(t)
    moveBodyFrame(-Vx,0,0,0)
    time.sleep(t)
    printSpeeds()
    time.sleep(t)
    stopRobot()
    time.sleep(t)
    moveBodyFrame(0,-Vy,0,0)
    time.sleep(t)
    printSpeeds()
    time.sleep(t)
    stopRobot()
    time.sleep(t)

def printSpeeds():
    print("Speeds")
    print(RC.readM1speed(RC.RoboClaw2))
    print(RC.readM1speed(RC.RoboClaw1))
    print(RC.readM2speed(RC.RoboClaw1))



if __name__ == "__main__":
    print("roboclaw_motion: I'm main!")
    RC.calibrateRoboclaws()

#    moveBodyFrame(1,0,0,0)
#    time.sleep(3)
    # hardcode the pidq values
#    p = int(65536 * 4) #262144
#    i = int(65536 * 2) #131072
#    d = int(65536 * 6)  #65536
#    q = 100000

#    print("Setting pidq to",p,i,d)
#    RC.SetM1pidq(RC.RoboClaw2,p,i,d,q)
#    RC.SetM1pidq(RC.RoboClaw1,p,i,d,q)
#    RC.SetM2pidq(RC.RoboClaw1,p,i,d,q)

    # FIXME we need to do SetM1pi
#dq. Get the correct p i d q values somehow -CB
    moveBodyFrame(0.5,0.5,0,0)
#    moveRobotInSquare()
    time.sleep(1)
#    print("roboclaw_motion: I'm tired. Going to sleep!")
    stopRobot()
