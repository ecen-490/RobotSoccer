import numpy as np
import parameters as P

_scalar1 = 1.2
_scalar2 = 2
_dminRR = P.RobotRadius + P.RobotRadius*_scalar1   # Minimum distance between two robots
_dminRB = P.RobotRadius*_scalar1                   # Minimum distance between robot and ball

class Strategy():

    def __init__():

        RH1 = np.zeros([6,1]) # Robot Home1's position and velocity (x, y, theta, vx, vy, vtheta)
        #RH2 = np.zeros([6,1]) # Robot Home2's position and velocity (x, y, theta, vx, vy, vtheta)
        RA1 = np.zeros([6,1]) # Robot Away1's position and velocity (x, y, theta, vx, vy, vtheta)
        RA2 = np.zeros([6,1]) # Robot Away2's position and velocity (x, y, theta, vx, vy, vtheta)
        Ball = np.zeros([4,1]) # Ball's position and velocity (x, y, vx, vy)

    def updateCurrentPositions(RH1,RH2,RA1,RA2,Ball):
        RH1 = RH1
        #RH2 = RH2
        RA1 = RA1
        RA2 = RA2
        Ball = Ball

    # returns the normalized vector from object1 to object 2, and the norm
    def getVector(object1,object2):
        v = object2[0:2,0] - object1[0:2,0]  # position vector
        vn = v/np.norm(v) 
        return vn,np.norm(v)   

    # Returns the angle between the two objects from object 1's perspective
    def getTheta(object1_pos,object2_pos):
        v = object2_pos[0:2,0] - object1_pos[0:2,0]  # position vector
        vn = v/np.norm(v)                            # normalize position vector
        theta = np.arctan2(vn[1,0],vn[0,0])
        return theta

    # Returns the position of the away goal and the angle between the ball and away goal.
    # This function should be called only when the robot is in line with the ball and away goal.
    def getGo2GA_F_B2GA():
        theta = getTheta(Ball,P.goalAway)
        x = P.goalAway[0,0]
        y = P.goalAway[1,0]
        return x,y,theta

    def circleObject(obj):
        d = 0.5
        vn1, vd1 = getVector(obj,RH1)
        clockwise = 1
        if clockwise :
            vn2 = np.matrix([[-vn1[1,0]],[vn1[0,0]])
        else:
            vn2 = np.matrix([[vn1[1,0]],[-vn1[0,0]])

        dp = RH + vn2*d
        x = dp[0,0]
        y = dp[1,0]
        return x,y

        


    # Moves the robot behind the ball. The parameter play determines how the robot will face.
    # play = 'offense' - the ball will face the direction from ball to away goal
    # play - 'defense' - the ball will face the dircetion from home goal to the ball
    def getBehindBall(play):

        if play == 'offense':
            theta_d = getTheta(Ball,P.goalAway)
        else:
            theta_d = getTheta(P.home_goal,Ball)

        Ball_phat = 

        # Distance between home goal and ball
        d_HgB = np.norm(Ball[0:2,0] - P.goalHome)

        # Distance between home goal and home robot 1
        d_HgR = np.norm(RH[0:2,0] - P.goalHome)

        if d_HgB < d_HgR - P.RobotRadius:  # The ball is closer to home goal than the robot
                                           # The robot will move to the left or right of the ball.

            if B[1,0] + P.RobotRadius*_scalar2 > P.max_y/2: # The robot cannot move to the left
                y = B[1,0] - P.RobotRadius*_scalar2
            elif B[1,0] - P.RobotRadius*_scalar2 < -P.max_y/2:  # The robot cannot move to the right
                y = B[1,0] + P.RobotRadius*_scalar2
            else: # The robot can move in either direction
                if RH1[1,0] - B[1,0] > 0: # The robot is on the left side of the ball
                    y = B[1,0] + P.RobotRadius*_scalar2
                else:
                    y = B[1,0] - P.RobotRadius*_scalar2

            x = B[0,0] - P.RobotRadius*_scalar2


        elif d_HgB > d_HgR + P.RobotRadius: # The rball is further away from the home goal than the robot
            r = d_HgB - P.RobotRadius
            r = P._Minimum_G2Rd if r <= P._Minimum_G2Rd else r

            theta = getTheta(Ball,P.home_goal)

                    # Calculate the x,y position to move to
            y = r*math.sin(theta)
            x = r*math.cos(theta)

            y = self.home_goal[1, 0]+y
            x = self.home_goal[0, 0]+x

        else:                # The ball is parallel to the robot
            if d_HgB < 2*P.RobotRadius + P._Minimum_G2Rd: # The ball is too close to the goal for the robot to get behind.
                                                          # The robot will attack from the side

                v_R_2_B,d = getVector(R,B)
                theta_d = np.arctan2(v_R_2_B[1,0], v_R_2_B[0,0]) 
                theta_d = theta_d + np.sign(theta_d)*(-30*np.pi/180)
                x = B[0,0]
                y = B[1,0] + P.RobotRadius*_sclar1*np.sign(theta_d)*(-1)

                # Checks to see if the robot is in position to move the ball
                if np.norm(B2Rv) < P.RobotRadius*1.4 and abs((self.robot1.position([2,0]) - theta_d)) < 10*np.pi/180 :
                    x = B[0,0] + 1*np.sin(theta_d)
                    y = B[1,0] + 1*np.cos(theta_d)


            else:
                r = d_HgB - P.RobotRadius
                r = P._Minimum_G2Rd if r <= P._Minimum_G2Rd else r
                theta = getTheta(Ball,P.home_goal)

                        # Calculate the x,y position to move to
                y = r*math.sin(theta)
                x = r*math.cos(theta)

                y = self.home_goal[1, 0]+y
                x = self.home_goal[0, 0]+x

    return x,y,theta_d

    # avoids collisions with robots
    def avoid_R_2_R_Collision(RH,x,y):
        # x - the desired x position
        # y - the desired y position
        # RH - the robot home that is moving position

        DP = np.matrix([[x],[y]]) # Desired point

        #v_RH_2_RH1 = RH1[0:1,0] - RH[0:1,0]  # Vector from robot home to robot home 1
        #v_RH_2_RH2 = RH2[0:1,0] - RH[0:1,0]  # Vector from robot home to robot home 2
        v_RH_2_RA1 = RA1[0:1,0] - RH[0:1,0]   # Vector from robot home to robot away 1
        v_RH_2_RA2 = RA2[0:1,0] - RH[0:1,0]   # Vector from robot home to robot away 2
        #v_RH_2_B  = Ball[0:1,0] - RH[0:1,0]  # Vector form the robot home to the ball
        v_RH_2_DP  = DP[0:1,0]  - RH[0:1,0]   # Vector from robot home to desired point

        #norm_RH_2_RH1 = np.norm(v_RH_2_RH1)  # Norm from robot home to robot home 1
        #norm_RH_2_RH2 = np.norm(v_RH_2_RH2)  # Norm from robot home to robot home 2
        norm_RH_2_RA1 = np.norm(v_RH_2_RA1)   # Norm from robot home to robot away 1
        norm_RH_2_RA2 = np.norm(v_RH_2_RA2)   # Norm from robot home to robot away 2
        #norm_RH_2_RB = np.norm(v_RH_2_B)     # Norm from robot home to ball
        norm_RH_2_DP = np.norm(v_RH_2_DP)     # Norm from robot home to desired point

        #vn_RH_2_RH1 = v_RH_2_RH1/norm_RH_2_RH1  # Normalized vector from robot home to robot home 1
        #vn_RH_2_RH2 = v_RH_2_RH2/norm_RH_2_RH2  # Normalized vector from robot home to robot home 2
        vn_RH_2_RA1 = v_RH_2_RA1/norm_RH_2_RA1   # Normalized vector from robot home to robot away 1
        vn_RH_2_RA2 = v_RH_2_RA2/norm_RH_2_RA2   # Vector from robot home to robot away 2
        #vn_RH_2_B   = v_RH_2_B/norm_RH_2_B      # Vector from robot home to ball
        vn_RH_2_DP  = v_RH_2_DP/norm_RH_2_DP     # Vector from robot home to desired point

        #theta_RH_2_RH1 =  np.arctan2(vn_RH_2_RH1[1,0],vn_RH_2_RH1[0,0]) # Theta from RH and RH1 
        #theta_RH_2_RH2 =  np.arctan2(vn_RH_2_RH2[1,0],vn_RH_2_RH2[0,0]) # Theta from RH and RH1 
        theta_RH_2_RA1 =  np.arctan2(vn_RH_2_RA1[1,0],vn_RH_2_RA1[0,0])  # Theta from RH and RH1 
        theta_RH_2_RA2 =  np.arctan2(vn_RH_2_RA2[1,0],vn_RH_2_RA2[0,0])  # Theta from RH and RH1 
        #theta_RH_2_B   =  np.arctan2(vn_RH_2_B[1,0],vn_RH_2_B[0,0])     # Theta from RH and RH1 
        theta_RH_2_DP  =  np.arctan2(vn_RH_2_DP[1,0],vn_RH_2_DP[0,0])    # Theta from RH and RH1 

        #thres_RH_2_RH1 = _dminRB/norm_RH_2_RH1
        #thres_RH_2_RH2 = _dminRB/norm_RH_2_RH2 
        thres_RH_2_RA1 = _dminRB/norm_RH_2_RA1 
        #thres_RH_2_RA2 = _dminRB/norm_RH_2_RA2 
        thres_RH_2_RB = _dminRB/norm_RH_2_B 


        if norm_RH_2_RA1 < norm_RH_2_B and np.dot(vn_RH_2_RA1,vn_RH_2_DP) < thres_RH_2_RA1: # robot will collide with RA1
            psi = theta_RH_2_RA1 + np.arcsin(thres_RH_2_RA1)*np.sign(theta_RH_2_DP - theta_RH_2_RA1)
            x = RH[0,0] + norm_RH_2_DP*np.cos(psi)
            y = RH[1,0] + norm_RH_2_DP*np.sin(psi)
            if abs(y) + P.RobotRadius > P.max_y/2:
                y = y - 2*norm_RH_2_DP*np.sin(psi)

        elif norm_RH_2_RA2 < norm_RH_2_B and np.dot(vn_RH_2_DP,vn_RH_2_RA2) < thres_RH_2_RA2: # robot will collide with RA2
            psi = theta_RH_2_RA2 + np.arcsin(thres_RH_2_RA2)*np.sign(theta_RH_2_DP - theta_RH_2_RA2)
            x = RH[0,0] + norm_RH_2_DP*np.cos(psi)
            y = RH[1,0] + norm_RH_2_DP*np.sin(psi)
            if abs(y) + P.RobotRadius > P.max_y/2:
                y = y - 2*norm_RH_2_DP*np.sin(psi)
        else:
            pass

        return x,y

