/*
 * ComputerVision.h
 *
 *  Created on: Mar 14, 2015
 *      Author: ecestudent
 */

#ifndef COMPUTERVISION_H_
#define COMPUTERVISION_H_

#include <sstream>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv/cv.h>
#include <pthread.h>
#include <semaphore.h>
#include <queue>

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "robotsoccer/locations.h"

#define PRINT_FREQ 30

#endif /* COMPUTERVISION_H_ */
