// Generated by gencpp from file robotsoccer/commandcenter.msg
// DO NOT EDIT!


#ifndef ROBOTSOCCER_MESSAGE_COMMANDCENTER_H
#define ROBOTSOCCER_MESSAGE_COMMANDCENTER_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace robotsoccer
{
template <class ContainerAllocator>
struct commandcenter_
{
  typedef commandcenter_<ContainerAllocator> Type;

  commandcenter_()
    : header()
    , command(0)  {
    }
  commandcenter_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , command(0)  {
    }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef int32_t _command_type;
  _command_type command;




  typedef boost::shared_ptr< ::robotsoccer::commandcenter_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::robotsoccer::commandcenter_<ContainerAllocator> const> ConstPtr;

}; // struct commandcenter_

typedef ::robotsoccer::commandcenter_<std::allocator<void> > commandcenter;

typedef boost::shared_ptr< ::robotsoccer::commandcenter > commandcenterPtr;
typedef boost::shared_ptr< ::robotsoccer::commandcenter const> commandcenterConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::robotsoccer::commandcenter_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::robotsoccer::commandcenter_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace robotsoccer

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/indigo/share/std_msgs/cmake/../msg'], 'robotsoccer': ['/home/ecestudent/RobotSoccer/catkin_ws/src/robotsoccer/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::robotsoccer::commandcenter_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::robotsoccer::commandcenter_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robotsoccer::commandcenter_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::robotsoccer::commandcenter_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robotsoccer::commandcenter_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::robotsoccer::commandcenter_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::robotsoccer::commandcenter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "b5324fc73f93e6e7fc3f8dd5112ba032";
  }

  static const char* value(const ::robotsoccer::commandcenter_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xb5324fc73f93e6e7ULL;
  static const uint64_t static_value2 = 0xfc3f8dd5112ba032ULL;
};

template<class ContainerAllocator>
struct DataType< ::robotsoccer::commandcenter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "robotsoccer/commandcenter";
  }

  static const char* value(const ::robotsoccer::commandcenter_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::robotsoccer::commandcenter_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
int32 command\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::robotsoccer::commandcenter_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::robotsoccer::commandcenter_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.command);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER;
  }; // struct commandcenter_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::robotsoccer::commandcenter_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::robotsoccer::commandcenter_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "command: ";
    Printer<int32_t>::stream(s, indent + "  ", v.command);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROBOTSOCCER_MESSAGE_COMMANDCENTER_H
